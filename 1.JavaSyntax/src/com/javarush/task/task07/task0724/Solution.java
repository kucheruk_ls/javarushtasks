package com.javarush.task.task07.task0724;

/* 
Семейная перепись
Создай класс Human с полями имя(String), пол(boolean), возраст(int), отец(Human), мать(Human). Создай объекты и заполни их так, чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.

Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.

Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
...
*/

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        Human grandPa1 = new Human("Ivan", true, 60);
        Human grandPa2 = new Human("Petro", true, 70);
        Human grandMa1 = new Human("Vasilisa", false, 60);
        Human grandMa2 = new Human("Irina", false, 65);
        Human father = new Human("Stepan", true, 42, grandPa1, grandMa1);
        Human muther = new Human("Pelageya", false, 36, grandPa2, grandMa2);
        Human kinder1 = new Human("Vasya", true, 12, father, muther);
        Human kinder2 = new Human("Kolya", true, 14, father, muther);
        Human kinder3 = new Human("Sveta", false, 10, father, muther);

        System.out.println(grandPa1);
        System.out.println(grandPa2);
        System.out.println(grandMa1);
        System.out.println(grandMa2);
        System.out.println(father);
        System.out.println(muther);
        System.out.println(kinder1);
        System.out.println(kinder2);
        System.out.println(kinder3);
    }

    public static class Human {
        //напишите тут ваш код
        private String name;
        private boolean sex;
        private int age;
        private Human father;
        private Human mother;

        public Human(String name, boolean sex, int age){
            this.name=name;
            this.sex=sex;
            this.age=age;

        }

        public Human(String name, boolean sex, int age, Human father, Human mother){
            this.name=name;
            this.sex=sex;
            this.age=age;
            this.father=father;
            this.mother=mother;
        }





        public String toString() {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }
}






















