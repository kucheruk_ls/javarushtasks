package com.javarush.task.task05.task0532;

import java.io.*;

/* 
Задача по алгоритмам
Написать программу, которая:
1. считывает с консоли число N, которое должно быть больше 0
2. потом считывает N чисел с консоли
3. выводит на экран максимальное из введенных N чисел.


Требования:
1. Программа должна считывать числа с клавиатуры.
2. В классе должен быть метод public static void main.
3. Нельзя добавлять новые методы в класс Solution.
4. Программа должна выводить на экран максимальное из введенных N чисел.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int maximum = 0;

        int N = Integer.parseInt(reader.readLine());

        for(int i=N;i>0;i--){
            int c = Integer.parseInt(reader.readLine());
            if(i==N){
                maximum=c;
            }else{
                if(maximum<c){
                    maximum=c;
                }
            }

        }


        //напишите тут ваш код

        System.out.println(maximum);
    }
}
