package com.javarush.task.task03.task0319;

/* 
Предсказание на будущее

Предсказание на будущее
Ввести с клавиатуры отдельно Имя, число1, число2.
Вывести надпись:
"Имя" получает "число1" через "число2" лет.

Пример:
Коля получает 3000 через 5 лет.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String name = reader.readLine();
        int ch1 = Integer.parseInt(reader.readLine());
        int ch2 = Integer.parseInt(reader.readLine());

        System.out.println(name+" получает "+ch1+" через "+ ch2 +" лет.");
    }
}
