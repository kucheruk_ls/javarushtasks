package com.javarush.task.task01.task0132;

/* 
Сумма цифр трехзначного числа

Реализуй метод sumDigitsInNumber(int number). Метод на вход принимает целое трехзначное число. Нужно посчитать сумму цифр этого числа, и вернуть эту сумму.

Пример:
Метод sumDigitsInNumber вызывается с параметром 546.

Пример вывода:
15


*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution  {
    public static void main(String[] args) throws IOException {
        System.out.println(sumDigitsInNumber(546));
    }

    public static int sumDigitsInNumber(int number)throws IOException  {
        //напишите тут ваш код

        int sum=0;
        while (number!=0){
            sum += number%10;
            number/=10;
        }
        return sum;
    }
}