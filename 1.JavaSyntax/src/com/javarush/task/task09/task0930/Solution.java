package com.javarush.task.task09.task0930;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

/* 
Задача по алгоритмам
Задача: Пользователь вводит с клавиатуры список слов (и чисел).
Слова вывести в возрастающем порядке, числа - в убывающем.

Пример ввода:
Вишня
1
Боб
3
Яблоко
22
0
Арбуз

Пример вывода:
Арбуз
22
Боб
3
Вишня
1
0
Яблоко


Требования:
1. Программа должна считывать данные с клавиатуры.
2. Программа должна выводить данные на экран.
3. Выведенные слова должны быть упорядочены по возрастанию.
4. Выведенные числа должны быть упорядочены по убыванию.
5. Метод main должен использовать метод sort.
6. Метод sort должен использовать метод isGreaterThan.
7. Метод sort должен использовать метод isNumber.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<String>();
        while (true) {
            String s = reader.readLine();
            if (s.isEmpty()) break;
            list.add(s);
        }

        String[] array = list.toArray(new String[list.size()]);
        sort(array);

        for (String x : array) {
            System.out.println(x);
        }
    }

    public static void sort(String[] array) {
        //напишите тут ваш код
        ArrayList<String> stznach = new ArrayList<>();
        ArrayList<Integer> stindex = new ArrayList<>();
        ArrayList<Integer> intznach = new ArrayList<>();
        ArrayList<Integer> intindex = new ArrayList<>();

        for(int i=0;i<array.length;i++){
            if(isNumber(array[i])){
                intznach.add(Integer.parseInt(array[i]));
                intindex.add(i);

            }else{
                stznach.add(array[i]);
                stindex.add(i);

            }

            for(int s = 0;s<stznach.size();s++){
                for(int s2=0;s2<stznach.size()-1;s2++) {

                    if (isGreaterThan(stznach.get(s2), stznach.get(s2+1))) {
                        String tmp = stznach.get(s2+1);
                        stznach.set(s2+1, stznach.get(s2));
                        stznach.set(s2, tmp);
                    }
                }
            }





            for(int i2=0;i2<intznach.size()-1;i2++){
                if(intznach.get(i2)<intznach.get(i2+1)){
                    int tmp = intznach.get(i2);
                    intznach.set(i2,intznach.get(i2+1));
                    intznach.set(i2+1,tmp);
                }
            }

            for(int i3=0;i3<intznach.size();i3++){
                array[intindex.get(i3)]=intznach.get(i3).toString();
            }
            for(int i4=0;i4<stznach.size();i4++){
                array[stindex.get(i4)]=stznach.get(i4);
            }





        }





    }

    // Метод для сравнения строк: 'а' больше чем 'b'
    public static boolean isGreaterThan(String a, String b) {
        return a.compareTo(b) > 0;
    }


    // Переданная строка - это число?
    public static boolean isNumber(String s) {
        if (s.length() == 0) return false;

        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if ((i != 0 && c == '-') // есть '-' внутри строки
                    || (!Character.isDigit(c) && c != '-') // не цифра и не начинается с '-'
                    || (i == 0 && c == '-' && chars.length == 1)) // не '-'
            {
                return false;
            }
        }
        return true;
    }
}
