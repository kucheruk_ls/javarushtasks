package com.javarush.task.task10.task1013;

/* 
Конструкторы класса Human
Конструкторы класса Human
Напиши класс Human с 6 полями.
Придумай и реализуй 10 различных конструкторов для него.
Каждый конструктор должен иметь смысл.
*/

public class Solution {
    public static void main(String[] args) {
    }

    public static class Human {
        // напишите тут ваши переменные и конструкторы
        private String name;
        private int age;
        private boolean sex;
        private int veight;
        private int height;
        private String famili;

        public Human(){//1

        }
        public Human(String name){//2
            this.name = name;
        }
        public Human(String name, int age){//3
            this.name = name;
            this.age = age;
        }
        public Human(String name, int age, boolean sex){//4
            this.name = name;
            this.age = age;
            this.sex = sex;
        }
        public Human(String name, int age, boolean sex, int veight){//5
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.veight = veight;
        }
        public Human(String name, int age, boolean sex, int veight, int height){//6
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.veight = veight;
            this.height = height;
        }
        public Human(String name, int age, boolean sex, int veight, int height, String famili){//7
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.veight = veight;
            this.height = height;
            this.famili = famili;
        }
        public Human(String name, int age, int veight, int height, String famili){//8
            this.name = name;
            this.age = age;
            this.veight = veight;
            this.height = height;
            this.famili = famili;
        }
        public Human(String name, int age, boolean sex, int height, String famili){//9
            this.name = name;
            this.age = age;
            this.sex = sex;

            this.height = height;
            this.famili = famili;
        }
        public Human(String name, int age, boolean sex, String famili){//10
            this.name = name;
            this.age = age;
            this.sex = sex;

            this.famili = famili;
        }

    }
}
