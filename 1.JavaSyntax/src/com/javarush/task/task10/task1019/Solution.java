package com.javarush.task.task10.task1019;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/* 
Функциональности маловато!
Задача: Программа вводит с клавиатуры пару (число и строку) и выводит их на экран.
Новая задача: Программа вводит с клавиатуры пары (число и строку), сохраняет их в HashMap.
Пустая строка - конец ввода данных.
Числа могут повторяться.
Строки всегда уникальны.
Введенные данные не должны потеряться!
Затем программа выводит содержание HashMap на экран.
Каждую пару с новой строки.

Пример ввода:
1
Мама
2
Рама
1
Мыла

Пример вывода:
1 Мыла
2 Рама
1 Мама


*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        HashMap<String, Integer> map = new HashMap<>();
        while (true) {

            String sid = reader.readLine();
            int id = 0;
            if(sid.isEmpty()){
                break;
            }else{
                id = Integer.parseInt(sid);
            }
            String name = reader.readLine();
            if(name.isEmpty()){
                map.put("",id);
                break;
            }

            map.put(name, id);
        }

        for(Map.Entry<String, Integer> pair : map.entrySet()){
            String name = pair.getKey();
            int id = pair.getValue();
            System.out.println(id+" "+name);
        }

        //System.out.println("Id=" + id + " Name=" + name);
    }
}
