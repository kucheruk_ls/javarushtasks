package com.javarush.task.task10.task1012;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

/* 
Количество букв
Ввести с клавиатуры 10 строчек и подсчитать в них количество различных букв (для 33 маленьких букв алфавита). Результат вывести на экран в алфавитном порядке.

Пример вывода:
а 5
б 8
в 3
г 7
д 0
...
я 9


*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        // алфавит
        String abc = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        char[] abcArray = abc.toCharArray();

        ArrayList<Character> alphabet = new ArrayList<Character>();
        for (int i = 0; i < abcArray.length; i++) {
            alphabet.add(abcArray[i]);
        }

        // ввод строк
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < 10; i++) {
            String s = reader.readLine();
            list.add(s.toLowerCase());
        }

HashMap<Character, Integer> map = new HashMap<>();
        for(int ia = 0;ia<alphabet.size();ia++){
            map.put(alphabet.get(ia), 0);
        }



        for(int i = 0;i<list.size();i++){
            char[] strArray = list.get(i).toCharArray();
        //поместим строку в список посимвольно
            ArrayList<Character> strList = new ArrayList<>();
        for(int i3=0;i3<strArray.length;i3++){
            strList.add(strArray[i3]);
        }
        //будем сверять список strList с алфавитным списком alphabet и считать обнаруженные буквы



            for(int i2 = 0;i2<strList.size();i2++){
                for(int i4=0;i4<alphabet.size();i4++){
                if(strList.get(i2).equals(alphabet.get(i4))){






                            Iterator<Map.Entry<Character, Integer>> iterator = map.entrySet().iterator();
                        while (iterator.hasNext()) {
                            Map.Entry<Character, Integer> entry = iterator.next();
                            Character simv = entry.getKey();
                            Integer kol = entry.getValue();
                            if (strList.get(i2).equals(simv)) {
                                kol = kol + 1;
                                map.put(simv, kol);

                            }
                        }


                }
                }

            }


        }
        //блок сортировки заменить блоком


        for(int ip=0;ip<alphabet.size();ip++){
            Iterator<Map.Entry<Character, Integer>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<Character, Integer> entry = iterator.next();

                if(alphabet.get(ip).equals(entry.getKey())){
                    System.out.println(entry.getKey()+" "+entry.getValue());
                }
            }

        }

    }


}
