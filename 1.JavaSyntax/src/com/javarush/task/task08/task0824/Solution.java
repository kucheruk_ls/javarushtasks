package com.javarush.task.task08.task0824;

/* 
Собираем семейство
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.


Требования:
1. Программа должна выводить текст на экран.
2. Класс Human должен содержать четыре поля.
3. Класс Human должен содержать один метод.
4. Класс Solution должен содержать один метод.
5. Программа должна создавать объекты и заполнять их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей и выводить все объекты Human на экран.
*/

import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        ArrayList<Human> chnull = new ArrayList<>();
        ArrayList<Human> children = new ArrayList<>();
        Human child1 = new Human("Sofiko", false, 5, chnull);
        Human child2 = new Human("Koli", true, 10, chnull);
        Human child3 = new Human("Max", true, 12, chnull);
        children.add(child1);
        children.add(child2);
        children.add(child3);

        Human mom = new Human("Elisabet", false, 32, children);
        Human dad = new Human("Nikolay", true, 36, children);
        //первый дед
        ArrayList<Human> gpchild1 = new ArrayList<>();
        ArrayList<Human> gpchild2 = new ArrayList<>();
        gpchild1.add(dad);
        gpchild2.add(mom);
        Human grandPa1 = new Human("Ivan", true, 78, gpchild1);
        Human grandMa1 = new Human("Sofi", false, 75, gpchild1);
        Human grandPa2 = new Human("Vasya", true, 74, gpchild2);
        Human grandMa2 = new Human("Nelli", false, 70, gpchild2);

        System.out.println(grandPa1);
        System.out.println(grandMa1);
        System.out.println(grandPa2);
        System.out.println(grandMa2);
        System.out.println(dad);
        System.out.println(mom);
        System.out.println(child1);
        System.out.println(child2);
        System.out.println(child3);


    }

    public static class Human {
        //напишите тут ваш код
        String name;
        boolean sex;
        int age;
        ArrayList<Human> children;

        public Human(String name, boolean sex, int age, ArrayList<Human> children){
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = children;
        }





        public String toString() {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0) {
                text += ", дети: " + this.children.get(0).name;

                for (int i = 1; i < childCount; i++) {
                    Human child = this.children.get(i);
                    text += ", " + child.name;
                }
            }
            return text;
        }
    }

}
