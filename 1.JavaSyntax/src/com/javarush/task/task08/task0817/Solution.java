package com.javarush.task.task08.task0817;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* 
Нам повторы не нужны
Создать словарь (Map<String, String>) занести в него десять записей по принципу "фамилия" - "имя".
Удалить людей, имеющих одинаковые имена.


Требования:
1. Программа не должна выводить текст на экран.
2. Программа не должна считывать значения с клавиатуры.
3. Класс Solution должен содержать только четыре метода.
4. Метод createMap() должен создавать и возвращать словарь HashMap с типом элементов String, String состоящих из 10 записей.
5. Метод removeTheFirstNameDuplicates() должен удалять из словаря всех людей, имеющие одинаковые имена.
6. Метод removeTheFirstNameDuplicates() должен вызывать метод removeItemFromMapByValue().
*/

public class Solution {
    public static HashMap<String, String> createMap() {
        //напишите тут ваш код
        HashMap<String, String> map = new HashMap<>();
        map.put("Ивановe", "Петр");
        map.put("Иванов", "Василий");
        map.put("Петров", "Иван");
        map.put("Сидоров", "Игнат");
        map.put("Кузнецов", "Василий");
        map.put("Федоров", "Алексей");
        map.put("Петровe", "Костя");
        map.put("Шапкин", "Илья");
        map.put("Кокин", "Меня");
        map.put("Попов", "Алексей");

        return map;
    }

    public static void removeTheFirstNameDuplicates(HashMap<String, String> map) {
        //напишите тут ваш код
        // ArrayList<String> list = new ArrayList<>();
        HashMap<String, String> mapdubl = new HashMap<>(map);

        //String key = pair.getKey();
        //String value = pair.getValue();
        for (Map.Entry<String, String> pair : mapdubl.entrySet()) {
            for (Map.Entry<String, String> pair2 : mapdubl.entrySet()) {
                if (pair.getValue().equals(pair2.getValue()) && !pair.getKey().equals(pair2.getKey())) {
                    //list.add(pair.getKey());
                    removeItemFromMapByValue(map, pair2.getValue());
                }
            }
        }
    }




    public static void removeItemFromMapByValue(HashMap<String, String> map, String value) {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair : copy.entrySet()) {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }

    public static void main(String[] args) {
/*
        HashMap<String, String> map = new HashMap<>(createMap());
        removeTheFirstNameDuplicates(map);

        for(Map.Entry<String, String> pair:map.entrySet()){
            System.out.println(pair.getKey()+" "+pair.getValue());
        }
*/
    }
}
