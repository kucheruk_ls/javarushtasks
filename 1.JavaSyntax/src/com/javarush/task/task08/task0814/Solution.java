package com.javarush.task.task08.task0814;

import java.util.HashSet;
import java.util.Set;

/* 
Больше 10? Вы нам не подходите
Создать множество чисел(Set<Integer>), занести туда 20 различных чисел.
Удалить из множества все числа больше 10.


Требования:
1. Программа не должна выводить текст на экран.
2. Программа не должна считывать значения с клавиатуры.
3. Класс Solution должен содержать только три метода.
4. Метод createSet() должен создавать и возвращать множество HashSet состоящих из 20 различных чисел.
5. Метод removeAllNumbersMoreThan10() должен удалять из множества все числа больше 10.
*/

public class Solution {
    public static HashSet<Integer> createSet() {
        //напишите тут ваш код
        HashSet<Integer> set = new HashSet<>();
        set.add(455);
        set.add(5);
        set.add(56);
        set.add(23);
        set.add(12);
        set.add(59);
        set.add(45);
        set.add(51);
        set.add(54);
        set.add(454);
        set.add(55);
        set.add(47);
        set.add(551);
        set.add(549);
        set.add(9);
        set.add(7);
        set.add(87);
        set.add(881);
        set.add(88);
        set.add(563);

        return set;


    }

    public static HashSet<Integer> removeAllNumbersMoreThan10(HashSet<Integer> set) {
        HashSet<Integer> tmp = new HashSet<>();
        for(Integer in:set){
           if(in>10){
               tmp.add(in);
           }
       }
       for(Integer inn:tmp){
            set.remove(inn);
       }
        return set;
    }

    public static void main(String[] args) {
/*
        HashSet<Integer> set = createSet();
        set = removeAllNumbersMoreThan10(set);

        for(Integer in:set){
            System.out.println(in);
        }

*/

    }
}
