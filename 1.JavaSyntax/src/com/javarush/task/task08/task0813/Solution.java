package com.javarush.task.task08.task0813;

import java.util.Set;
import java.util.HashSet;

/* 
20 слов на букву «Л»
Создать множество строк (Set<String>), занести в него 20 слов на букву "Л".


Требования:
1. Программа не должна выводить текст на экран.
2. Программа не должна считывать значения с клавиатуры.
3. Класс Solution должен содержать только два метода.
4. Метод createSet() должен создавать и возвращать множество (реализация HashSet).
5. Множество из метода createSet() должно содержать 20 слов на букву «Л».
*/

public class Solution {
    public static Set<String> createSet() {
        //напишите тут ваш код
        HashSet<String> set = new HashSet<String>();
        set.add("Лист");
        set.add("Лоб");
        set.add("Лоь");
        set.add("Лом");
        set.add("Лось");
        set.add("Лост");
        set.add("Лоск");
        set.add("Лето");
        set.add("Ла");
        set.add("Лили");
        set.add("Лимпопо");
        set.add("Лимон");
        set.add("Лоббист");
        set.add("Литература");
        set.add("Лосось");
        set.add("Линк");
        set.add("Либидо");
        set.add("Леракс");
        set.add("Ломка");
        set.add("Лунтик");

        return set;
    }

    public static void main(String[] args) {

    }
}
