package com.javarush.task.task08.task0816;

import java.util.*;

/* 
Добрая Зинаида и летние каникулы
*/

public class Solution {
    public static HashMap<String, Date> createMap() {
        HashMap<String, Date> map = new HashMap<String, Date>();
        map.put("Stallone", new Date("JUNE 1 1980"));
        map.put("Chan", new Date("JULY 23 1955"));
        map.put("Shvarcenegger", new Date("OCTOBER 10 1946"));
        map.put("Ivanov", new Date("DECEMBER 12 1954"));
        map.put("Cvetov", new Date("MAY 11 1978"));
        map.put("Stallonef", new Date("JUNE 1 1980"));
        map.put("Chanf", new Date("JULY 23 1955"));
        map.put("Shvarceneggerf", new Date("OCTOBER 10 1946"));
        map.put("Ivanovf", new Date("DECEMBER 12 1954"));
        map.put("Cvetovf", new Date("MAY 11 1978"));
    return map;
    }

    public static void removeAllSummerPeople(HashMap<String, Date> map) {
        //напишите тут ваш код
        ArrayList<String> removeList = new ArrayList<>();
        Iterator<Map.Entry<String, Date>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String, Date> entry = iterator.next();

            Date d = entry.getValue();
           if(d.getMonth()==5||d.getMonth()==6||d.getMonth()==7){
               removeList.add(entry.getKey());
           }
        }

        for(String s:removeList){
            map.remove(s);
        }

    }

    public static void main(String[] args) {
/*
        HashMap<String, Date> map = createMap();

        removeAllSummerPeople(map);

        Iterator<Map.Entry<String, Date>> iterator = map.entrySet().iterator();



        while (iterator.hasNext()){
            Map.Entry<String, Date> entry = iterator.next();
            String s = entry.getKey();
            Date d = entry.getValue();

            System.out.println(s+" "+d.getMonth());
        }

*/

    }
}
