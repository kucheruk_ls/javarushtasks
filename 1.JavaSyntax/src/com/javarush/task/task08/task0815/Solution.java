package com.javarush.task.task08.task0815;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* 
Перепись населения
Создать словарь (Map<String, String>) занести в него десять записей по принципу "Фамилия" - "Имя".
Проверить сколько людей имеют совпадающие с заданным именем или фамилией.
*/

public class Solution {
    public static HashMap<String, String> createMap() {
        //напишите тут ваш код
        HashMap<String, String> map = new HashMap<>();
        map.put("Ивановe", "Петр");
        map.put("Иванов", "Василий");
        map.put("Петров", "Иван");
        map.put("Сидоров", "Игнат");
        map.put("Кузнецов", "Василий");
        map.put("Федоров", "Алексей");
        map.put("Петровe", "Костя");
        map.put("Шапкин", "Илья");
        map.put("Кокин", "Меня");
        map.put("Попов", "Алексей");

        return map;
    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name) {
        //напишите тут ваш код
        int countfn=0;
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String, String> entry = iterator.next();
            if(entry.getValue()==name){
                countfn++;
            }
        }

        return countfn;

    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String lastName) {
        //напишите тут ваш код
        int countln=0;
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();

        while (iterator.hasNext()){
            Map.Entry<String, String> entry = iterator.next();
            if(entry.getKey()==lastName){
                countln++;
            }
        }
        return countln;
    }

    public static void main(String[] args) {
/*
        HashMap<String, String> map = createMap();

        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();

        while (iterator.hasNext()){
            Map.Entry<String, String> entry = iterator.next();
            String k = entry.getKey();
            String v = entry.getValue();
            System.out.println(k+" "+v);
        }
        */
    }
}
