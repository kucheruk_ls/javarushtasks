package com.javarush.task.task08.task0818;

import java.util.HashMap;
import java.util.Map;

/* 
Только для богачей

Создать словарь (Map<String, Integer>) и занести в него десять записей по принципу: "фамилия" - "зарплата".
Удалить из словаря всех людей, у которых зарплата ниже 500.


Требования:
1. Программа не должна выводить текст на экран.
2. Программа не должна считывать значения с клавиатуры.
3. Класс Solution должен содержать только три метода.
4. Метод createMap() должен создавать и возвращать словарь HashMap с типом элементов String, Integer состоящих из 10 записей по принципу «фамилия» - «зарплата».
5. Метод removeItemFromMap() должен удалять из словаря всех людей, у которых зарплата ниже 500.
*/

public class Solution {
    public static HashMap<String, Integer> createMap() {
        //напишите тут ваш код
        HashMap<String, Integer> zp = new HashMap<>();
        zp.put("Orlov", 500);
        zp.put("Ivanovz", 300);
        zp.put("Ivanovx", 300);
        zp.put("Ivanovc", 800);
        zp.put("Ivanovcv", 800);
        zp.put("Ivanovv", 800);
        zp.put("Ivanovb", 800);
        zp.put("Ivanovn", 800);
        zp.put("Ivanovm", 800);
        zp.put("Ivanovg", 800);
        return zp;
    }

    public static void removeItemFromMap(HashMap<String, Integer> map) {
        HashMap<String, Integer> mapdbl = new HashMap<>(map);
        for(Map.Entry<String, Integer> entry:mapdbl.entrySet()){
            if(entry.getValue()<500){
                map.remove(entry.getKey());
            }
        }

    }

    public static void main(String[] args) {

    }
}