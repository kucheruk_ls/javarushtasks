package com.javarush.task.task08.task0827;

import java.util.Date;

/* 
Работа с датой
1. Реализовать метод isDateOdd(String date) так, чтобы он возвращал true, если количество дней с начала года - нечетное число, иначе false
2. String date передается в формате FEBRUARY 1 2013
Не забудьте учесть первый день года.

Пример:
JANUARY 1 2000 = true
JANUARY 2 2020 = false


Требования:
1. Программа должна выводить текст на экран.
2. Класс Solution должен содержать два метода.
3. Метод isDateOdd() должен возвращать true, если количество дней с начала года - нечетное число, иначе false.
4. Метод main() должен вызывать метод isDateOdd().
*/

public class Solution {
    public static void main(String[] args) {
        //System.out.println(isDateOdd("MAY 1 2013"));
        System.out.println(isDateOdd("JANUARY 7 2000"));
    }

    public static boolean isDateOdd(String date) {
        Date day = new Date(date);
        Date oneday = new Date();
        oneday.setMonth(0);
        oneday.setDate(1);
        oneday.setMinutes(0);
        oneday.setHours(0);
        oneday.setSeconds(0);
        oneday.setYear(day.getYear());
        long newg = oneday.getTime();

        long asysya = day.getTime();
        long ps = asysya-newg;
        if ((ps/1000L/60L/60L/24L) % 2 == 0) {
            return false;
        } else {
            return true;
        }
    }
}