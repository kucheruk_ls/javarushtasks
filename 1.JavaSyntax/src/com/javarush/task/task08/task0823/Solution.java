package com.javarush.task.task08.task0823;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Омовение Рамы
Написать программу, которая вводит с клавиатуры строку текста.
Программа заменяет в тексте первые буквы всех слов на заглавные.
Вывести результат на экран.

Пример ввода:
мама мыла раму.

Пример вывода:
Мама Мыла Раму.


*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();

        //напишите тут ваш код
        char[] sm = s.toCharArray();
        sm[0]=Character.toUpperCase(sm[0]);
        for(int i=0;i<sm.length;i++){
            if(sm[i]==' '){
                sm[i+1]=Character.toUpperCase(sm[i+1]);

            }
        }
        String sr="";
        for(int i=0;i<sm.length;i++){
            sr=sr+sm[i];
        }
        System.out.println(sr);
}
}
