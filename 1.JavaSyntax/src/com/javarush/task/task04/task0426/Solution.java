package com.javarush.task.task04.task0426;

/* 
Ярлыки и числа

Ярлыки и числа
Ввести с клавиатуры целое число. Вывести на экран его строку-описание следующего вида:
"отрицательное четное число" - если число отрицательное и четное,
"отрицательное нечетное число" - если число отрицательное и нечетное,
"ноль" - если число равно 0,
"положительное четное число" - если число положительное и четное,
"положительное нечетное число" - если число положительное и нечетное.

Пример для числа 100:
положительное четное число

Пример для числа -51:
отрицательное нечетное число


*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int c = Integer.parseInt(reader.readLine());
        if(c==0) System.out.println("ноль");
        if(c<0&&c%2==0) System.out.println("отрицательное четное число");
        if(c>0&&c%2==0) System.out.println("положительное четное число");
        if(c<0&&c%2!=0) System.out.println("отрицательное нечетное число");
        if(c>0&&c%2!=0) System.out.println("положительное нечетное число");
    }
}
