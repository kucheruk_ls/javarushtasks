package com.javarush.task.task04.task0436;


/* 
Рисуем прямоугольник
Рисуем прямоугольник
Ввести с клавиатуры два числа m и n.
Используя цикл for вывести на экран прямоугольник размером m на n из восьмёрок.

Пример: m=2, n=4
8888
8888
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int m = Integer.parseInt(reader.readLine());
        int n = Integer.parseInt(reader.readLine());

        for(int i=m;i>0;i--){
            for(int i2=n;i2>0;i2--){
                System.out.print("8");
            }
            System.out.println();
        }

    }
}
