package com.javarush.task.task04.task0409;

/* 
Ближайшее к 10

Ближайшее к 10
Напишите метод closeToTen. Метод должен выводить на экран ближайшее к 10 из двух чисел, записанных в аргументах метода.
Например, среди чисел 8 и 11 ближайшее к десяти 11. Если оба числа на равной длине к 10, то вывести на экран любое из них.

Подсказка:
используйте метод public static int abs(int a), который возвращает абсолютную величину числа.
*/

public class Solution {
    public static void main(String[] args) {
        closeToTen(11, 11);
        closeToTen(-16, 16);
    }

    public static void closeToTen(int a, int b) {
        //напишите тут ваш код
        int a1=abs(10-a);
        int b1=abs(10-b);
        if(a1>b1){
            System.out.println(b);
        }else if(a1<b1){
            System.out.println(a);
        }else{
            double  rend = Math.random();
            if((double)(rend-a1)<(double) (rend-b1)){
                System.out.println(a);
            }else{
                System.out.println(b);
            }

        }






    }

    public static int abs(int a) {
        if (a < 0) {
            return -a;
        } else {
            return a;
        }
    }
}