package com.javarush.task.task04.task0429;

/* 
Положительные и отрицательные числа

Положительные и отрицательные числа
Ввести с клавиатуры три целых числа. Вывести на экран количество положительных и количество отрицательных чисел в исходном наборе,
в следующем виде:
"количество отрицательных чисел: а", "количество положительных чисел: б",
где а, б - искомые значения.

Пример:
а) при вводе чисел
2
5
6
получим вывод
количество отрицательных чисел: 0
количество положительных чисел: 3

Пример:
б) при вводе чисел
-2
-5
6
получим вывод
количество отрицательных чисел: 2
количество положительных чисел: 1
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());
        int countp=0;
        int counto=0;
        if(a>0)countp++;
        else if(a<0) counto++;
        if(b>0)countp++;
        else if(b<0) counto++;
        if(c>0)countp++;
        else if(c<0) counto++;


        System.out.println("количество отрицательных чисел: "+counto);
        System.out.println("количество положительных чисел: "+countp);




    }
}
