package com.javarush.task.task04.task0417;

/* 
Существует ли пара?
Существует ли пара?
Ввести с клавиатуры три целых числа. Определить, имеется ли среди них хотя бы одна пара равных между собой чисел.
Если такая пара существует, вывести на экран числа через пробел.
Если все три числа равны между собой, то вывести все три.

Примеры:
а) при вводе чисел
1
2
2
получим вывод
2 2

б) при вводе чисел
2
2
2
получим вывод
2 2 2
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int one = Integer.parseInt(reader.readLine());
        int two = Integer.parseInt(reader.readLine());
        int tru = Integer.parseInt(reader.readLine());

        if(one==two||one==tru||two==tru){
            if(one==two&&one==tru){
                System.out.println(one+" "+two+" "+tru);
            }else if(one==two){
                System.out.println(one+" "+two);
            }else if(one==tru){
                System.out.println(one+" "+tru);
            }else if(tru==two){
                System.out.println(tru+" "+two);
            }
        }
    }
}