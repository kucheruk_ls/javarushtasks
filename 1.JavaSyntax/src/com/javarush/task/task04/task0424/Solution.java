package com.javarush.task.task04.task0424;

/* 
Три числа
Три числа
Ввод с клавиатуры, сравнение чисел и вывод на экран - у студентов 4 уровня секретного центра JavaRush эти навыки оттачиваются до автоматизма.
Давайте напишем программу, в которой пользователь вводит три числа с клавиатуры. Затем происходит сравнение, и если мы находим число, которое
отличается от двух других, выводим на экран его порядковый номер.


*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int c1 = Integer.parseInt(reader.readLine());
        int c2 = Integer.parseInt(reader.readLine());
        int c3 = Integer.parseInt(reader.readLine());
        if(c1!=c2&&c1!=c3&&c2==c3){
            System.out.println(1);
        }else if(c2!=c1&&c2!=c3&&c1==c3){
            System.out.println(2);
        }else if(c3!=c2&&c3!=c1&&c2==c1){
            System.out.println(3);
        }
    }
}
