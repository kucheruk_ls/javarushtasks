package com.javarush.task.task04.task0442;


/* 
Суммирование
Вводить с клавиатуры числа.
Если пользователь ввел -1, вывести на экран сумму всех введенных чисел и завершить программу.
-1 должно учитываться в сумме.


*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int summ=0;
        while (true){
            int i = Integer.parseInt(reader.readLine());
            summ+=i;
            if(i==-1){
                System.out.println(summ);
                break;
            }

        }
        }

}
