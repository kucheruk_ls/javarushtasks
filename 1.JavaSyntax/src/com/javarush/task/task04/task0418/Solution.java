package com.javarush.task.task04.task0418;

/* 
Минимум двух чисел
Минимум двух чисел
Ввести с клавиатуры два целых числа, и вывести на экран минимальное из них.
Если два числа равны между собой, необходимо вывести любое.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());

        if(a<b)
            System.out.println(a);
        else if(a>b)
            System.out.println(b);
        else{
            double a1 = a+Math.random();
            double b1 = b+Math.random();
            if(a1>b1)
                System.out.println(a);
            else System.out.println(b);
        }
    }
}