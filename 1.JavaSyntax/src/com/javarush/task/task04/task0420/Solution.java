package com.javarush.task.task04.task0420;

/* 
Сортировка трех чисел
Сортировка трех чисел
Ввести с клавиатуры три числа, и вывести их в порядке убывания.
Выведенные числа должны быть разделены пробелом.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());
        int a1=0;
        int b1=0;
        int c1=0;
        if(a>b&&a>c){
            a1=a;
            if(b>c){
                b1=b;
                c1=c;
            }else{
                b1=c;
                c1=b;
            }
        }else if(b>a&&b>c){
            a1=b;
            if(a>c){
                b1=a;
                c1=c;
            }else{
                b1=c;
                c1=a;
            }
        }else if(c>a&&c>b){
            a1=c;
            if(b>a){
                b1=b;
                c1=a;
            }else{
                b1=a;
                c1=b;
            }
        }

        System.out.println(a1+" "+b1+" "+c1);

    }
}
