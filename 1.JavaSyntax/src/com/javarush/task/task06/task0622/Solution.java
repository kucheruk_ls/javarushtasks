package com.javarush.task.task06.task0622;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Числа по возрастанию
Задача: Написать программу, которая вводит с клавиатуры 5 чисел и выводит их в возрастающем порядке.

Пример ввода:
3
2
15
6
17

Пример вывода:
2
3
6
15
17


Требования:
1. Программа должна считывать 5 чисел с клавиатуры.
2. Программа должна выводить 5 чисел, каждое с новой строки.
3. Выведенные числа должны быть отсортированы по возрастанию.
4. Вывод должен содержать те же числа, что и были введены (порядо
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] m = new int[5];
        for(int i=0;i<m.length;i++){
            m[i]=Integer.parseInt(reader.readLine());

        }
        int im=0;
        for(int i2=0;i2<m.length;i2++){
            for(int i3=0;i3<m.length;i3++){
                if (m[i2] < m[i3]) {
                    im=m[i2];
                    m[i2]=m[i3];
                    m[i3]=im;
                }
            }
        }
        for(int i4=0;i4<m.length;i4++){
            System.out.println(m[i4]);
        }

        //напишите тут ваш код
    }
}
