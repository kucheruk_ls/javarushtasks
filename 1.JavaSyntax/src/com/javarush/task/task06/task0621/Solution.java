package com.javarush.task.task06.task0621;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Родственные связи кошек
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String grandPa = reader.readLine();
        Cat catGrandPa = new Cat(grandPa);

        String grandMa = reader.readLine();
        Cat catGrandMa = new Cat(grandMa);



        String dadKotofey = reader.readLine();
        Cat catdad = new Cat(dadKotofey, null,catGrandPa);

        String mamVasilisa = reader.readLine();
        Cat catmom = new Cat(mamVasilisa, catGrandMa, null);

        String sonMurchik = reader.readLine();
        Cat catson = new Cat(sonMurchik, catmom, catdad);

        String daughter = reader.readLine();
        Cat catDaughter = new Cat(daughter, catmom, catdad);

        System.out.println(catGrandPa);
        System.out.println(catGrandMa);
        System.out.println(catdad);
        System.out.println(catmom);
        System.out.println(catson);
        System.out.println(catDaughter);
    }

    public static class Cat {
        private String name;
        private Cat muther;
        private Cat father;

        Cat(String name) {
            this.name = name;
        }

        Cat(String name, Cat muther) {
            this.name = name;
            this.muther = muther;
        }


        Cat(String name, Cat muther, Cat father){
            this.name=name;
            this.muther=muther;
            this.father=father;
        }

        @Override
        public String toString() {
            String str="";
            if (muther == null&&father==null){
                str = "Cat name is " + name + ", no mother, no father";
            }
            else if(muther!=null&&father==null){
                str = "Cat name is " + name + ", mother is " + muther.name +", no father";
            }else if(muther!=null&&father!=null){
                str = "Cat name is " + name + ", mother is " + muther.name +", father is "+ father.name;
            }else if(muther==null&&father!=null){
                str = "Cat name is " + name + ", no mother, father is "+ father.name;
            }
              return str;
        }
    }

}
