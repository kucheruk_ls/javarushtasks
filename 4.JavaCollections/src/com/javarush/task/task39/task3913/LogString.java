/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task39.task3913;

import com.javarush.task.task39.task3913.Event;
import com.javarush.task.task39.task3913.Status;

import java.util.Date;

public class LogString {
    private String ip;
    private String user;
    private Date date;
    private Event event;
    private int jobNum;
    private Status status;

    public LogString(String ip, String user, Date date, Event event, int jobNum, Status status) {
        this.ip = ip;
        this.user = user;
        this.date = date;
        this.event = event;
        this.jobNum = jobNum;
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public String getUser() {
        return user;
    }

    public Date getDate() {
        return date;
    }

    public Event getEvent() {
        return event;
    }

    public Status getStatus() {
        return status;
    }

    public int getJobNum() {
        return jobNum;
    }

    @Override
    public String toString() {
        return "LogString{" +
                "ip='" + ip + '\'' +
                ", user='" + user + '\'' +
                ", date=" + date +
                ", event=" + event +
                ", jobNum=" + jobNum +
                ", status=" + status +
                '}';
    }
}
