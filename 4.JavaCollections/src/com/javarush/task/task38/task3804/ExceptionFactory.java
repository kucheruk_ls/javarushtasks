/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task38.task3804;

public class ExceptionFactory {
    //ApplicationExceptionMessage applicationExceptionMessage = new ApplicationExceptionMessage();
    public static Throwable makeThrowable(Enum Exenum){
        Throwable rException = null;
        if(Exenum!=null) {
            if (Exenum instanceof ApplicationExceptionMessage) {
                if (Exenum == ApplicationExceptionMessage.SOCKET_IS_CLOSED) {
                    rException = new Exception("Socket is closed");
                } else if (Exenum == ApplicationExceptionMessage.UNHANDLED_EXCEPTION) {
                    rException = new Exception("Unhandled exception");
                }
            } else if (Exenum instanceof DatabaseExceptionMessage) {
                if (Exenum == DatabaseExceptionMessage.NO_RESULT_DUE_TO_TIMEOUT) {
                    rException = new RuntimeException("No result due to timeout");
                } else if (Exenum == DatabaseExceptionMessage.NOT_ENOUGH_CONNECTIONS) {
                    rException = new RuntimeException("Not enough connections");
                }
            } else if (Exenum instanceof UserExceptionMessage) {
                if (Exenum == UserExceptionMessage.USER_DOES_NOT_EXIST) {
                    rException = new Error("User does not exist");
                } else if (Exenum == UserExceptionMessage.USER_DOES_NOT_HAVE_PERMISSIONS) {
                    rException = new Error("User does not have permissions");
                }
            } else {
                rException = new IllegalArgumentException();
            }
        }else{
            rException = new IllegalArgumentException();
        }
        return rException;
    }

}
