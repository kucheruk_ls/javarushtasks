/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task36.task3606;

import java.io.*;
import java.nio.file.Path;

public class MyClassLoader extends ClassLoader {
    public Path path;
    public MyClassLoader(ClassLoader parent){
        super(parent);


    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {

        //получаем байт код из файла и загружаем класс в рантайм
        Class<?> returnClass = null;
        try{
            byte[] b = fetchClassFromFS(name);
            returnClass = defineClass(null,b,0,b.length);


        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return returnClass;
    }
    /**
     * Взято из www.java-tips.org/java-se-tips/java.io/reading-a-file-into-a-byte-array.html
     */
    private byte[] fetchClassFromFS(String path) throws FileNotFoundException, IOException {
        InputStream is = new FileInputStream(new File(path));

        // Get the size of the file
        long length = new File(path).length();

        if (length > Integer.MAX_VALUE) {
            // File is too large
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[(int)length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+path);
        }

        // Close the input stream and return bytes
        is.close();
        return bytes;

    }
}
