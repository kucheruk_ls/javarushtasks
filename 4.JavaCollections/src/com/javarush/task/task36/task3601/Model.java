/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task36.task3601;

import java.util.List;

public class Model {
    //Controller controller = new Controller();
    Service service = new Service();

    public List<String> getStringDataList() {
        return service.getData();
    }

}
