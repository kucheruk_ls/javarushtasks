/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task26.task2613;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CurrencyManipulatorFactory {
    private static Map<String, CurrencyManipulator> map = new HashMap<>();

    private CurrencyManipulatorFactory(){

    }

    public static CurrencyManipulator getManipulatorByCurrencyCode(String currencyCode){
        currencyCode = currencyCode.toLowerCase();
        if(!map.containsKey(currencyCode)){
            //временно добавлю заполнение манипулятора, а то уже заебался его руками заполнять каждый раз
//            CurrencyManipulator currencyManipulator = new CurrencyManipulator(currencyCode);
//            currencyManipulator.addAmount(500,2);
//            currencyManipulator.addAmount(200,3);
//            currencyManipulator.addAmount(100,3);
//            currencyManipulator.addAmount(50,12);
//            map.put(currencyCode,currencyManipulator);
           map.put(currencyCode, new CurrencyManipulator(currencyCode)); // раскоментить после удаления заполнения манипулятора
            return map.get(currencyCode);
        }else {
            return map.get(currencyCode);
        }
    }
    public static Collection<CurrencyManipulator> getAllCurrencyManipulators(){
       return map.values();

    }
}
