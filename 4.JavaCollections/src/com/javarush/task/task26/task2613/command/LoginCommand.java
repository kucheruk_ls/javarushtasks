/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task26.task2613.command;

import com.javarush.task.task26.task2613.CashMachine;
import com.javarush.task.task26.task2613.ConsoleHelper;
import com.javarush.task.task26.task2613.exception.InterruptOperationException;

import java.util.ResourceBundle;

/**
 * в котором захардкодь номер карточки с пином 123456789012 и 1234 соответственно.
 *
 *  Пока пользователь не введет валидные номер карты и пин - выполнять следующие действия:
 * 4.2. Запросить у пользователя 2 числа - номер кредитной карты, состоящий из 12 цифр, и пин - состоящий из 4 цифр.
 * 4.3. Вывести юзеру сообщение о невалидных данных, если они такими являются.
 * 4.4. Если данные валидны, то проверить их на соответствие захардкоженным (123456789012 и 1234).
 * 4.5. Если данные в п. 4.4. идентифицированы, то сообщить, что верификация прошла успешно.
 * 4.6. Если данные в п. 4.4. НЕ идентифицированы, то вернуться к п.4.1.
 */
public class LoginCommand implements Command {
    private ResourceBundle validCreditCards = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH+"verifiedCards");
    private ResourceBundle res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH+"login_en");
    @Override
    public void execute() throws InterruptOperationException {
//        ConsoleHelper.writeMessage("before");
//////        while (true){
//////            ConsoleHelper.writeMessage(res.getString("specify.data"));
//////            String[] mass = new String[2];
//////                   mass[0] = ConsoleHelper.readString();
//////                   //ConsoleHelper.writeMessage("Введите пин код: ");
//////                   mass[1] = ConsoleHelper.readString();
//////            if(!mass[0].matches("[0-9]{12}")||!mass[1].matches("[0-9]{4}")){
//////                ConsoleHelper.writeMessage(String.format(res.getString("not.verified.format"),mass[0]));
//////            }else if(validCreditCards.containsKey(mass[0])&&validCreditCards.getString(mass[0]).equals(mass[1])){
//////                ConsoleHelper.writeMessage(String.format(res.getString("success.format"),mass[0]));
//////                break;
//////            }
//////        }

        ConsoleHelper.writeMessage(res.getString("before"));
        while (true)
        {
            ConsoleHelper.writeMessage(res.getString("specify.data"));
            String s1 = ConsoleHelper.readString();
            String s2 = ConsoleHelper.readString();
            if (validCreditCards.containsKey(s1))
            {
                if (validCreditCards.getString(s1).equals(s2))
                    ConsoleHelper.writeMessage(String.format(res.getString("success.format"), s1));
                else
                {
                    ConsoleHelper.writeMessage(String.format(res.getString("not.verified.format"), s1));
                    ConsoleHelper.writeMessage(res.getString("try.again.or.exit"));
                    continue;
                }
            }
            else
            {
                ConsoleHelper.writeMessage(String.format(res.getString("not.verified.format"), s1));
                ConsoleHelper.writeMessage(res.getString("try.again.with.details"));
                continue;
            }

            break;
        }

    }
}
