/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task26.task2613.command;

import com.javarush.task.task26.task2613.CashMachine;
import com.javarush.task.task26.task2613.ConsoleHelper;
import com.javarush.task.task26.task2613.CurrencyManipulator;
import com.javarush.task.task26.task2613.CurrencyManipulatorFactory;

import java.util.ResourceBundle;

/**
 *  В InfoCommand в цикле выведите [код валюты - общая сумма денег для выбранной валюты].
 * Запустим прогу и пополним счет на EUR 100 2 и USD 20 6, и посмотрим на INFO.
 *
 * Все работает правильно?
 * EUR - 200
 * USD - 120
 * Отлично!
 */
class InfoCommand implements Command {
    private ResourceBundle res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH+"info_en");
    @Override
    public void execute() {
        ConsoleHelper.writeMessage(res.getString("before"));
        if(CurrencyManipulatorFactory.getAllCurrencyManipulators().isEmpty()){
            ConsoleHelper.writeMessage(res.getString("no.money"));
        }else {
            for (CurrencyManipulator cm : CurrencyManipulatorFactory.getAllCurrencyManipulators()) {
                if(cm.hasMoney()) {
                    ConsoleHelper.writeMessage(cm.getCurrencyCode().toUpperCase() + " - " + cm.getTotalAmount());
                }else{
                    ConsoleHelper.writeMessage(res.getString("no.money"));
                }
            }
        }
    }
}
