/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task26.task2613;

public enum Operation {
    LOGIN,
    INFO,
    DEPOSIT,
    WITHDRAW,
    EXIT;

    /**
     * Должен возвращать элемент энума: для 1 - INFO, 2 - DEPOSIT, 3 - WITHDRAW, 4 - EXIT;
     * На некорректные данные бросать IllegalArgumentException.
     * @param i
     * @return
     */
    public static Operation getAllowableOperationByOrdinal(Integer i){
        switch(i){

            case (1):
                return INFO;

            case(2):
                return DEPOSIT;

            case(3):
                return WITHDRAW;

            case(4):
                return EXIT;

            }
            throw new IllegalArgumentException();
        }
}


