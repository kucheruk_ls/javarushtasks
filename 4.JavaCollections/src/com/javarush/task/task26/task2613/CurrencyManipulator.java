/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task26.task2613;

//import java.util.Collections;
import com.javarush.task.task26.task2613.exception.NotEnoughMoneyException;

import java.util.*;

public class CurrencyManipulator {
    private String currencyCode;
    private Map<Integer, Integer> denominations = new HashMap<>();// - это Map<номинал, количество>.

    public CurrencyManipulator(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void addAmount(int denomination, int count){
        if(denominations.containsKey(denomination)){
            int tmp = denominations.get(denomination);
            tmp = tmp+count;
            denominations.put(denomination,tmp);
        }else {
            denominations.put(denomination, count);
        }
    }

    /**
     *  который посчитает общую сумму денег для выбранной валюты.
     * @return
     */
    public int getTotalAmount(){
        int count = 0;
        for(Map.Entry<Integer,Integer> entry:denominations.entrySet()){
            int nominalSumm = entry.getKey()*entry.getValue();
            count = count+nominalSumm;
        }
        return count;
    }

    /**
     * оторый будет показывать, добавлены ли какие-то банкноты или нет.
     * @return
     */
    public boolean hasMoney(){
        if(denominations.size()==0)return false;
        return true;
    }

    /**
     * который вернет true, если денег достаточно для выдачи. т.е. если денег больше или равно expectedAmount
     * @param expectedAmount
     * @return
     */
    public boolean isAmountAvailable(int expectedAmount){
        if(getTotalAmount()>=expectedAmount)return true;
        return false;
    }

    /**
     * Внимание!!! Метод withdrawAmount должен возвращать минимальное количество банкнот, которыми набирается
     * запрашиваемая сумма.
     * Используйте Жадный алгоритм (use google).
     * Если есть несколько вариантов, то использовать тот, в котором максимальное количество банкнот высшего номинала.
     * Если для суммы 600 результат - три банкноты: 500 + 50 + 50 = 200 + 200 + 200, то выдать первый вариант.
     * @param expectedAmount
     * @return
     */
    public Map<Integer, Integer> withdrawAmount(int expectedAmount) throws NotEnoughMoneyException{

        SortedMap<Integer, Integer> list = new TreeMap(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2-o1;
            }
        });
        list.putAll(denominations);
        //тестовый вывод!
//        System.out.println("Сортируем номиналы!");
//        for(Map.Entry<Integer, Integer> entry:list.entrySet()){
//            ConsoleHelper.writeMessage(entry.getKey()+" "+entry.getValue());
//        }
        //------------------------------------
        //HashMap<Integer, Integer> resultMap = new HashMap<>();
        TreeMap<Integer, Integer> resultMap = new TreeMap<>(Comparator.reverseOrder());
        int summNabrTmp = 0;//пробуем не будет ли сумма больше, в процессе набора
        int summNabr = 0; //набранная сумма
        int expAmountTmp = expectedAmount;
        boolean otdano = false;
        for(Map.Entry<Integer, Integer> entry:list.entrySet()){//обходим мапу и жадным методом набираем нужную сумму
           Integer nominal = entry.getKey();
           for(int i=0;i<entry.getValue();i++){
               summNabrTmp=summNabrTmp+nominal;
               if(summNabrTmp==expAmountTmp){
                   resultMap.put(nominal,i+1);
                   for(Map.Entry<Integer, Integer> entry2:resultMap.entrySet()){
                       Integer collCup = denominations.get(entry2.getKey())-entry2.getValue();
                       if(collCup!=0) {
                           denominations.put(entry2.getKey(), collCup);
                       }else if(collCup==0){
                           denominations.remove(entry2.getKey());
                       }
                   }
                   return resultMap;
               }else if(summNabrTmp>expAmountTmp){
                   if(i!=0) {
                       summNabrTmp = summNabrTmp - nominal;
                       resultMap.put(nominal, i);
                       otdano = true;
                   }else {
                       summNabrTmp = summNabrTmp - nominal;
                       otdano = true;
                   }
                   break;
               }

           }
           if(!otdano)
           resultMap.put(nominal,entry.getValue());
           otdano=false;
        }

        //распечатаем нашу получившуюся карту
//        System.out.println("распечатаем что набрали!");
//        for(Map.Entry<Integer, Integer> entry : resultMap.entrySet()){
//            System.out.println(entry.getKey()+" "+entry.getValue());
//        }

        if(summNabr!=expectedAmount){

            throw new NotEnoughMoneyException();
        }

        for(Map.Entry<Integer, Integer> entry:resultMap.entrySet()){
            Integer collCup = denominations.get(entry.getKey())-entry.getValue();
            if(collCup!=0) {
                denominations.put(entry.getKey(), collCup);
            }else if(collCup==0){
                denominations.remove(entry.getKey());
            }
        }
        return resultMap;
    }

//    private void spisDenominations(Map<Integer, Integer> map){
//        for(Map.Entry<Integer, Integer> entry:map.entrySet()){
//            Integer collCup = denominations.get(entry.getKey())-entry.getValue();
//            if(collCup!=0) {
//                denominations.put(entry.getKey(), collCup);
//            }else if(collCup==0){
//                denominations.remove(entry.getKey());
//            }
//        }
//    }
}
