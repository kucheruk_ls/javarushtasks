/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task26.task2613;

import com.javarush.task.task26.task2613.exception.InterruptOperationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ResourceBundle;

public class ConsoleHelper {
    private static ResourceBundle res = ResourceBundle.getBundle(CashMachine.class.getPackage().getName()+".resources.common_en");
   // private static ResourceBundle res = ResourceBundle.getBundle(CashMachine.class.getPackage().getName()+".resources.common_en");
    private static BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));

    public static void writeMessage(String message){
        System.out.println(message);
    }

    public static String readString() throws InterruptOperationException {
        String str = "";
        try {
            str = bis.readLine();
        }catch (IOException e){

        }
        if (str.equalsIgnoreCase("exit")) {
            throw new InterruptOperationException();
        }

        return str;
    }

    /**
     * Этот метод должен предлагать пользователю ввести код валюты, проверять, что код содержит 3 символа.
     * Если данные некорректны, то сообщить об этом пользователю и повторить.
     * Если данные валидны, то перевести код в верхний регистр и вернуть.
     * @return
     */
    public static String askCurrencyCode() throws InterruptOperationException {


        while (true){

            writeMessage(res.getString("choose.currency.code"));
            String currencyCode = null;

                currencyCode = readString();


            if(currencyCode.length()==3){
                currencyCode = currencyCode.toUpperCase();
                return currencyCode;
            }else{
                writeMessage(res.getString("invalid.data"));
            }


        }
    }

    /**
     * Этот метод должен предлагать пользователю ввести два целых положительных числа.
     * Первое число - номинал, второе - количество банкнот.
     * Никаких валидаторов на номинал нет. Т.е. 1200 - это нормальный номинал.
     * Если данные некорректны, то сообщить об этом пользователю и повторить.
     * @param currencyCode
     * @return
     */
    public static String[] getValidTwoDigits(String currencyCode) throws InterruptOperationException {
        while (true){
            writeMessage(res.getString("choose.denomination.and.count.format")+currencyCode);
            String str = "";

                str = readString();


                boolean strOne = str.matches("[0-9]+\\s[0-9]+");
                //boolean strTwo = count.matches("[0-9]+");
                if(strOne){
                    String[] resul = str.split(" ");
                    return resul;
                }else{
                    writeMessage(res.getString("invalid.data"));
                }

        }
    }

    /**
     * Спросить у пользователя операцию.
     * Если пользователь вводит 1, то выбирается команда INFO, 2 - DEPOSIT, 3 - WITHDRAW, 4 - EXIT;
     * Используйте метод, описанный в п.1.
     * Обработай исключение - запроси данные об операции повторно.
     * @return
     */
    public static Operation askOperation() throws InterruptOperationException {

        Operation retOp = null;
        while (true){
            writeMessage(res.getString("choose.operation")+" 1 - "+res.getString("operation.INFO")+", 2 - "+res.getString("operation.DEPOSIT")+", 3 - "+res.getString("operation.WITHDRAW")+", 4 - "+res.getString("operation.EXIT"));
            String operation = "";

            try{
                operation = readString();
                retOp = Operation.getAllowableOperationByOrdinal(Integer.parseInt(operation));
                break;
            }catch (IllegalArgumentException e){
                writeMessage(res.getString("invalid.data"));
            }

        }
        return retOp;
    }

    public static void printExitMessage(){
        writeMessage(res.getString("the.end"));
    }
}
