/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task37.task3708.retrievers;

import com.javarush.task.task37.task3708.cache.LRUCache;
import com.javarush.task.task37.task3708.storage.Storage;

public class CachingProxyRetriever implements Retriever {
    LRUCache lruCache = new LRUCache(16);
    OriginalRetriever originalRetriever;
    Storage storage;

    public CachingProxyRetriever(Storage storage){
       this.storage = storage;
       this.originalRetriever = new OriginalRetriever(storage);
    }


    @Override
    public Object retrieve(long id) {
        if(lruCache.find(id)==null){
            lruCache.set(id,originalRetriever.retrieve(id));
            return lruCache.find(id);
        }else{
            return lruCache.find(id);
        }

    }
}
