/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task37.task3710.decorators;


import com.javarush.task.task37.task3710.shapes.Shape;

public class RedShapeDecorator extends ShapeDecorator {
        protected Shape decoratorShape;

    public RedShapeDecorator(Shape shape){
        this.decoratorShape = shape;
    }

    /**
     * Он должен выводить на экран фразу "Setting border color for XXX to red.", где XXX - имя конкретного декорируемого
     * класса (можешь воспользоваться методами getClass().getSimpleName() вызванными на объекте полученном в качестве
     * параметра).
     * @param shape
     */
        private void setBorderColor(Shape shape){
            System.out.println("Setting border color for "+shape.getClass().getSimpleName()+" to red.");
        }

    /**
     * сначала измени цвет отображаемого объекта с помощью метода setBorderColor(), а потом нарисуй его.
     */
    public void draw() {
        setBorderColor(decoratorShape);
        decoratorShape.draw();
    }


}
