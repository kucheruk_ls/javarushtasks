/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task37.task3709.connectors;
import com.javarush.task.task37.task3709.security.SecurityChecker;
import com.javarush.task.task37.task3709.security.SecurityCheckerImpl;

public class SecurityProxyConnector implements Connector {
    SimpleConnector connector;
    SecurityChecker securityChecker;


    public SecurityProxyConnector(String ConnString) {
          this.connector = new SimpleConnector(ConnString);
          this.securityChecker = new SecurityCheckerImpl();

    }

    @Override
    public void connect() {
        if(securityChecker.performSecurityCheck()){
            connector.connect();
        }
    }
}
