package com.javarush.task.task37.task3707;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class AmigoSet<E> extends AbstractSet implements Set, Serializable, Cloneable {
    private static final Object PRESENT = new Object();
    private transient HashMap<E,Object> map;


    public AmigoSet() {
        this.map = new HashMap<E, Object>();
    }

    public AmigoSet(Collection<? extends E> collection ){
        this.map = new HashMap(Math.max(16, (int) Math.ceil(collection.size()/.75f)));
        for(E e: collection){
            map.put(e,PRESENT);
        }
    }

    /**
     * сериализация
     *
     */
    private void writeObject(ObjectOutputStream objectOutputStream){
        try{
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeInt(HashMapReflectionHelper.callHiddenMethod(map,"capacity"));
            objectOutputStream.writeFloat(HashMapReflectionHelper.callHiddenMethod(map, "loadFactor"));
            objectOutputStream.writeInt(map.keySet().size());
            //objectOutputStream.writeInt(HashMapReflectionHelper.callHiddenMethod(map,"size"));
            for(E e:map.keySet()){
                objectOutputStream.writeObject(e);
            }

        }catch (IOException e){
            e.printStackTrace();
        }

    }

    /**
     *
     * десериализация
     */
    private void readObject(ObjectInputStream objectInputStream){
        try{
            objectInputStream.defaultReadObject();
            int capacity = objectInputStream.readInt();
            float loadFactor = objectInputStream.readFloat();
            int size = objectInputStream.readInt();
            map = new HashMap<>(capacity,loadFactor);

            for(int i=0;i<size;i++){
                map.put((E) objectInputStream.readObject(), PRESENT);
            }


        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    @Override
    public Spliterator spliterator() {
        return null;
    }

    @Override
    public boolean removeIf(Predicate filter) {
        return false;
    }

    @Override
    public Stream stream() {
        return null;
    }

    @Override
    public Stream parallelStream() {
        return null;
    }

    @Override
    public void forEach(Consumer action) {

    }


    @Override
    public int hashCode() {
        return this.map.hashCode()*31+PRESENT.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if ((o == null)||!(o instanceof AmigoSet )) return false;

        if (this.hashCode()!= ((AmigoSet)o).hashCode()) return false;
        AmigoSet<E> compare = (AmigoSet)o;
        if (this.map.size()!= compare.map.size()) return false;
        for (E e: map.keySet()){
            if (!compare.contains(e)) return false;
        }
        return true;
    }

    @Override
    public Object clone() throws InternalError {

        try {
            AmigoSet<E> amigoSet = (AmigoSet<E>) super.clone();
            amigoSet.map = (HashMap) map.clone();
            /*
            for(Map.Entry<E,Object> entry: map.entrySet()){

                amigoSet.map.put(entry.getKey(),entry.getValue());
            }
            */
            return amigoSet;
        }catch (Exception e){
            throw new InternalError(e);
        }
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override

    /**
     * это количество ключей в map, равно количеству элементов в map
     */
    public int size() {
        return map.size();
    }

    @Override
    /**
     * должен возвращать true, если map не содержит ни одного элемента, иначе - false.
     */
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    /**
     * contains должен возвращать true, если map содержит анализируемый элемент, иначе - false.
     */
    public boolean contains(Object o) {
        if(map.containsKey(o)){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public Iterator iterator() {
        return map.keySet().iterator();
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }


    /**
     * Напиши свою реализацию для метода метод add(E e): добавь в map элемент 'e'
     * в качестве ключа и PRESENT в качестве значения.
     *
     * Верни true, если был добавлен новый элемент, иначе верни false.
     */

    public boolean add(Object o){


          return   map.put((E) o,PRESENT)==null;

    }






    @Override
    /**
     * remove должен удалять из map полученный в качестве параметра элемент.
     */
    public boolean remove(Object o) {
        return map.remove(o)==PRESENT;

    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    /**
     * должен вызывать метод clear объекта map.
     */
    public void clear() {
        map.clear();
    }
}
