package com.javarush.task.task31.task3109;

import java.io.*;
import java.nio.file.Paths;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

/* 
Читаем конфиги чтение конфигурационного файла properties
*/
public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        Properties properties = solution.getProperties("4.JavaCollections/src/com/javarush/task/task31/task3109/properties.xml");
        properties.list(System.out);

        properties = solution.getProperties("4.JavaCollections/src/com/javarush/task/task31/task3109/properties.txt");
        properties.list(System.out);

        properties = solution.getProperties("4.JavaCollections/src/com/javarush/task/task31/task3109/notExists");
        properties.list(System.out);
    }

    public Properties getProperties(String fileName) {
        //String fileNameShort = fileName.substring(fileName.lastIndexOf(File.separator));
        Properties properties = new Properties();
        File file = new File(fileName);
        try{


            if(fileName.endsWith(".xml")){
                FileInputStream fileInputStream = new FileInputStream(file);
                properties.loadFromXML(fileInputStream);
                fileInputStream.close();
            }else if(fileName.endsWith(".txt")){
                FileReader fileReader = new FileReader(file);
                properties.load(fileReader);
                fileReader.close();
            }else{
                FileInputStream fileInputStream = new FileInputStream(file);
                properties.load(fileInputStream);
                fileInputStream.close();
            }
        }catch (Exception e){
            properties = new Properties();
        }

        return properties;
    }
}
