package com.javarush.task.task31.task3113;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/* 
Что внутри папки?
работа с Files.w
*/
public class Solution {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Path path = Paths.get(reader.readLine());
        reader.close();
        if(!Files.isDirectory(path)){
            System.out.println(path.getParent().toString()+path.getFileName()+" - не папка");
        }
        MyFileVisitor myFileVisitor = new MyFileVisitor();
        Files.walkFileTree(path,myFileVisitor);

        System.out.println("Всего папок - "+myFileVisitor.getSummDir());
        System.out.println("Всего файлов - "+myFileVisitor.getSummFiles());
        System.out.println("Общий размер - "+myFileVisitor.getSummLength());

    }
}
