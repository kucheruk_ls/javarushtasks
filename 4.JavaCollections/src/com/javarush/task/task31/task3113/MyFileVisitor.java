package com.javarush.task.task31.task3113;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class MyFileVisitor extends SimpleFileVisitor<Path>  {
    private int summDir = -1; // всего папок
    private int summFiles = 0; // всего файлов
    private long summLength = 0; //общий размер файлов
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

        if(Files.isRegularFile(file)){
            summFiles++;
            summLength+= Files.size(file);
        }



        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path path, BasicFileAttributes attrs) throws IOException {
        if(Files.isDirectory(path)){
            summDir++;
        }

        return FileVisitResult.CONTINUE;
    }

    public int getSummDir() {
        return summDir;
    }

    public int getSummFiles() {
        return summFiles;
    }

    public long getSummLength() {
        return summLength;
    }
}
