package com.javarush.task.task31.task3111;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class SearchFileVisitor extends SimpleFileVisitor<Path> {
    private String partOfName;
    private String partOfContent;
    private int minSize = 0;
    private int maxSize = 0;
    private List<Path> foundFiles = new ArrayList<>();

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        byte[] content = Files.readAllBytes(file); // размер файла: content.length
        Path candidat = file;
        byte[] candidatByte = Files.readAllBytes(file);
        if(partOfName!=null){ // проверяем содержит ли искомую строку заголовок файла
            if(!file.toString().contains(partOfName)) {
                candidat = null;
            }

        }
        if(partOfContent!=null&&!new String(candidatByte).contains(partOfContent)){
            candidat = null;
        }
        if(minSize>0) {
            if(candidatByte.length < minSize){
                candidat = null;
            }
        }if(maxSize > 0){
            if(candidatByte.length>maxSize){
                candidat = null;
            }
        }

        if(candidat!=null){
            foundFiles.add(candidat);
        }


        return FileVisitResult.CONTINUE;
    }

    public void setPartOfName(String partOfName) {
        this.partOfName = partOfName;
    }

    public void setPartOfContent(String partOfContent) {
        this.partOfContent = partOfContent;
    }

    public void setMinSize(int minSize) {
        this.minSize = minSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public List<Path> getFoundFiles(){
        return this.foundFiles;
    }
}
