package com.javarush.task.task31.task3106;


import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.TreeSet;
import java.util.zip.ZipInputStream;




/*
Разархивируем файл многотомный архив разсборка многотомного архива
*/
public class Solution {
    public static void main(String[] args) {
        //соберем файлы архива и отсортируем
        TreeSet<String> zipFileNameList = new TreeSet<>();
        for(int i=1;i<args.length;i++){
            zipFileNameList.add(args[i]);
        }
        //создадим список потоков
        ArrayList<FileInputStream> inputStreams = new ArrayList<>();

        //создадим потоки чтения для каждого файла в списке
        for(String s:zipFileNameList){
            try{
                inputStreams.add(new FileInputStream(s));
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        //создадим поток записи в результирующий файл
        try{
            FileOutputStream outputStream = new FileOutputStream(args[0]);

            Enumeration enumeration = Collections.enumeration(inputStreams);
            SequenceInputStream sequenceInputStream = new SequenceInputStream(enumeration);
            ZipInputStream zipInputStream = new ZipInputStream(sequenceInputStream);

            byte[] buffer = new byte[1024*1024];
            while (zipInputStream.getNextEntry()!=null){
                int count;
                while ((count = zipInputStream.read(buffer))!=-1){
                    outputStream.write(buffer,0,count);
                }
            }


            zipInputStream.close();
            outputStream.flush();
            outputStream.close();




        }catch (IOException e){
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }





    }


}
