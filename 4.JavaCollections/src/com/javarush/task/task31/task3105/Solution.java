package com.javarush.task.task31.task3105;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/* 
Добавление файла в архив
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        String args1 = args[0];//путь к файлу
        String args2 = args[1];//путь к архиву

        //создаем временный файл
        Path tempZipFile = Files.createTempFile(null,null);
        //List<Path> arhiveFiles = new ArrayList<>();

        //формируем путь к новому архивному файлу
        Path pathFile = Paths.get(args1);
        Path newArhivePath = Paths.get("new/"+pathFile.getFileName().toString());

        //вычитаем все файлы из архива во временный файл
        //поток для записи во временный файл
        ZipOutputStream zipOutputStream = new ZipOutputStream(Files.newOutputStream(tempZipFile));
        //поток для чтения архива
        ZipInputStream zipInputStream = new ZipInputStream(Files.newInputStream(Paths.get(args2)));
        //непосредственно вычитка
        ZipEntry zipEntry = zipInputStream.getNextEntry();
        while (zipEntry!=null) {
            String fileName = zipEntry.getName();//получаем имя файла
            //arhiveFiles.add(Paths.get(fileName));//сохраняем путь и имя файла в список файлов
            //Path zipDirAndFile = Paths.get(zipEntry.getName())

            if(!Paths.get(fileName).equals(newArhivePath)) {
                zipOutputStream.putNextEntry(new ZipEntry(fileName)); //создаем Entry для потока записи и помещаем туда новый ZipEntry
                //вычитали все во временный файл.
                byte[] buffer = new byte[8 * 1028];
                int len;
                while ((len = zipInputStream.read(buffer)) > 0) {
                    zipOutputStream.write(buffer, 0, len);
                }


                zipInputStream.closeEntry();
                zipOutputStream.closeEntry();
            }
            zipEntry = zipInputStream.getNextEntry();

        }
        //закончили вычитку во временный файл теперь добавим туда наш файл




        InputStream inputStream = Files.newInputStream(pathFile);
        ZipEntry entry = new ZipEntry(newArhivePath.toString());

        zipOutputStream.putNextEntry(entry);
        Files.copy(pathFile,zipOutputStream);
        /*
        byte[] buffer = new byte[8 * 1024];
        int len;
        while ((len = inputStream.read(buffer)) > 0) {
            zipOutputStream.write(buffer, 0, len);
        }
*/
        zipOutputStream.closeEntry();
        zipInputStream.closeEntry();

        inputStream.close();

        zipOutputStream.close();
        zipInputStream.close();

        //теперь скопируем временный файл на место нашего.
        Files.delete(Paths.get(args2));
        Files.move(tempZipFile,Paths.get(args2));
    }
}
