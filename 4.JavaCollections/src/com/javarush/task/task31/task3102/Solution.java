package com.javarush.task.task31.task3102;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/* 
Находим все файлы
обход директорий папок и вложенных директори папок и извлечение всех файлов в список с использованием очереди
*/
public class Solution {
    public static List<String> getFileTree(String root) throws IOException {

        ArrayList<String> list = new ArrayList<>();
        Queue<File> queue = new PriorityQueue<>();



        File file = new File(root);
        queue.offer(file);

        while (queue.size()>0){
            File f = queue.poll();
            if(f==null)break;
            for(File fi:f.listFiles()) {
                if (fi.isDirectory()) {
                    queue.offer(fi);
                } else {
                    list.add(fi.getAbsolutePath());
                }
            }
        }






        return list;

    }

    public static void main(String[] args) throws IOException {
        ArrayList<String> list = (ArrayList<String>) getFileTree("c:\\1");


        for(String s:list){
            System.out.println(s);
        }

    }


}
