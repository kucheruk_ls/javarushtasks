package com.javarush.task.task31.task3107;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/*
Null Object Pattern
*/
public class Solution {
    private FileData fileData;

    public Solution(String pathToFile) {
        Path pathToFileP = Paths.get(pathToFile);
            try{
                this.fileData = new ConcreteFileData(Files.isHidden(pathToFileP), Files.isExecutable(pathToFileP),Files.isDirectory(pathToFileP),Files.isWritable(pathToFileP));
            }catch (Exception e){
                NullFileData nullFileData = new NullFileData(e);
                this.fileData = nullFileData;
            }

    }

    public FileData getFileData() {
        return fileData;
    }
}
