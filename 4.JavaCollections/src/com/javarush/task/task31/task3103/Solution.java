package com.javarush.task.task31.task3103;

import java.io.IOException;//+
import java.nio.charset.Charset;//+
import java.nio.file.Files;//+
import java.nio.file.Paths;//+
import java.util.List;//+

/* 
Своя реализация
*/
public class Solution {
    /**
     * должен возвращать все байты файла fileName.
     * @param fileName
     * @return
     * @throws IOException
     */
    public static byte[] readBytes(String fileName) throws IOException {

        byte[] b = Files.readAllBytes(Paths.get(fileName));
        return b;
    }

    /**
     * должен возвращать все строки файла fileName. Используй кодировку по умолчанию.
     * @param fileName
     * @return
     * @throws IOException
     */
    public static List<String> readLines(String fileName) throws IOException {
        List<String> list = Files.readAllLines(Paths.get(fileName));
        return list;
    }

    /**
     * должен записывать массив bytes в файл fileName.
     * @param fileName
     * @param bytes
     * @throws IOException
     */
    public static void writeBytes(String fileName, byte[] bytes) throws IOException {
        Files.write(Paths.get(fileName),bytes);
    }

    /**
     * должен копировать файл resourceFileName на место destinationFileName.
     * @param resourceFileName
     * @param destinationFileName
     * @throws IOException
     */
    public static void copy(String resourceFileName, String destinationFileName) throws IOException {
        Files.copy(Paths.get(resourceFileName),Paths.get(destinationFileName));


    }
}
