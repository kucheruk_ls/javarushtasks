package com.javarush.task.task20.task2028;

import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

/* 
Построй дерево(1)
*/
public class CustomTree extends AbstractList<String> implements Cloneable, Serializable {
    Entry<String> root;
    private ArrayList<Entry> listTree = new ArrayList<>();

    public CustomTree() {
        this.root = new Entry<>("elementeName");
        this.listTree.add(root);
    }


    @Override
    public String get(int index) {
       throw new UnsupportedOperationException("хуякс");
    }

    public String set(int index, String element){
        throw new UnsupportedOperationException();
    }


    public void add(int index, String element){
        throw new UnsupportedOperationException("хуякс2");
    }

    public String remove(int index){
        throw new UnsupportedOperationException();
    }

    public List<String> subList(int fromIndex, int toIndex){
        throw new UnsupportedOperationException();
    }

    protected void removeRange(int fromIndex, int toIndex){
        throw new UnsupportedOperationException();
    }

    public boolean addAll(int index, Collection<? extends String> c){
        throw new UnsupportedOperationException();
    }


    @Override
    public int size() {
        int countElement = 0;
        for(int i=0;i<listTree.size();i++){
            if(listTree.get(i)!=null)countElement++;
        }
        return countElement-1;
    }

    @Override
    public void replaceAll(UnaryOperator<String> operator) {

    }

    @Override
    public void sort(Comparator<? super String> c) {

    }

    @Override
    public Spliterator<String> spliterator() {
        return null;
    }

    @Override
    public boolean removeIf(Predicate<? super String> filter) {
        return false;
    }

    @Override
    public Stream<String> stream() {
        return null;
    }

    @Override
    public Stream<String> parallelStream() {
        return null;
    }

    @Override
    public void forEach(Consumer<? super String> action) {

    }

    public String getParent(String s){

        for(int i=0;i<listTree.size();i++) {
            if(listTree.get(i)!=null) {
                if (listTree.get(i).elementName.equals(s))
                    return listTree.get(i).parent.elementName;
            }
        }
        return null;
    }


    /**
     * добавляет элементы дерева, в качестве параметра принимает имя элемента (elementName),
     * искать место для вставки начинаем слева направо.
     */
    @Override
    public boolean add(String s) {
        boolean addresult = false;
        Entry<String> newEntry = new Entry<>(s);//создадим новый узел

        for(int i=0;i<listTree.size();i++){//будем обходить дерево и искать свободное место для добавления узла
if(listTree.get(i)!=null) {
    listTree.get(i).checkChildren();//обновим статусы возможности иметь потомков у узла

    if (listTree.get(i).availableToAddLeftChildren) {//если нет левого потомка
        listTree.get(i).leftChild = newEntry;
        newEntry.parent = listTree.get(i);
        listTree.add(newEntry);
        addresult = true;
        break;
    } else if (listTree.get(i).availableToAddRightChildren) {//видимо левый узел занят проверим правый
        listTree.get(i).rightChild = newEntry;
        newEntry.parent = listTree.get(i);
        //listTree.add(null);
        listTree.add(newEntry);
        addresult = true;
        break;

    }
}
        }
        return addresult;
    }

    @Override
    public boolean remove(Object o) {
        boolean removeResult = false;
        if(!(o instanceof String)){ //если о не String выбросим исключение
         throw new UnsupportedOperationException();
        }
        String removeEntryName = ((String) o);
        Entry<String> removeEntry = null;
        //найдем узел который нужно удалить
        for(int j=0;j<listTree.size();j++){
         if(listTree.get(j)!=null) {
             if (listTree.get(j).elementName.equals(removeEntryName)) {
                 removeEntry = listTree.get(j);
                 break;
             }
         }
        }
        //нам нужно удалить не тольоко узел но и всех его потомков. Соберем потомков в список
        ArrayList<Entry<String>> childRemoveList = new ArrayList<>();
        childRemoveList.add(removeEntry);
        for(int i=0;i<listTree.size();i++){

if(listTree.get(i)!=null) {
    if (childRemoveList.contains(listTree.get(i).parent)) {
        childRemoveList.add(listTree.get(i));
    }
}

        }
        //Удалим все узлы из списка
        for(int j=listTree.size()-1;j>0;j--){
            if(childRemoveList.contains(listTree.get(j))){
                Entry<String> remParent = listTree.get(j).parent;
                if(remParent.leftChild==listTree.get(j)){
                    remParent.leftChild=null;
                }else{
                    remParent.rightChild=null;
                }
                listTree.set(j,null);
                remParent.checkChildren();


            }
        }

        //попробуем почекать потомков.
        for(int i = 0;i<listTree.size();i++){
            if(listTree.get(i)!=null)listTree.get(i).checkChildren();
        }

        return removeResult;
    }

    static class Entry<T> implements Serializable{
        String elementName;//имя элемента
        int lineNumber; // номер линии
        boolean availableToAddLeftChildren, availableToAddRightChildren; //можно добавить дочернюю ветвь?
        Entry<T> parent, leftChild, rightChild;


        public Entry(String string){
            this.elementName = string;
            this.availableToAddLeftChildren = true;
            this.availableToAddRightChildren = true;

        }

        /**
         * устанавливающий поле availableToAddLeftChildren в false,
         * если leftChild не равен null и availableToAddRightChildren в false, если rightChild не равен null.
         */
        public void checkChildren(){
            if(leftChild!=null)availableToAddLeftChildren=false;
            else availableToAddLeftChildren=true;
            if(rightChild!=null)availableToAddRightChildren=false;
            else availableToAddRightChildren=true;
        }

        /**
         * возвращающий дизъюнкцию полей availableToAddLeftChildren и availableToAddRightChildren.
         * @return
         */
        public boolean isAvailableToAddChildren(){
            return (availableToAddLeftChildren||availableToAddRightChildren);
        }


    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for(Entry<String> entry:listTree){
            if(entry!=null) {
                sb.append("[узел:"+entry.elementName+", родитель:");
                sb.append(entry.parent!=null? entry.parent.elementName:"null");
                sb.append(", левый чилдрен:");
                sb.append(entry.leftChild!=null? entry.leftChild.elementName:"null");
                sb.append(", правый чилдрен:");
                sb.append(entry.rightChild!=null? entry.rightChild.elementName: "null");
                sb.append(", возможно добавить левого?"+entry.availableToAddLeftChildren+", возможно доабвить правого?"+entry.availableToAddRightChildren+"]");
            }else{
                sb.append("null");
            }
        }


        return sb.toString();
    }
}
