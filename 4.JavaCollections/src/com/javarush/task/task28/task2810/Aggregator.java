/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task28.task2810;


import com.javarush.task.task28.task2810.model.HHStrategy;
import com.javarush.task.task28.task2810.model.Model;
import com.javarush.task.task28.task2810.model.MoikrugStrategy;
import com.javarush.task.task28.task2810.model.Provider;
import com.javarush.task.task28.task2810.view.HtmlView;
import com.javarush.task.task28.task2810.view.View;

import java.io.IOException;
import java.util.HashSet;
//import java.util.ArrayList;

public class Aggregator {
    public static void main(String[] args) throws IOException {
        View view = new HtmlView();
        Provider[] providers = new Provider[]{new Provider(new HHStrategy()),new Provider(new MoikrugStrategy())};

        Model model = new Model(view, providers);
        Controller controller = new Controller(model);
        view.setController(controller);
        ((HtmlView) view).userCitySelectEmulationMethod();

//        View view1 = new HtmlView();
//        Model model1 = new Model(view1,new Provider(new MoikrugStrategy()));
//        Controller controller1 = new Controller(model1);
//        view1.setController(controller1);
//        ((HtmlView) view1).userCitySelectEmulationMethod();


    }

}
