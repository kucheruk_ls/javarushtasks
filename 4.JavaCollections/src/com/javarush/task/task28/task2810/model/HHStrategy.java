/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task28.task2810.model;

import com.javarush.task.task28.task2810.vo.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Этот класс будет реализовывать конкретную стратегию работы с сайтом ХэдХантер (http://hh.ua/ и http://hh.ru/).
 */
public class HHStrategy implements Strategy {
    private static final String URL_FORMAT = "http://hh.ua/search/vacancy?text=java+%s&page=%d";
    //private static final String URL_FORMAT = "http://javarush.ru/testdata/big28data.html";

    @Override
    public List<Vacancy> getVacancies(String searchString) {

        ArrayList<Vacancy> vacansies = new ArrayList<>();
        int count = 0;
        while (count>-1) {
            Document html = null;
            try {
                html = getDocument(searchString, count);
            } catch (IOException e) {

            }
            ArrayList<Element> tagList = html.body().getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy");
            if(tagList.size()>0){
                count++;
            }else{
                count=-1;
            }
//            class Vacancy {
//                private String title,
//                        salary,
//                        city,
//                        companyName,
//                        siteName,
//                        url;
            for (Element e : tagList) {
                //System.out.println(e);
                Vacancy vacancy = new Vacancy();
                vacancy.setTitle(e.getElementsByAttributeValueContaining("data-qa","vacancy-serp__vacancy-title").text());

                String salary = e.getElementsByAttributeValueContaining("data-qa","compensation").text();
                vacancy.setSalary(salary.length()==0?"":salary);
               // e.getElementsByAttributeValueContaining("date-qa","salary")==null?vacancy.setSalary("Не указана!"):vacancy.setSalary(e.getElementsByAttributeValueContaining("date-qa", "salary").text());;
                vacancy.setCity(e.getElementsByAttributeValueContaining("data-qa","address").text());
                vacancy.setCompanyName(e.getElementsByAttributeValueContaining("data-qa","employer").text().trim());
                vacancy.setSiteName(URL_FORMAT);
                String url = e.getElementsByAttributeValueContaining("data-qa","title").attr("href");
                vacancy.setUrl(url);
                vacansies.add(vacancy);
            }
        }
        return vacansies;
    }

    protected Document getDocument(String searchString, int page) throws IOException{
        Document html = Jsoup.connect(String.format(URL_FORMAT,searchString,page)).userAgent("Mozilla").referrer("no-referrer-when-downgrade").get();
        return html;
    }
}
