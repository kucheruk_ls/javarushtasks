/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task28.task2810.model;

import com.javarush.task.task28.task2810.view.View;
import com.javarush.task.task28.task2810.vo.Vacancy;

import java.util.ArrayList;
import java.util.List;

public class Model {
    View view;
    Provider[] providers;

    public Model(View view, Provider... providers) {

        if(view==null||providers==null||providers.length==0){
            throw new IllegalArgumentException();
        }else {
            this.view = view;
            this.providers = providers;
        }


    }
    public void selectCity(String city){
        List<Vacancy> vlist = new ArrayList<>();
        for(Provider p:providers){
            //p.getJavaVacancies(city);
            vlist.addAll(p.getJavaVacancies(city));
        }
        view.update(vlist);
    }


}
