/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task28.task2810.view;

import com.google.common.base.Utf8;
import com.javarush.task.task28.task2810.Controller;
import com.javarush.task.task28.task2810.vo.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class HtmlView implements View {
    Controller controller;
    /**
     * присвой ему относительный путь к vacancies.html.
     * Путь должен быть относительно корня проекта.
     * Формируй путь динамически используя this.getClass().getPackage() и разделитель "/".
     *
     * Подсказка: путь должен начинаться с "./".
     */
    private final String filePath = "./4.JavaCollections/src/"+this.getClass().getPackage().getName().replace(".","/")+"/vacancies.html";
    //String s = filePath;

    @Override
    public void update(List<Vacancy> vacancies) {

        try{
            updateFile(getUpdatedFileContent(vacancies));
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void userCitySelectEmulationMethod(){
        controller.onCitySelect("Москва");
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    private String getUpdatedFileContent(List<Vacancy> vacancies){
        Document document = null;
        try{
            document = getDocument();
            //выбираем элемент содержащим template в class



        }catch (IOException e){
            e.printStackTrace();
            return "Some exception occurred";
        }
        Element PatternElement = document.getElementsByClass("template").first();
        Element CloneElement = PatternElement.clone();
        CloneElement.removeClass("template");
        CloneElement.removeAttr("style");
        document.getElementsByAttributeValue("class","vacancy").remove();//jsoup удалим елемент документа по значениям атрибуттов
        //System.out.println(document);

        for(Vacancy v:vacancies){
            Element newElementV = CloneElement.clone();
            newElementV.getElementsByClass("city").first().text(v.getCity());
            newElementV.getElementsByClass("companyName").first().text(v.getCompanyName());
            newElementV.getElementsByClass("salary").first().text(v.getSalary());
            //newElementV.getElementsByClass("title").first().text(v.getTitle());
            //тег с атрр class равным title и содержащим аттр а получим в отдельный элемент
            Element link = newElementV.getElementsByTag("a").first();
            //установим значение тега а
            link.text(v.getTitle());
            // и установим аттр href тега а
            link.attr("href",v.getUrl());
            PatternElement.before(newElementV);
        }

        System.out.println(document);
        return document.html();
    }

    private void updateFile(String file) throws IOException {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filePath))){
            bufferedWriter.write(file);
            bufferedWriter.flush();
        }
    }

    protected Document getDocument() throws IOException{
        //String filePath2 = "C:\\Users\\Admin\\Documents\\JavaRushProject\\JavaRushTasks\\4.JavaCollections\\src\\com\\javarush\\task\\task28\\task2810\\view\\vacancies.html";
        File file = new File(filePath);
        //парсим файл с помощью jsoup
        Document document = Jsoup.parse(file,"UTF-8");

        return document;
    }
}
