/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task34.task3410.model;

import java.awt.*;

public class Player  extends CollisionObject implements Movable {

    public Player(int x, int y) {
        super(x, y);
    }

    @Override
    /**
     * Он должен смещать координаты объектов (x и y) на переданные значения.
     */
    public void move(int x, int y) {
        this.setX(this.getX()+x);
        this.setY(this.getY()+y);
    }

    /**
     * В каждом из них, реализуй метод, отвечающий за отрисовку. Этот метод должен: устанавливать какой-то цвет и
     * рисовать какие-то примитивные фигуры. Проследи, чтобы центр фигуры имел координаты x и y, а высота и ширина соответствовали
     * значениям полей height и width.
     * @param graphics
     */
    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(Color.YELLOW);

        int leftUpperCornerX = getX() - getWidth() / 2;
        int leftUpperCornerY = getY() - getHeight() / 2;

        graphics.drawOval(leftUpperCornerX, leftUpperCornerY, getWidth(), getHeight());
        graphics.fillOval(leftUpperCornerX, leftUpperCornerY, getWidth(), getHeight());
    }
}
