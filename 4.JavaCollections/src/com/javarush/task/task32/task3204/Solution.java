package com.javarush.task.task32.task3204;

import java.io.ByteArrayOutputStream;

/* 
Генератор паролей

Реализуй логику метода getPassword, который должен возвращать ByteArrayOutputStream, в котором будут байты пароля.
Требования к паролю:
1) 8 символов.
2) только цифры и латинские буквы разного регистра.
3) обязательно должны присутствовать цифры, и буквы разного регистра.
Все сгенерированные пароли должны быть уникальные.

Пример правильного пароля:
wMh7smNu
*/
public class Solution {
    public static void main(String[] args) {
        ByteArrayOutputStream password = getPassword();
        System.out.println(password.toString());
    }

    /**
     * цифры 48-57
     * прописные 65-90
     * строчные буквы 97-122
     *
     * @return
     */
    public static ByteArrayOutputStream getPassword() {
        //byte[] pass = new byte[8];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        boolean checkPass = false;
        while (!checkPass) {
            for (int i = 0; i < 8; i++) {
                int b = (int) (Math.random() * 122);
                if (b > 47 && b < 58 || b > 64 && b < 91 || b > 96 && b < 123) {
                    byteArrayOutputStream.write(b);
                } else {
                    i--;
                }

            }
            String tmpPass = byteArrayOutputStream.toString();
            if (tmpPass.matches("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$")) {
                checkPass = true;
            } else{
                byteArrayOutputStream.reset();
            }
        }
        //System.out.println(byteArrayOutputStream.toString());


        return byteArrayOutputStream;
    }
}