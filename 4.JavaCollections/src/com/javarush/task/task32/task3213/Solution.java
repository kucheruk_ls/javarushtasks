package com.javarush.task.task32.task3213;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

/* 
Шифр Цезаря
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        StringReader reader = new StringReader("Khoor#Dpljr#&C,₷B'3");
        System.out.println(decode(reader, -3));  //Hello Amigo #@)₴?$0
    }

    public static String decode(StringReader reader, int key) throws IOException {
        //char[] abc = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        if(reader == null) return "";
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine())!=null){
            stringBuilder.append(line);
        }
        String tmp = stringBuilder.toString();
        char[] tmpMassiv = tmp.toCharArray();

        stringBuilder.delete(0,stringBuilder.length());
        for(int i=0;i<tmpMassiv.length;i++){
            int tmpS = (int) tmpMassiv[i];
            tmpMassiv[i]= (char)(tmpS+key);
            stringBuilder.append(tmpMassiv[i]);
        }
        String result = stringBuilder.toString();



        return result;
    }
}
