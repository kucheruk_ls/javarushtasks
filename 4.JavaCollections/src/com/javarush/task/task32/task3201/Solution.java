package com.javarush.task.task32.task3201;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/*
Запись в существующий файл

В метод main приходят три параметра:
1) fileName - путь к файлу;
2) number - число, позиция в файле;
3) text - текст.
Записать text в файл fileName начиная с позиции number.
Запись должна производиться поверх старых данных, содержащихся в файле.
Если файл слишком короткий, то записать в конец файла.
Используй RandomAccessFile и его методы seek и write.
*/
public class Solution {
    public static void main(String... args) {
        String fileName = args[0];
        String number = args[1];
        int numPos = Integer.parseInt(number);
        String text = args[2];
        byte[] textw = new byte[text.length()];
        textw = text.getBytes();

        try{
            RandomAccessFile randomAccessFile = new RandomAccessFile(fileName, "rw");
            if(numPos<randomAccessFile.length()) {
                randomAccessFile.seek(numPos);
            }else{
                long newPos = randomAccessFile.length();
                randomAccessFile.seek(newPos);
            }
            randomAccessFile.write(textw);
            randomAccessFile.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
