package com.javarush.task.task32.task3210;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/* 
Используем RandomAccessFile
В метод main приходят три параметра:
1) fileName - путь к файлу;
2) number - число, позиция в файле;
3) text - текст.

Считать текст с файла начиная с позиции number, длинной такой же как и длинна переданного текста в третьем параметре.
Если считанный текст такой же как и text, то записать в конец файла строку 'true', иначе записать 'false'.
Используй RandomAccessFile и его методы seek(long pos), read(byte b[], int off, int len), write(byte b[]).
Используй один из конструкторов класса String для конвертации считанной строчки в текст.
*/

public class Solution {
    public static void main(String... args) {
        String fileName = args[0];
        String number = args[1];
        String text = args[2];

        try{
            RandomAccessFile randomAccessFile = new RandomAccessFile(fileName,"rw");
            byte[]mb = new byte[text.getBytes().length];
            randomAccessFile.seek(Integer.parseInt(number));
            int tl = text.getBytes().length;
            randomAccessFile.read(mb,0,tl);
            String readText = new String(mb).trim();
            if(text.equals(readText)){
                randomAccessFile.seek(randomAccessFile.length());
                byte[] tr = "true".getBytes();
                randomAccessFile.write(tr);
            }else{
                randomAccessFile.seek(randomAccessFile.length());
                byte[] fl = "false".getBytes();
                randomAccessFile.write(fl);
            }

            randomAccessFile.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
