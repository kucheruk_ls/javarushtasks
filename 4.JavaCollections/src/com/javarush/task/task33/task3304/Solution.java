package com.javarush.task.task33.task3304;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

/* 
Конвертация из одного класса в другой используя JSON
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        Second s = (Second) convertOneToAnother(new First(), Second.class);
        First f = (First) convertOneToAnother(new Second(), First.class);

       // System.out.println(s.name);
        // System.out.println(f.name);

/*
        First f = new First();
        Second s = new Second();
        StringWriter stringWriter = new StringWriter();
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.writeValue(stringWriter, f);
        objectMapper.writeValue(stringWriter, s);
        System.out.println(stringWriter.toString());
*/
    }

    public static Object convertOneToAnother(Object one, Class resultClassObject) throws IOException {
        StringWriter stringWriter = new StringWriter();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(stringWriter,one);
        String ObjectOne = stringWriter.toString();
        //System.out.println(stringWriter.toString());
        //System.out.println("one name:"+one.getClass().getSimpleName().toLowerCase().toString());
        //System.out.println("result name:"+resultClassObject.getSimpleName().toLowerCase().toString());
        String resultClass = stringWriter.toString().replaceFirst(one.getClass().getSimpleName().toLowerCase().toString(),resultClassObject.getSimpleName().toLowerCase().toString());
        //System.out.println(resultClass);

        StringReader stringReader = new StringReader(resultClass);
        Object result = objectMapper.readValue(stringReader,resultClassObject);


       //return objectMapper1.readValue(stringReader,resultClassObject);
        return result;
    }

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME,  property="className")
    @JsonSubTypes(@JsonSubTypes.Type(value=First.class,  name="first"))
    public static class First {
        public int i;
        public String name;
    }

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME,  property="className")
    @JsonSubTypes(@JsonSubTypes.Type(value=Second.class, name="second"))
    public static class Second {
        public int i;
        public String name;
    }
}
