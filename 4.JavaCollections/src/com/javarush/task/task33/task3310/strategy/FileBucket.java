/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task33.task3310.strategy;


import com.javarush.task.task33.task3310.ExceptionHandler;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileBucket {
    private Path path;

    public FileBucket(){
        try{

            this.path = Files.createTempFile(null,null);
            Files.deleteIfExists(this.path);
            Files.createFile(this.path);
            this.path.toFile().deleteOnExit();
        }catch (IOException e){

        }



    }

    /**
     * он должен возвращать размер файла на который указывает path.
     * @return
     */
    public long getFileSize(){
      long size = 0L;
       try{
           size = Files.size(path);
       }catch (IOException e){

       }
       return size;
    }

    /**
     * должен сериализовывать переданный entry в файл. Учти, каждый entry может содержать еще один entry.
     * @param entry
     */
 //   void putEntry(Entry entry){
//        try{
//            OutputStream fos = Files.newOutputStream(path); //создадим поток для записи в файл
//            ObjectOutputStream oos = new ObjectOutputStream(fos);//с
//            oos.writeObject(entry);
//            oos.flush();
//            oos.close();
//        }catch (FileNotFoundException e){
//            e.printStackTrace();
//        }catch (IOException e){
//
//        }

       // так не проходит попробуем использовать try with resources

        public void putEntry(Entry entry) {
            try {
                OutputStream fos = Files.newOutputStream(path);
                ObjectOutputStream outputStream = new ObjectOutputStream(fos);
                outputStream.writeObject(entry);
                fos.close();
                outputStream.close();
            }
            catch (Exception e) {
                ExceptionHandler.log(e);
            }
        }

        public Entry getEntry() {
            Entry entry = null;
            if(getFileSize() > 0) {
                try {
                    InputStream fis = Files.newInputStream(path);
                    ObjectInputStream inputStream = new ObjectInputStream(fis);
                    Object object = inputStream.readObject();
                    fis.close();
                    inputStream.close();
                    entry = (Entry)object;
                }
                catch (Exception e) {
                    ExceptionHandler.log(e);
                }
            }
            return entry;
        }

    /**
     * удалять файл на который указывает path.
     */
    public void remove(){
        try{
            Files.delete(path);
        }catch (IOException e){
            ExceptionHandler.log(e);
        }
    }
}
