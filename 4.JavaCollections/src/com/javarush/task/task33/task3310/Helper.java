/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task33.task3310;

import java.math.BigInteger;
import java.security.SecureRandom;

public class Helper {

    /**
     * который будет генерировать случайную строку. Воспользуйся для этого классами SecureRandom и BigInteger.
     * Подсказка: гугли запрос "random string java". Строка может состоять из цифр и любой из 26 маленьких букв
     * английского алфавита.
     * основание системы 36 включает сюда все цифры и буквы английского алфавита
     * @return
     */
    public static String generateRandomString(){
        //byte[] bytes = new BigInteger(130, new SecureRandom().toString(36));
        String s = new BigInteger(130, new SecureRandom()).toString(36); // случайная строка
        return s;
    }

    /**
     * Он должен выводить переданный текст в консоль. Весь дальнейший вывод в программе должен быть реализован через этот метод!
     * @param message
     */
    public static void printMessage(String message){
        System.out.println(message);
    }
}
