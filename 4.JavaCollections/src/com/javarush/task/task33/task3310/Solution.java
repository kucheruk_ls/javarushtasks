/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task33.task3310;

import com.javarush.task.task33.task3310.strategy.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Solution {
    public static void main(String[] args) {
        testStrategy(new HashMapStorageStrategy(),10000);
        Helper.printMessage("------------------------------------");
        testStrategy(new OurHashMapStorageStrategy(),10000);
        Helper.printMessage("------------------------------------");
        testStrategy(new FileStorageStrategy(),100);
        Helper.printMessage("------------------------------------");
        testStrategy(new OurHashBiMapStorageStrategy(),10000);
        Helper.printMessage("------------------------------------");
        testStrategy(new HashBiMapStorageStrategy(),10000);
        Helper.printMessage("------------------------------------");
        testStrategy(new DualHashBidiMapStorageStrategy(),10000);


    }

    /**
     * Set<Long> getIds(Shortener shortener, Set<String> strings). Этот метод должен для переданного множества строк
     * возвращать множество идентификаторов. Идентификатор для каждой отдельной строки нужно получить, используя shortener.
     * @param shortener
     * @param strings
     * @return
     */
    public static Set<Long> getIds(Shortener shortener, Set<String> strings){
        Set<Long> longs = new HashSet<>();
        for(String s:strings){
            //тестовый вывод
           // System.out.println("строка "+s);
            Long id = shortener.getId(s);
            //System.out.println("полученный id "+id);
            longs.add(id);
        }
        //тестовый вывод -убрать
        //Helper.printMessage("в getIds обработано "+strings.size()+" строк, выдано "+longs.size()+" идентификаторов.");
        return longs;
    }

    /**
     * Метод будет возвращать множество строк, которое соответствует переданному множеству идентификаторов.
     * При реальном использовании Shortener, задача получить из множества строк множество идентификаторов и наоборот
     * скорее всего не встретится, это нужно исключительно для тестирования.
     * @param shortener
     * @param keys
     * @return
     */
    public static Set<String> getStrings(Shortener shortener, Set<Long> keys){
        Set<String> strings = new HashSet<>();
        for(Long l:keys){
            strings.add(shortener.getString(l));
        }
        return strings;
    }

    /**
     Метод будет тестировать работу переданной стратегии на определенном количестве элементов elementsNumber. Реализация метода должна:
     6.2.3.1. Выводить имя класса стратегии. Имя не должно включать имя пакета.
     6.2.3.2. Генерировать тестовое множество строк, используя Helper и заданное количество элементов elementsNumber.
     6.2.3.3. Создавать объект типа Shortener, используя переданную стратегию.
     6.2.3.4. Замерять и выводить время необходимое для отработки метода getIds для заданной стратегии и заданного
     множества элементов. Время вывести в миллисекундах. При замере времени работы метода можно пренебречь переключением
     процессора на другие потоки, временем, которое тратится на сам вызов, возврат значений и вызов методов получения
     времени (даты). Замер времени произведи с использованием объектов типа Date.
     6.2.3.5. Замерять и выводить время необходимое для отработки метода getStrings для заданной стратегии и полученного
     в предыдущем пункте множества идентификаторов.
     6.2.3.6. Сравнивать одинаковое ли содержимое множества строк, которое было сгенерировано и множества, которое было
     возвращено методом getStrings. Если множества одинаковы, то выведи "Тест пройден.", иначе "Тест не пройден.".
     * @param strategy
     * @param elementsNumber
     */
    public static void testStrategy(StorageStrategy strategy, long elementsNumber){
        Helper.printMessage(strategy.getClass().getSimpleName());
        Set<String> testSetString = new HashSet<>();
        //генерируем тестовое множество строк
        for(Long i=0L;i<elementsNumber;i++){
            testSetString.add(Helper.generateRandomString());
        }
        //Helper.printMessage("Тестовая печать testSetString.size(): "+testSetString.size());
        Shortener shortener = new Shortener(strategy);
        //считаем время работы метода getIds
        Long dateStart = new Date().getTime();
        Set<Long> resultLng = getIds(shortener,testSetString);
        Long dateStop = new Date().getTime();

        //Helper.printMessage("Тестовая печать resultLng.size(): "+resultLng.size());

        Long time  = (dateStop - dateStart);
        Helper.printMessage("Время обработки методом getId: "+time);

        //считаем время работы метода getString
        Set<String> resStrs = new HashSet<>();
        Long timeStart = new Date().getTime();
        for(Long l:resultLng){
            resStrs.add(shortener.getString(l));
        }

        //Helper.printMessage("Тестовая печать resStrs.size(): "+resStrs.size());

        Long timeStop = new Date().getTime();
        Long resultT = timeStop - timeStart;
        Helper.printMessage("Время обработки методом getString: "+resultT);

        //Сравнение двух списков


        Boolean testResult = true;
        if (testSetString.size() == resStrs.size()){
        for(String s1:testSetString) {

                if (!resStrs.contains(s1)) {
                    testResult = false;
                    break;
                }
            }
        } else {
            testResult = false;
        }
        if(testResult){
            Helper.printMessage("Тест пройден.");
        }else{
            Helper.printMessage("Тест не пройден.");
        }

    }

}
