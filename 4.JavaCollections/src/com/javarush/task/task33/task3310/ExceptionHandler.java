/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task33.task3310;

public class ExceptionHandler extends Exception {

    /**
     * , который будет выводить краткое описание исключения.
     * @param e
     */
    public static void log(Exception e){
        System.out.println(e.getMessage());
    }
}
