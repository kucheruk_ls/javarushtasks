/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task33.task3310.tests;

import com.javarush.task.task33.task3310.Helper;
import com.javarush.task.task33.task3310.Shortener;
import com.javarush.task.task33.task3310.strategy.HashBiMapStorageStrategy;
import com.javarush.task.task33.task3310.strategy.HashMapStorageStrategy;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class SpeedTest {

    /**
     * Он должен возвращать время в миллисекундах необходимое для получения идентификаторов для всех строк из strings.
     * Идентификаторы должны быть записаны в ids.
     * @param shortener
     * @param strings
     * @param ids
     * @return
     */
    public long getTimeForGettingIds(Shortener shortener, Set<String> strings, Set<Long> ids){
        long timeStart = new Date().getTime();
        long tmpId = 0L;
        for(String s:strings){
            tmpId = shortener.getId(s);
            ids.add(tmpId);
        }
        long timeStop = new Date().getTime();
        long timeResult = timeStop - timeStart;
        return timeResult;
    }

    /**
     * Он должен возвращать время в миллисекундах необходимое для получения строк для всех идентификаторов из ids.
     * Строки должны быть записаны в strings.
     * @param shortener
     * @param ids
     * @param strings
     * @return
     */
    public long getTimeForGettingStrings(Shortener shortener, Set<Long> ids, Set<String> strings){
        long timeStart = new Date().getTime();
        String s = "";
        for(long l:ids){
            s = shortener.getString(l);
            strings.add(s);
        }
        long timeStop = new Date().getTime();
        long timeResult = timeStop-timeStart;
        return timeResult;
    }

    @Test
    public void testHashMapStorage(){
        Shortener shortener1 = new Shortener(new HashMapStorageStrategy());
        Shortener shortener2 = new Shortener(new HashBiMapStorageStrategy());

        HashSet<String> origStrings = new HashSet<>();
        for(int i = 0;i<10000;i++){
            origStrings.add(Helper.generateRandomString());
        }
        HashSet<Long> shortener1Ids = new HashSet<>();
        float timeWorkIdShort1 = getTimeForGettingIds(shortener1,origStrings,shortener1Ids);

        HashSet<Long> shortener2Ids = new HashSet<>();
        float timeWorkIdShort2 = getTimeForGettingIds(shortener2,origStrings,shortener2Ids);
        Assert.assertTrue(timeWorkIdShort1 > timeWorkIdShort2);

        float timeWorkStringShort1 = getTimeForGettingStrings(shortener1,shortener1Ids,origStrings);
        float timeWorkStringShort2 = getTimeForGettingStrings(shortener2,shortener2Ids,origStrings);
        Assert.assertEquals(timeWorkStringShort1,timeWorkStringShort2,30);

    }
}
