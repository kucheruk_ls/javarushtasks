/*
 * Copyright (c) 2019.
 * kls   shtormlbt@mail.ru
 */

package com.javarush.task.task33.task3310.tests;


import com.javarush.task.task33.task3310.Helper;
import com.javarush.task.task33.task3310.Shortener;
import com.javarush.task.task33.task3310.strategy.*;
import org.junit.Assert;
import org.junit.Test;

public class FunctionalTest {
    /**
     * Создавать три строки. Текст 1 и 3 строк должен быть одинаковым.
     * 14.4.2. Получать и сохранять идентификаторы для всех трех строк с помощью shortener.
     * 14.4.3. Проверять, что идентификатор для 2 строки не равен идентификатору для 1 и 3 строк.
     *
     * Подсказка: метод Assert.assertNotEquals.
     *
     * 14.4.4. Проверять, что идентификаторы для 1 и 3 строк равны.
     *
     * Подсказка: метод Assert.assertEquals.
     *
     * 14.4.5. Получать три строки по трем идентификаторам с помощью shortener.
     * 14.4.6. Проверять, что строки, полученные в предыдущем пункте, эквивалентны оригинальным.
     *
     * Подсказка: метод Assert.assertEquals.
     * @param shortener
     */
    public void testStorage(Shortener shortener){
        String one = "Бравые солдаты уходили на запад...";
        String two = Helper.generateRandomString();
        String three = "Бравые солдаты уходили на запад...";

        Long oneId = shortener.getId(one);
        Long twoId = shortener.getId(two);
        Long threeId = shortener.getId(three);

        Assert.assertNotEquals(twoId,oneId);
        Assert.assertNotEquals(twoId,threeId);

        Assert.assertEquals(oneId,threeId);

        String oner = shortener.getString(oneId);
        String twor = shortener.getString(twoId);
        String threer = shortener.getString(threeId);

        Assert.assertEquals(oner,one);
        Assert.assertEquals(twor,two);
        Assert.assertEquals(threer,three);
    }

    /**
     *Каждый тест должен иметь аннотацию @Test, создавать подходящую стратегию, создавать объект класса Shortener на
     * базе этой стратегии и вызывать метод testStorage для него.
     * Запусти и проверь, что все тесты проходят.
     */
    @Test
    public void testHashMapStorageStrategy(){
        HashMapStorageStrategy hashMapStorageStrategy = new HashMapStorageStrategy();
        Shortener shortener = new Shortener(hashMapStorageStrategy);
        testStorage(shortener);
    }

    @Test
    public void testOurHashMapStorageStrategy(){
        OurHashMapStorageStrategy ourHashMapStorageStrategy = new OurHashMapStorageStrategy();
        Shortener shortener = new Shortener(ourHashMapStorageStrategy);
        testStorage(shortener);
    }

    @Test
    public void testFileStorageStrategy(){
        FileStorageStrategy fileStorageStrategy = new FileStorageStrategy();
        Shortener shortener = new Shortener(fileStorageStrategy);
        testStorage(shortener);
    }

    @Test
    public void testHashBiMapStorageStrategy(){
        HashBiMapStorageStrategy hashBiMapStorageStrategy = new HashBiMapStorageStrategy();
        Shortener shortener = new Shortener(hashBiMapStorageStrategy);
        testStorage(shortener);
    }

    @Test
    public void testDualHashBidiMapStorageStrategy(){
        DualHashBidiMapStorageStrategy dualHashBidiMapStorageStrategy = new DualHashBidiMapStorageStrategy();
        Shortener shortener = new Shortener(dualHashBidiMapStorageStrategy);
        testStorage(shortener);
    }

    @Test
    public void testOurHashBiMapStorageStrategy(){
        OurHashBiMapStorageStrategy ourHashBiMapStorageStrategy = new OurHashBiMapStorageStrategy();
        Shortener shortener = new Shortener(ourHashBiMapStorageStrategy);
        testStorage(shortener);
    }
}
