package com.javarush.task.task33.task3309;

import org.w3c.dom.Document;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.ArrayDeque;
import java.util.Queue;

/*
Комментарий внутри xml
*/
public class Solution {
    public static String toXmlWithComment(Object obj, String tagName, String comment) {

        Document document = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try{
            DocumentBuilder db = dbf.newDocumentBuilder();
            document = db.newDocument();
            JAXBContext context = JAXBContext.newInstance(obj.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.marshal(obj, document);
        }catch (Exception e){
            e.printStackTrace();
        }

        Node root = document.getDocumentElement();
        if(root.hasChildNodes()){
            Queue<NodeList> queue = new ArrayDeque<>();
            queue.offer(root.getChildNodes());
            while (!queue.isEmpty()){
                NodeList list = queue.poll();

            }
        }




        return null;
    }

    public static void main(String[] args) {
        Cat cat = new Cat("Vaska", 3, 4);
        toXmlWithComment(cat,"tag", "comment");
    }
}
