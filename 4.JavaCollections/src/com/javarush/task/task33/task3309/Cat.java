package com.javarush.task.task33.task3309;


import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Cat {
    public String name;
    public int age;
    private int weight;

    public Cat(){

    }

    public Cat(String name,int age,int weight){
        this.name = name;
        this.age = age;
        this.weight = weight;
    }
}
