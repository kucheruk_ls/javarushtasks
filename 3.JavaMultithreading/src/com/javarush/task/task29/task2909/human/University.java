package com.javarush.task.task29.task2909.human;

import java.util.ArrayList;
import java.util.List;

public class University extends UniversityPerson {
    private List<Student> students = new ArrayList<>();
    private String name;
    private int age;

    public University(String name, int age) {
        super(name,age);
        //this.name = name;
        //this.age = age;
    }

    public List<Student> getStudents() {
        return students;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Student getStudentWithAverageGrade(double averageGrade) {
        //TODO:

        Student candidat=null;
        for(int i=0;i<students.size();i++){
            if(students.get(i).getAverageGrade()==averageGrade){
                candidat = students.get(i);
                break;
            }
        }

        return candidat;
    }

    public Student getStudentWithMaxAverageGrade() {
        //TODO:
        double maxGr = 0.0;
        Student Sch = students.get(0);
        for(int i = 0;i<students.size();i++){

            if(students.get(i).getAverageGrade()>maxGr){
                maxGr=students.get(i).getAverageGrade();
                Sch = students.get(i);
            }
        }
        return Sch;
    }

    public Student getStudentWithMinAverageGrade() {
        //TODO:
        double mixGr = students.get(0).getAverageGrade();
        Student Sch = students.get(0);
        for(int i = 0;i<students.size();i++){

            if(students.get(i).getAverageGrade()<mixGr){
                mixGr=students.get(i).getAverageGrade();
                Sch = students.get(i);
            }
        }
        return Sch;
    }

    public void expel(Student student){
        students.remove(student);
    }
}