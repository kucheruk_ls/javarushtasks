package com.javarush.task.task35.task3513;

import java.util.*;

public class Model {
    private static final int FIELD_WIDTH = 4;
    private Tile[][] gameTiles;
    int score; // текущий счет
    int maxTile; // max вес плитки на поле
    Stack<Tile[][]> previousStates = new Stack(); // здесь будем хранить предыдущие состояния игрового поля
    Stack<Integer> previousScores = new Stack(); //предыдущие счета
    boolean isSaveNeeded = true;//оно нам понадобится в будущем)

    public Model(){
        score=0;
        maxTile=0;
        gameTiles = new Tile[FIELD_WIDTH][FIELD_WIDTH];
        resetGameTiles();
    }

    /**
     * Приватный метод saveState с одним параметром типа Tile[][] будет сохранять текущее
     * игровое состояние и счет в стеки с помощью метода push и устанавливать флаг isSaveNeeded равным false.
     * @param tiles
     */
    private void saveState(Tile[][] tiles){
        Tile[][] backTile = new Tile[gameTiles.length][gameTiles.length];
        for(int i=0;i<gameTiles.length;i++){
            for(int j=0;j<gameTiles.length;j++){
                backTile[i][j] = new Tile(gameTiles[i][j].value);
            }
        }

        previousStates.push(backTile);
        previousScores.push(score);
        isSaveNeeded=false;
    }

    /**
     * Публичный метод rollback будет устанавливать текущее игровое состояние равным последнему находящемуся
     * в стеках с помощью метода pop.
     */
    public void rollback(){
        if(!previousStates.isEmpty())gameTiles = (Tile[][]) previousStates.pop();
        if(!previousScores.isEmpty()) score = (int) previousScores.pop();
    }

    public Tile[][] getGameTiles() {
        return gameTiles;
    }


    /**
     * метод для начала новой игры
     */
    void resetGameTiles(){

        for(int i=0;i<gameTiles.length;i++){
            for(int j=0;j<gameTiles.length;j++){
                gameTiles[i][j] = new Tile();
            }
        }

        addTile();
        addTile();
    }

    /**
     * метод возвращает лист с пустыми плитками
     * @return
     */
    private List<Tile> getEmptyTiles(){
        ArrayList<Tile> tiles = new ArrayList<>();
        for(int i=0;i<gameTiles.length;i++){
            for(int j=0;j<gameTiles[i].length;j++){
                if(gameTiles[i][j].isEmpty())tiles.add(gameTiles[i][j]);
            }
        }
        return tiles;
    }

    /**
     * Который будет смотреть какие плитки пустуют и, если такие имеются,
     * менять вес одной из них, выбранной случайным образом, на 2 или 4 (на 9 двоек должна приходиться 1 четверка).
     * Получить случайный объект из списка можешь использовав следующее выражение:
     * (размерСписка * случайноеЧислоОтНуляДоЕдиницы).
     */
    private void addTile(){
        if(!getEmptyTiles().isEmpty()) {
            getEmptyTiles().get((int) (getEmptyTiles().size() * Math.random())).value = (Math.random() < 0.9 ? 2 : 4);
        }
    }

    /**
     * Сжатие плиток, таким образом, чтобы все пустые плитки были справа, т.е. ряд {4, 2, 0, 4} становится рядом {4, 2, 4, 0}
     * @param tiles
     */
    private boolean compressTiles(Tile[] tiles){
        boolean isUpdate = false;
       for(int j=0;j<tiles.length;j++) {
           for (int i = 0; i < tiles.length - 1; i++) {
               if (tiles[i].isEmpty() && !tiles[i + 1].isEmpty()) {
                   Tile tmp = tiles[i];
                   tiles[i] = tiles[i + 1];
                   tiles[i + 1] = tmp;
                    isUpdate = true;
               }
           }
       }
       return isUpdate;
    }

    /**
     * Слияние плиток одного номинала, т.е. ряд {4, 4, 2, 0} становится рядом {8, 2, 0, 0}.
     * Обрати внимание, что ряд {4, 4, 4, 4} превратится в {8, 8, 0, 0}, а {4, 4, 4, 0} в {8, 4, 0, 0}.
     * @param tiles
     */
    private boolean mergeTiles(Tile[] tiles){
        boolean isUpdate = false;
        for(int i=0;i<tiles.length-1;i++){
            if(!tiles[i].isEmpty()&&tiles[i].value==tiles[i+1].value){
                tiles[i].value=tiles[i].value*2;
                tiles[i+1].value=0;
                isUpdate = true;
                score+=tiles[i].value;
                if(maxTile<tiles[i].value)maxTile=tiles[i].value;
            }
        }
        compressTiles(tiles);
        return isUpdate;
    }

    /**
     * Реализуем метод left, который будет для каждой строки массива gameTiles вызывать методы compressTiles
     * и mergeTiles и добавлять одну плитку с помощью метода addTile в том случае, если это необходимо.
     */
    void left(){
        if(isSaveNeeded==true)saveState(gameTiles);
        boolean isUpdate = false;
        for(int i=0;i<gameTiles.length;i++){
            if(compressTiles(gameTiles[i]) | mergeTiles(gameTiles[i])){
                isUpdate = true;

            }
        }

        if(isUpdate) addTile();
        isSaveNeeded=true;
    }

    /**
     * смещает в низ
     */
    void down(){
        saveState(gameTiles);
        rotate();
        left();
        rotate();
        rotate();
        rotate();
    }

    /**
     * угадайте что делает этот метод.
     */
    void up(){
        saveState(gameTiles);
        rotate();
        rotate();
        rotate();
        left();
        rotate();
    }

    void right(){
        saveState(gameTiles);
        rotate();
        rotate();
        left();
        rotate();
        rotate();

    }

    /**
     * метод поворачивает массив на 90 градусов
     */
    private void rotate(){

        Tile[][] rotateTiles = new Tile[FIELD_WIDTH][FIELD_WIDTH];
        for(int i=0;i<gameTiles.length;i++){
            for(int j=0;j<gameTiles[i].length;j++){
                int z = gameTiles[i].length-(i+1);
                rotateTiles[j][z]=gameTiles[i][j];
            }
        }

        gameTiles=rotateTiles;

    }

    /**
     * проверяет есть ли еще ходы
     * @return
     */
    boolean canMove(){
        boolean priznak = false;
        for(int i=0;i<gameTiles.length-1;i++){
            for(int j=0;j<gameTiles[i].length-1;j++){
                if(gameTiles[i][j].isEmpty()||(gameTiles[i][j].value==gameTiles[i+1][j].value)||(gameTiles[i][j].value==gameTiles[i][j+1].value)){
                    priznak=true;
                }
            }
        }
        return priznak;
    }

    /**
     * который будет вызывать один из методов движения случайным образом. Можешь реализовать это вычислив целочисленное
     * n = ((int) (Math.random() * 100)) % 4.
     * Это число будет содержать целое псевдослучайное число в диапазоне [0..3], по каждому из которых можешь вызывать
     * один из методов left, right, up, down.
     */
    public void  randomMove(){
        int n = ((int) (Math.random() * 100 % 4));
        switch (n){
            case 0:up();
            break;
            case 1:down();
            break;
            case 2:right();
            break;
            case 3:left();
            break;
        }
    }

    /**
     * будет возвращать true, в случае, если вес плиток в массиве gameTiles отличается от веса плиток в верхнем массиве
     * стека previousStates. Обрати внимание на то, что мы не должны удалять из стека верхний элемент, используй метод peek.
     * @return
     */
    boolean hasBoardChanged(){
        boolean result = false;
        Tile[][] tmp = (Tile[][]) previousStates.peek();
        int tmpvalue = 0;
        int gameTileValue = 0;
        for(int i=0;i<tmp.length;i++){
            for(int j=0;j<tmp.length;j++){
                tmpvalue+=tmp[i][j].value;
            }
        }
        for(int i=0;i<gameTiles.length;i++){
            for(int j=0;j<gameTiles.length;j++){
                gameTileValue+=gameTiles[i][j].value;
            }
        }
        if(gameTileValue-tmpvalue!=0)result = true;
        return result;
    }

    /**
     *принимает один параметр типа move, и возвращает объект типа MoveEfficiency описывающий эффективность переданного
     * хода. Несколько советов:
     * а) не забудь вызывать метод rollback, чтобы восстановить корректное игровое состояние;
     * б) в случае, если ход не меняет состояние игрового поля, количество пустых плиток и счет у объекта MoveEfficiency
     * сделай равными -1 и 0 соответственно;
     * в) выполнить ход можно вызвав метод move на объекте полученном в качестве параметра.
     * 1) Ход (который move).
     * 2) Количество пустых плиток, которое будет на поле после этого хода.
     * 3) Счет, который будет после этого хода.
     * @param move
     * @return
     */
    public MoveEfficiency getMoveEfficiency(Move move){

        int oldScore = score;
        int oldNumberOfEmptyTiles = getEmptyTiles().size();
        move.move();
        int newNumberOfEmptyTiles = getEmptyTiles().size();
        MoveEfficiency moveEfficiency;
        if (!hasBoardChanged() && score == oldScore && oldNumberOfEmptyTiles == newNumberOfEmptyTiles) {
            moveEfficiency = new MoveEfficiency(-1, 0, move);
        } else {
            moveEfficiency = new MoveEfficiency(newNumberOfEmptyTiles, score, move);
        }
        rollback();
        return moveEfficiency;
    }

    /**
     * метод autoMove в классе Model, который будет выбирать лучший из возможных ходов и выполнять его.
     */
    public void autoMove(){
        PriorityQueue<MoveEfficiency> priorityQueue = new PriorityQueue(4, Collections.reverseOrder()); // важно передачь дженерик в очередь
        priorityQueue.offer(getMoveEfficiency(this::left));
        priorityQueue.offer(getMoveEfficiency(this::right));
        priorityQueue.offer(getMoveEfficiency(this::up));
        priorityQueue.offer(getMoveEfficiency(this::down));

        Move move = priorityQueue.peek().getMove();
        move.move();
    }
}
