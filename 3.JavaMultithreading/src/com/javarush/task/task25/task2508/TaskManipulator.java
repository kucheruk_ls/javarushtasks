package com.javarush.task.task25.task2508;

import java.io.IOException;

public class TaskManipulator implements Runnable, CustomThreadManipulator {
    private Thread thread;
    @Override
    public void run() {
        Thread thread = new Thread();
        try {

while (!thread.isInterrupted()) {
    System.out.println(Thread.currentThread().getName());
    Thread.sleep(100);
}

        }catch(InterruptedException e){
            e.printStackTrace();
        }
    }

    @Override
    public void start(String threadName) {
        /*
        Метод start должен создавать, сохранять во внутреннее поле и запускать нить с именем, которое передано через аргумент метода.
         */

       Thread thread = new Thread(this);
       thread.setName(threadName);
       this.thread = thread;
       thread.start();
    }

    @Override
    public void stop() {
        /*
        Метод stop должен прерывать последнюю созданную классом TaskManipulator нить.
         */
        this.thread.interrupt();

    }
}
