package com.javarush.task.task25.task2504;

/* 
Switch для нитей
*/
public class Solution {
    public static void processThreads(Thread... threads) {
        //implement this method - реализуйте этот метод
        for(Thread thread : threads){
            //System.out.println(thread.getState());

            switch(thread.getState()){
                case NEW:
                    thread.start();
                    break;
                case RUNNABLE:
                    thread.isInterrupted();
                    break;
                case WAITING:
                    thread.interrupt();
                    break;
                case TIMED_WAITING:
                    thread.interrupt();
                    break;
                case BLOCKED:
                    thread.interrupt();
                    break;
                case TERMINATED:
                    System.out.println(thread.getPriority());
                    break;
            }

        }
    }

    public static void main(String[] args) {
        Thread[] mt  = new Thread[5];
        Thread thread1 = new Thread();
        Thread thread2 = new Thread();
        Thread thread3 = new Thread();
        Thread thread4 = new Thread();
        Thread thread5 = new Thread();

        mt[0] = thread1;
        mt[1] = thread2;
        mt[2] = thread3;
        mt[3] = thread4;
        mt[4] = thread5;

        processThreads(mt);
    }
}
