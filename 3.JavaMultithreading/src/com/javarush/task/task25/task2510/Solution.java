package com.javarush.task.task25.task2510;

import java.io.IOException;

/*
Поживем - увидим
*/
public class Solution extends Thread {

    public Solution() {
        setUncaughtExceptionHandler(new MyUncaughtExceptionHandler());
    }



    public void run(){
        try {
            throw new IOException();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Solution sol = new Solution();
        sol.start();

    }

    public class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler{
        @Override
        public void uncaughtException(Thread t, Throwable e) {
            if(e instanceof Exception){
                System.out.println("Надо обработать");
            }else if(e instanceof Error){
                System.out.println("Нельзя дальше работать");
            }else{
                System.out.printf("Поживем - увидим");
            }
        }
    }




}
