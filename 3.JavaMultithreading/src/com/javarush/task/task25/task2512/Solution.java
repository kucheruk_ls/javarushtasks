package com.javarush.task.task25.task2512;

import java.util.Stack;

/*
Живем своим умом
*/
public class Solution extends Thread implements Thread.UncaughtExceptionHandler {


    @Override
    public void uncaughtException(Thread t, Throwable e) {
        t.interrupt();

        Stack<Throwable> exseptions = new Stack<>();
        exseptions.push(e);
        Throwable throwable = e.getCause();
        while (throwable!=null){
            exseptions.push(throwable);
            throwable = throwable.getCause();
        }
        while (!exseptions.empty()){
            Throwable currentException = exseptions.pop();
            System.out.println(currentException.getClass().getName()+": "+currentException.getMessage());
        }
    }

    public void exeptiongen(){
        throw new IndexOutOfBoundsException();

    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.uncaughtException(new Thread(), new Exception("ABC", new RuntimeException("DEF", new IllegalAccessException("GHI"))));


    }
}
