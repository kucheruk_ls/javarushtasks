package com.javarush.task.task25.task2502;

import java.util.*;

/* 
Машину на СТО не повезем!
*/
public class Solution {
    public static enum Wheel {
        FRONT_LEFT,
        FRONT_RIGHT,
        BACK_LEFT,
        BACK_RIGHT
    }

    public static class Car {
        protected List<Wheel> wheels;

        public Car() {
            //init wheels here
            String[] kolesa = loadWheelNamesFromDB();

            if(kolesa.length<1||kolesa.length>4){
                throw new IllegalArgumentException();
            }else {

                ArrayList<Wheel> wheels = new ArrayList<Wheel>();

                wheels.add(Wheel.valueOf(kolesa[0]));
                wheels.add(Wheel.valueOf(kolesa[1]));
                wheels.add(Wheel.valueOf(kolesa[2]));
                wheels.add(Wheel.valueOf(kolesa[3]));

                this.wheels = wheels;



            }
        }

        protected String[] loadWheelNamesFromDB() {
            //this method returns mock data
            return new String[]{"FRONT_LEFT", "FRONT_RIGHT", "BACK_LEFT", "BACK_RIGHT"};
        }
    }

    public static void main(String[] args) {
        Car cat  = new Car();
        for(Wheel wheel:Wheel.values()){
            System.out.println(wheel);
        }
    }
}
