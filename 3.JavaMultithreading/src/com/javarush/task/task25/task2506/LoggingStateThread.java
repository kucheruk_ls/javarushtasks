package com.javarush.task.task25.task2506;

public class LoggingStateThread extends Thread{
        private Thread t;
        public LoggingStateThread(Thread t){
            this.setDaemon(true);
            this.t = t;
        }

        public void run(){
            Thread.State pts = null;
            while (true){
                Thread.State ts = t.getState();

                if(ts!=pts) {
                    System.out.println(ts);
                    pts = ts;

                }
                if(pts==State.TERMINATED){
                    this.stop();
                }

            }
        }


}
