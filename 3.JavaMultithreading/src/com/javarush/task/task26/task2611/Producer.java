package com.javarush.task.task26.task2611;

import java.util.concurrent.ConcurrentHashMap;

public class Producer implements Runnable {
    private ConcurrentHashMap<String, String> map;

    public Producer(ConcurrentHashMap<String, String> map){
        this.map = map;
    }


    public void run(){
        Integer i=0;
        while (true){

            try{
                i++;
                map.put(i.toString(),"Some text for "+i.toString());
                Thread.sleep(500);


            }catch(Exception e){
                System.out.println("["+Thread.currentThread().getName()+"] thread was terminated");
            }
        }

    }


}
