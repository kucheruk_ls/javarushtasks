package com.javarush.task.task26.task2603;


import java.util.Comparator;


/*
Убежденному убеждать других не трудно
*/
public class Solution {

    public static void main(String[] args) {

    }

    public static class CustomizedComparator<T> implements Comparator<T> {
        private Comparator<T>[] comparators;

        public CustomizedComparator(Comparator<T>... comparators){
            if(comparators.length>0){
                this.comparators=comparators;
            }else{
                throw new NullPointerException();
            }
        }

        public int compare(T o1, T o2){
            int rtmp = 0;
            for(Comparator comparator : comparators){
                rtmp = comparator.compare(o1,o2);
                if(rtmp!=0)break;
            }
            return rtmp;
        }




    }
}
