package com.javarush.task.task36.task3608.model;

import com.javarush.task.task36.task3608.bean.User;

import java.util.ArrayList;
import java.util.List;

public class FakeModel implements Model {
    private ModelData modelData = new ModelData();
    @Override
    public ModelData getModelData() {
        return modelData;
    }
    public void loadUsers(){

        ArrayList<User> users = new ArrayList<User>();
        users.add(new User("Ivan", 1,1));
        users.add(new User("Petr", 1, 1));
        modelData.setUsers(users);
    }

    public void loadDeletedUsers() {
        throw new UnsupportedOperationException();
    }

    public void loadUserById(long userId) {
        throw new UnsupportedOperationException();
    }

    public void deleteUserById(long id){
        throw new UnsupportedOperationException();
    }
    public void changeUserData(String name, long id, int level){
        throw new UnsupportedOperationException();
    }
}
