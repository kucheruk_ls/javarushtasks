package com.javarush.task.task32.task3209;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class HTMLFileFilter extends FileFilter {

    public HTMLFileFilter() {
        super();
    }

    @Override
    public String getDescription() {
        return "HTML и HTM файлы";
    }

    @Override
    public boolean accept(File f) {
        /*
        Переопредели метод accept(File file), чтобы он возвращал true, если переданный файл директория или содержит
        в конце имени ".html" или ".htm" без учета регистра.
         */
        boolean result = false;
        if(f.isDirectory())result = true;
        String NameFile = f.getName().toLowerCase();
        if((NameFile.endsWith(".html"))||(NameFile.endsWith(".htm")))result = true;
        return result;
    }
}
