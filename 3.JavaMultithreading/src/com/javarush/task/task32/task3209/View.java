package com.javarush.task.task32.task3209;

import com.javarush.task.task32.task3209.listeners.FrameListener;
import com.javarush.task.task32.task3209.listeners.TabbedPaneChangeListener;
import com.javarush.task.task32.task3209.listeners.UndoListener;

import javax.swing.*;
import javax.swing.text.html.HTMLDocument;
import javax.swing.undo.UndoManager; //класс для отката изменения
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class View extends JFrame implements ActionListener {
    private Controller controller;
    private JTabbedPane tabbedPane = new JTabbedPane();//панель с двумя вкладками
    private JTextPane htmlTextPane = new JTextPane();//это будет компонент для визуального редактирования html.Он будет размещен на первой вкладке.
    private JEditorPane plainTextPane = new JEditorPane();//это будет компонент для редактирования html в виде текста, он будет отображать код html (теги и их содержимое).
    private UndoManager undoManager = new UndoManager();
    private UndoListener undoListener = new UndoListener(undoManager);


    public View(){
        try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch (Exception e){
            ExceptionHandler.log(e);
        }
    }

    public void update(){
        /*
        который должен получать документ у контроллера и устанавливать его в панель редактирования htmlTextPane.
         */
        HTMLDocument document = controller.getDocument();
        htmlTextPane.setDocument(document);

    }

    public void showAbout(){
        /*
        который должен показывать диалоговое окно с информацией о программе. Информацию придумай
        сам, а вот тип сообщения должен быть JOptionPane.INFORMATION_MESSAGE.
         */
        JPanel panel = new JPanel();
        JOptionPane.showMessageDialog(panel,"Program HTML editor","0.1",JOptionPane.INFORMATION_MESSAGE);


    }

    public UndoListener getUndoListener(){
        return this.undoListener;
    }

    public void init(){ //отвечает за инициализацию объекта View
/*
Вызывать инициализацию графического интерфейса initGui().
4.3.2. Добавлять слушателя событий нашего окна. В качестве подписчика создай и используй объект класса FrameListener.
В качестве метода для добавления подписчика используй подходящий метод из класса Window от которого наследуется и наш
класс через классы JFrame и Frame.
4.3.3. Показывать наше окно. Используй метод setVisible с правильным параметром.
На этом этапе приложение при запуске должно показывать окно, которое можно растягивать, разворачивать, закрыть и т.д.
 */

        initGui();
        FrameListener frameListener = new FrameListener(this);
        addWindowListener(frameListener);
        setVisible(true);
    }

    public void initMenuBar(){
            /*
            9.1.1. Создавать новый объект типа JMenuBar. Это и будет наша панель меню.
            9.1.2. С помощью MenuHelper инициализировать меню в следующем порядке: Файл, Редактировать, Стиль, Выравнивание, Цвет, Шрифт и Помощь.
            9.1.3. Добавлять в верхнюю часть панели контента текущего фрейма нашу панель меню, аналогично тому, как это мы делали с панелью вкладок.
             */
            JMenuBar menuBar = new JMenuBar();
            MenuHelper.initFileMenu(this,menuBar);
            MenuHelper.initEditMenu(this,menuBar);
            MenuHelper.initStyleMenu(this,menuBar);
            MenuHelper.initAlignMenu(this,menuBar);
            MenuHelper.initColorMenu(this,menuBar);
            MenuHelper.initFontMenu(this,menuBar);
            MenuHelper.initHelpMenu(this,menuBar);
            getContentPane().add(menuBar,BorderLayout.NORTH);

    }

    public void initEditor(){ // инициализация панелей редактора
        htmlTextPane.setContentType("text/html");
        JScrollPane scrollPaneHtml = new JScrollPane(htmlTextPane);
        tabbedPane.addTab("HTML",scrollPaneHtml);
        JScrollPane scrollPaneText = new JScrollPane(plainTextPane);
        tabbedPane.addTab("Текст",scrollPaneText);

        tabbedPane.setPreferredSize(new Dimension(500,500));
        TabbedPaneChangeListener tabbedPaneChangeListener = new TabbedPaneChangeListener(this);
        tabbedPane.addChangeListener(tabbedPaneChangeListener);
        getContentPane().add(tabbedPane,BorderLayout.CENTER);

    }

    public void selectHtmlTab(){
        tabbedPane.setSelectedIndex(0);
        this.resetUndo();

    }
    public void selectedTabChanged(){
        /*
        18.1. Метод должен проверить, какая вкладка сейчас оказалась выбранной.
        18.2. Если выбрана вкладка с индексом 0 (html вкладка), значит нам нужно получить текст из plainTextPane и
        установить его в контроллер с помощью метода setPlainText.
        18.3. Если выбрана вкладка с индексом 1 (вкладка с html текстом), то необходимо получить текст у контроллера
        с помощью метода getPlainText() и установить его в панель plainTextPane.
        18.4. Сбросить правки (вызвать метод resetUndo представления).
         */
        int idx = tabbedPane.getSelectedIndex();
        if(idx==0){
            controller.setPlainText(plainTextPane.getText());
        }else if(idx==1){
            plainTextPane.setText(controller.getPlainText());
        }
        resetUndo();
    }



    public void initGui(){
        initMenuBar();
        initEditor();
        pack();// отвечает за установку размеров окна
    }

    public void exit(){
        controller.exit();
    }


    public Controller getController() {
        return controller;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    //имплементация интерфейса ActionListener
    @Override
    public void actionPerformed(ActionEvent e) {
        /*
        Реализуем метод actionPerformed(ActionEvent actionEvent) у представления, этот метод наследуется от интерфейса
        ActionListener и будет вызваться при выборе пунктов меню, у которых наше представление указано в виде слушателя событий.
19.1. Получи из события команду с помощью метода getActionCommand(). Это будет обычная строка. По этой строке ты можешь
понять какой пункт меню создал данное событие.
19.2. Если это команда "Новый", вызови у контроллера метод createNewDocument(). В этом пункте и далее, если необходимого
метода в контроллере еще нет - создай заглушки.
19.3. Если это команда "Открыть", вызови метод openDocument().
19.4. Если "Сохранить", то вызови saveDocument().
19.5. Если "Сохранить как..." - saveDocumentAs().
19.6. Если "Выход" - exit().
19.7. Если "О программе", то вызови метод showAbout() у представления.
Проверь, что заработали пункты меню Выход и О программе.
         */
        String MenuCommand = e.getActionCommand();
        switch (MenuCommand){
            case "Новый": controller.createNewDocument();
            break;
            case "Открыть": controller.openDocument();
            break;
            case "Сохранить": controller.saveDocument();
            break;
            case "Сохранить как...": controller.saveDocumentAs();
            break;
            case "Выход": controller.exit();
            break;
            case "О программе": this.showAbout();
            break;
        }
    }


    public boolean canUndo(){
        return undoManager.canUndo();
    }
    public boolean canRedo(){
        return undoManager.canRedo();
    }

    public void undo(){
        try{
            this.undoManager.undo();
        }catch(Exception e){
            ExceptionHandler.log(e);
        }
    }

    public void redo(){
        try{
            this.undoManager.redo();
        }catch(Exception e){
            ExceptionHandler.log(e);
        }
    }

    public void resetUndo(){
        this.undoManager.discardAllEdits();
    }

    public boolean isHtmlTabSelected(){
        return tabbedPane.getSelectedIndex()==0?true:false;

    }
}
