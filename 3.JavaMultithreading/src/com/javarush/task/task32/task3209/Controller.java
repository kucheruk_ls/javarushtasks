package com.javarush.task.task32.task3209;

import com.javarush.task.task32.task3209.listeners.UndoListener;

import javax.swing.*;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import java.io.*;

public class Controller {
    private View view;
    private HTMLDocument document;
    private File currentFile;

    public void init(){ //отвечает за инициализацию контролера
        /*
        Реализуй метод инициализации init() контроллера. Он должен просто вызывать метод создания нового документа.
Проверь работу пункта меню Новый.
         */
        createNewDocument();
    }

    public HTMLDocument getDocument(){
        return document;
    }

    public void resetDocument(){
        /*
        Он должен:
        15.1. Удалять у текущего документа document слушателя правок которые можно отменить/
        вернуть (найди подходящий для этого метод, унаследованный от AbstractDocument).
        Слушателя нужно запросить у представления (метод getUndoListener()).
        Не забудь проверить, что текущий документ существует (не null).
        15.2. Создавать новый документ по умолчанию и присваивать его полю document.

        Подсказка: воспользуйся подходящим методом класса HTMLEditorKit.

        15.3. Добавлять новому документу слушателя правок.
        15.4. Вызывать у представления метод update().
         */

        if(document != null) {

                document.removeUndoableEditListener(view.getUndoListener());
        }
        HTMLEditorKit htmlEditorKit = new HTMLEditorKit();
        document  = (HTMLDocument) htmlEditorKit.createDefaultDocument();
        document.addUndoableEditListener(view.getUndoListener());
        view.update();

    }

    public Controller(View view){
        this.view = view;
    }

    public void exit(){
        System.exit(0);
    }

    public void setPlainText(String text){
        /*
        Сбрось документ.
16.2. Создай новый реадер StringReader на базе переданного текста.
16.3. Вызови метод read() из класса HTMLEditorKit, который вычитает данные из реадера в
документ document.
16.4. Проследи, чтобы метод не кидал исключения. Их необходимо просто логировать.
         */
        resetDocument();
        StringReader reader = new StringReader(text);
        HTMLEditorKit htmlEditorKit = new HTMLEditorKit();
        try{
            htmlEditorKit.read(reader,document,0);
        }catch (Exception e){
            ExceptionHandler.log(e);
        }
    }

    public  String getPlainText(){
        /*
        Класс Controller должен содержать публичный метод String getPlainText().
2. В методе getPlainText() должен создаваться объект класса StringWriter.
3. Метод getPlainText() должен вызывать метод write() у объекта HTMLEditorKit.
4. Если в методе getPlainText() возникнет исключительная ситуация, то исключение должно логироваться через метод log у класса ExceptionHandler.
5. Метод getPlainText() должен возвращать текст из документа со всеми html тегами.
         */
        StringWriter stringWriter = new StringWriter();
        HTMLEditorKit htmlEditorKit = new HTMLEditorKit();
        int len = document.getLength();
        try{
            htmlEditorKit.write(stringWriter, document,0,len);
        }catch (Exception e){
            ExceptionHandler.log(e);
        }
        return stringWriter.toString();
    }
    public void createNewDocument(){
        /*
        Он должен:
20.1.1. Выбирать html вкладку у представления.
20.1.2. Сбрасывать текущий документ.
20.1.3. Устанавливать новый заголовок окна, например: "HTML редактор". Воспользуйся методом setTitle(), который унаследован в нашем представлении.
20.1.4. Сбрасывать правки в Undo менеджере. Используй метод resetUndo представления.
20.1.5. Обнулить переменную currentFile.
         */
        view.selectHtmlTab();
        resetDocument();
        view.setTitle("HTML редактор");
        view.resetUndo();
        currentFile = null;
    }

    public void openDocument(){
        /*
        Пришло время реализовать метод openDocument(). Метод должен работать аналогично методу saveDocumentAs(), в той части, которая отвечает за выбор файла.

Подсказка: Обрати внимание на имя метода для показа диалогового окна.

Когда файл выбран, необходимо:
23.2.1. Установить новое значение currentFile.
23.2.2. Сбросить документ.
23.2.3. Установить имя файла в заголовок у представления.
23.2.4. Создать FileReader, используя currentFile.
23.2.5. Вычитать данные из FileReader-а в документ document с помощью объекта класса HTMLEditorKit.
23.2.6. Сбросить правки (вызвать метод resetUndo представления).
23.2.7. Если возникнут исключения - залогируй их и не пробрасывай наружу.
Проверь работу пунктов меню Сохранить и Открыть.
         */
        view.selectHtmlTab();
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new HTMLFileFilter());
        int chose = fileChooser.showOpenDialog(view);
        if(chose == JFileChooser.APPROVE_OPTION){
            currentFile = fileChooser.getSelectedFile();
            resetDocument();
            view.setTitle(currentFile.getName());
            try{
                FileReader fileReader = new FileReader(currentFile);
                new HTMLEditorKit().read(fileReader,document,0);
                fileReader.close();
                view.resetUndo();

            }catch (Exception e){
                ExceptionHandler.log(e);
            }
        }
    }

    public void saveDocument(){
        /*
        23.1. Напишем метод для сохранения открытого файла saveDocument(). Метод должен работать также,
        как saveDocumentAs(), но не запрашивать файл у пользователя, а использовать currentFile. Если currentFile равен
        null, то вызывать метод saveDocumentAs().
         */
        if(currentFile!=null){
            view.selectHtmlTab();
            try{

                FileWriter fileWriter = new FileWriter(currentFile);
                new HTMLEditorKit().write(fileWriter,document,0,document.getLength());
                fileWriter.close();
            }catch (Exception e){
                ExceptionHandler.log(e);
            }
        }else{
            saveDocumentAs();
        }
    }

    public void saveDocumentAs(){
        /*
        22.1. Переключать представление на html вкладку.
        22.2. Создавать новый объект для выбора файла JFileChooser.
        22.3. Устанавливать ему в качестве фильтра объект HTMLFileFilter.
        22.4. Показывать диалоговое окно "Save File" для выбора файла.
        22.5. Если пользователь подтвердит выбор файла:
        22.5.1. Сохранять выбранный файл в поле currentFile.
        22.5.2. Устанавливать имя файла в качестве заголовка окна представления.
        22.5.3. Создавать FileWriter на базе currentFile.
        22.5.4. Переписывать данные из документа document в объекта FileWriter-а аналогично тому, как мы это делали в методе getPlainText().
        StringWriter stringWriter = new StringWriter();
        HTMLEditorKit htmlEditorKit = new HTMLEditorKit();
        int len = document.getLength();
        try{
            htmlEditorKit.write(stringWriter, document,0,len);
        }catch (Exception e){
            ExceptionHandler.log(e);
        }
        return stringWriter.toString();
         */
        view.selectHtmlTab();

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new HTMLFileFilter());
        int chose = fileChooser.showSaveDialog(view);
        if(chose == JFileChooser.APPROVE_OPTION){
            currentFile = fileChooser.getSelectedFile();
            view.setTitle(currentFile.getName());
            try{
                FileWriter fileWriter = new FileWriter(currentFile);
                new HTMLEditorKit().write(fileWriter,document,0,document.getLength());
                fileWriter.close();
            }catch (Exception e){
                ExceptionHandler.log(e);
            }

        }


/*
        fileChooser.showSaveDialog(view);
        currentFile = fileChooser.getSelectedFile();
        view.setTitle(fileChooser.getSelectedFile().getName().toString());
        try{
            FileWriter fileWriter = new FileWriter(currentFile);
            new HTMLEditorKit().write(fileWriter,document,0,document.getLength());
            fileWriter.close();
        }catch (Exception e){
            ExceptionHandler.log(e);
        }

*/
    }

    public static void main(String[] args){
        View view = new View();
        Controller controller = new Controller(view);
        view.setController(controller);
        view.init();
        controller.init();


    }
}
