package com.javarush.task.task30.task3010;

/* 
Минимальное допустимое основание системы счисления
*/

public class Solution {
    public static void main(String[] args) {
        try {
            String s = args[0];
            s = s.toUpperCase();
            if (!s.matches("[A-Z0-9]+")) {
                System.out.println("incorrect");
                return;
            }
            char[] ch = s.toCharArray();
            byte[] b = new byte[ch.length];
            byte bmax = 48;
            int osnSSch = 2;
            for (int i = 0; i < ch.length; i++) {
                b[i] = (byte) ch[i];

                if (b[i] > bmax) bmax = b[i];


                //System.out.println(b[i]);
            }
            if (bmax > 47 && bmax < 58) {
                if (bmax == 48 || bmax == 49) osnSSch = 2;
                int count = 2;
                for (int i2 = 50; i2 < 58; i2++) {
                    count++;
                    if (bmax == i2) {
                        osnSSch = count;
                        break;
                    }
                }
            } else if (bmax > 64 && bmax < 91) {
                int count = 10;
                for (int i3 = 65; i3 < 91; i3++) {
                    count++;
                    if (bmax == i3) {
                        osnSSch = count;
                        break;
                    }
                }
            }

            System.out.println(osnSSch);
        }catch (Exception e){
            System.out.println("incorrect");
        }
    }
}