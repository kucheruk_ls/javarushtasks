package com.javarush.task.task30.task3008;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleHelper {

    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void writeMessage(String message){
        System.out.println(message);
    }

    public static String readString(){
        boolean okin = false;
        String s = "";
        while (!okin) {
            try {

                s = reader.readLine();
                okin = true;
            } catch (IOException e) {
                System.out.println("Произошла ошибка при попытке ввода текста. Попробуйте еще раз.");

            }
        }

        return s;
    }

    public static int readInt(){
        boolean inok = false;
        int ino = 0;
        while (!inok){
            try{
                ino = Integer.parseInt(readString());
                inok = true;
            }catch (NumberFormatException e){
                System.out.println("Произошла ошибка при попытке ввода числа. Попробуйте еще раз.");
            }
        }

       return ino;

    }
}
