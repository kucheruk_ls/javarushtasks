package com.javarush.task.task30.task3008.client;

public class ClientGuiController extends Client {
    private ClientGuiModel model = new ClientGuiModel();
    private ClientGuiView view = new ClientGuiView(this);

    /*
    protected GuiSocketThread getSocketThread(){
        /*
        Метод getSocketThread в классе ClientGuiController должен возвращать новый объект типа GuiSocketThread.

        return new GuiSocketThread();
    }
*/

    @Override
    protected SocketThread getSocketThread() {
        return new GuiSocketThread();
    }

    public void run(){
        SocketThread guiSocketThread = getSocketThread();
        guiSocketThread.run();

    }

    @Override
    protected String getServerAddress() {
        return view.getServerAddress();
    }

    @Override
    protected int getServerPort() {
        return view.getServerPort();
    }

    public ClientGuiModel getModel(){
        return model;
    }

    @Override
    protected String getUserName() {
        return view.getUserName();
    }

    public static void main(String[] args){
        ClientGuiController clientGuiController = new ClientGuiController();
        clientGuiController.run();
    }

    public class GuiSocketThread extends SocketThread{


        public void processIncomingMessage(String message){
            /*
            должен устанавливать новое сообщение у модели и вызывать обновление вывода сообщений у представления.
             */

            model.setNewMessage(message);
            view.refreshMessages();

        }

        public void informAboutAddingNewUser(String userName){
            /*
         - должен добавлять нового пользователя в модель и вызывать обновление вывода пользователей у отображения.
             */

            model.addUser(userName);
            view.refreshUsers();
        }
        public void informAboutDeletingNewUser(String userName){
            /*
            должен удалять пользователя из модели и вызывать обновление вывода пользователей у отображения
             */
            model.deleteUser(userName);
            view.refreshUsers();

        }

        public void notifyConnectionStatusChanged(boolean clientConnected){
            view.notifyConnectionStatusChanged(clientConnected);
        }


    }
}
