package com.javarush.task.task30.task3008.client;

import com.javarush.task.task30.task3008.ConsoleHelper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BotClient extends Client {


    @Override
    protected boolean shouldSendTextFromConsole() {
        return false;
    }

    @Override
    protected SocketThread getSocketThread() {
        return new BotSocketThread();
    }

    @Override
    protected String getUserName() {
        int x = (int) (Math.random()*100);
        String bn = "date_bot_"+x;
        return bn;
    }


    public class BotSocketThread extends SocketThread{
        @Override
        protected void clientMainLoop() throws IOException, ClassNotFoundException {
            sendTextMessage("Привет чатику. Я бот. Понимаю команды: дата, день, месяц, год, время, час, минуты, секунды.");
            super.clientMainLoop();
        }

        @Override
        protected void processIncomingMessage(String message) {
            ConsoleHelper.writeMessage(message);
            if (message.contains(":")) {
                String[] mass = message.split(":");
                String userName = mass[0].trim();
                String query = mass[1].trim();
                StringBuffer stringBuffer = new StringBuffer("Информация для ");
                stringBuffer.append(userName + ": ");
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                String date = null;
                if (query.equalsIgnoreCase("дата")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d.MM.YYYY");
                    date = simpleDateFormat.format(calendar.getTime());
                    stringBuffer.append(date);
                    sendTextMessage(stringBuffer.toString());

                } else if (query.equalsIgnoreCase("день")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d");
                    date = simpleDateFormat.format(calendar.getTime());
                    stringBuffer.append(date);
                    sendTextMessage(stringBuffer.toString());
                } else if (query.equalsIgnoreCase("месяц")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM");
                    date = simpleDateFormat.format(calendar.getTime());
                    stringBuffer.append(date);
                    sendTextMessage(stringBuffer.toString());
                } else if (query.equalsIgnoreCase("год")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY");
                    date = simpleDateFormat.format(calendar.getTime());
                    stringBuffer.append(date);
                    sendTextMessage(stringBuffer.toString());
                } else if (query.equalsIgnoreCase("время")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("H:mm:ss");
                    date = simpleDateFormat.format(calendar.getTime());
                    stringBuffer.append(date);
                    sendTextMessage(stringBuffer.toString());
                } else if (query.equalsIgnoreCase("час")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("H");
                    date = simpleDateFormat.format(calendar.getTime());
                    stringBuffer.append(date);
                    sendTextMessage(stringBuffer.toString());
                } else if (query.equalsIgnoreCase("минуты")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("m");
                    date = simpleDateFormat.format(calendar.getTime());
                    stringBuffer.append(date);
                    sendTextMessage(stringBuffer.toString());
                } else if (query.equalsIgnoreCase("секунды")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("s");
                    date = simpleDateFormat.format(calendar.getTime());
                    stringBuffer.append(date);
                    sendTextMessage(stringBuffer.toString());
                }


                //super.processIncomingMessage(message);
            }
        }
    }

    public static void main(String[] args){
        BotClient botClient = new BotClient();
        botClient.run();
    }
}
