package com.javarush.task.task30.task3008.client;

import com.javarush.task.task30.task3008.Connection;
import com.javarush.task.task30.task3008.ConsoleHelper;
import com.javarush.task.task30.task3008.Message;
import com.javarush.task.task30.task3008.MessageType;


import java.awt.*;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


public class Client {
    protected Connection connection;
    private volatile boolean clientConnected = false;

    protected String getServerAddress(){
        /*
        должен запросить ввод адреса сервера у пользователя и вернуть введенное значение. Адрес может быть строкой,
        содержащей ip, если клиент и сервер запущен на разных машинах или 'localhost', если клиент и сервер работают на одной машине.
         */

        String serverAddr = null;

        while (serverAddr==null){
        System.out.println("Введите адрес сервера:");
          serverAddr = ConsoleHelper.readString();


        }
        return serverAddr;
    }

    protected int getServerPort(){
        System.out.println("Введите номер порта сервера:");

        return ConsoleHelper.readInt();
    }

    protected String getUserName(){
        System.out.println("Введите имя пользователя");
       return ConsoleHelper.readString();
    }

    protected  boolean shouldSendTextFromConsole(){
        return true;
    }

    protected SocketThread getSocketThread(){
        SocketThread socketThread = new SocketThread();
        return socketThread;
    }

    protected void sendTextMessage(String text){


        try{
            Message message = new Message(MessageType.TEXT, text);
            connection.send(message);
        }catch (IOException e){
            clientConnected = false;
            ConsoleHelper.writeMessage("Не удалось отправить сообщение");

        }

    }

    public void run(){
        SocketThread socketThread = getSocketThread();
        socketThread.setDaemon(true);
        socketThread.start();
        try{

            synchronized (this) {
                wait();
            }
        }catch (InterruptedException e){
            ConsoleHelper.writeMessage("Соединение с сервером потеряно");
            System.exit(1);
        }
        if(clientConnected){
            ConsoleHelper.writeMessage("Соединение установлено. Для выхода наберите команду 'exit'.");
            boolean seans = true;
            while (clientConnected){
                String textMsg = ConsoleHelper.readString();

                if(textMsg.equalsIgnoreCase("exit")){
                    break;
                }else{
                    if(shouldSendTextFromConsole()) {
                        sendTextMessage(textMsg);
                    }
                }
            }
        }else{
            ConsoleHelper.writeMessage("Произошла ошибка во время работы клиента.");
        }


    }


    public class SocketThread extends Thread{
        protected void processIncomingMessage(String message){
            /*
            должен выводить текст message в консоль
             */
            ConsoleHelper.writeMessage(message);

        }
        protected void informAboutAddingNewUser(String userName){
            /*
            должен выводить в консоль информацию о том, что участник с именем userName присоединился к чату.
             */
            ConsoleHelper.writeMessage(userName+" присоединился к чату");
        }

        protected void informAboutDeletingNewUser(String userName){
            /*
          - должен выводить в консоль, что участник с именем userName покинул чат.
             */
            ConsoleHelper.writeMessage(userName+" покинул чат");

        }

        protected void notifyConnectionStatusChanged(boolean clientConnected){
            /*
            а) Устанавливать значение поля clientConnected внешнего объекта Client в соответствии с переданным параметром.
            б) Оповещать (пробуждать ожидающий) основной поток класса Client.

Подсказка: используй синхронизацию на уровне текущего объекта внешнего класса и метод notify. Для класса SocketThread внешним классом является класс Client.
             */
            Client.this.clientConnected = clientConnected;
            synchronized (Client.this){
                Client.this.notify();
            }

        }

        protected void clientHandshake() throws IOException, ClassNotFoundException{
            /*

             */
            while (true){
                Message message = connection.receive();
                if(message.getType()==MessageType.NAME_REQUEST){
                    String userName = getUserName();
                    Message reMsg = new Message(MessageType.USER_NAME, userName);
                    connection.send(reMsg);
                }else if(message.getType()==MessageType.NAME_ACCEPTED){
                    notifyConnectionStatusChanged(true);
                    break;
                }else{
                    throw new IOException("Unexpected MessageType");
                }
            }


        }

        protected void clientMainLoop() throws IOException, ClassNotFoundException{

            while (true){
                Message message = connection.receive();

                if(message.getType()==MessageType.TEXT){
                    processIncomingMessage(message.getData());
                }else if(message.getType()==MessageType.USER_ADDED){
                    informAboutAddingNewUser(message.getData());
                }else if(message.getType()==MessageType.USER_REMOVED){
                    informAboutDeletingNewUser(message.getData());
                }else{
                    throw new IOException("Unexpected MessageType");
                }

            }

        }

        public void run(){
            String serverAddres = getServerAddress();
            int serverPort = getServerPort();

            try{
                Socket socket = new Socket(serverAddres,serverPort);
                connection = new Connection(socket);

                clientHandshake();
                clientMainLoop();
            }catch (UnknownHostException e){
                ConsoleHelper.writeMessage("Не верный адрес сервера");
            }catch (IOException e){
                ConsoleHelper.writeMessage("Не верный порт или адрес");
                notifyConnectionStatusChanged(false);
            }catch (ClassNotFoundException e){
                ConsoleHelper.writeMessage("Не известный тип сообщения");
                notifyConnectionStatusChanged(false);
            }

        }


    }

    public static void main(String[] args){
        Client client = new Client();
        client.run();
    }

}
