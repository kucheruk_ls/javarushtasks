package com.javarush.task.task30.task3008;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Server {
    private static Map<String, Connection> connectionMap = new ConcurrentHashMap<>();



    public static  void sendBroadcastMessage(Message message){
        try{
            for(Map.Entry<String, Connection> entry:connectionMap.entrySet()){
                Connection connection = entry.getValue();
                connection.send(message);
            }
        }catch (IOException e){
            System.out.println("Сообщение клиенту отправить не удалось");
        }
    }

    private static class Handler extends Thread {
        private Socket socket;

        public Handler(Socket socket) {
            this.socket = socket;
        }

        public void run(){
            ConsoleHelper.writeMessage("Установлено соединение с адресом "+socket.getRemoteSocketAddress());

            String thisUserName = null;
            try(Connection connection = new Connection(socket)){

                thisUserName = serverHandshake(connection);
                Message message = new Message(MessageType.USER_ADDED,thisUserName);
                sendBroadcastMessage(message);
                sendListOfUsers(connection,thisUserName);
                serverMainLoop(connection,thisUserName);


            }catch (IOException | ClassNotFoundException e){
                ConsoleHelper.writeMessage("Произошла ошибка при обмене данными с "+socket.getRemoteSocketAddress());

            }finally {
                if(thisUserName!=null) {
                    Message message = new Message(MessageType.USER_REMOVED, thisUserName);
                    connectionMap.remove(thisUserName);
                    sendBroadcastMessage(message);
                }
                ConsoleHelper.writeMessage("Сообщение с удаленным адресом "+socket.getRemoteSocketAddress()+" закрыто");
            }

        }




        private String serverHandshake(Connection connection) throws IOException, ClassNotFoundException {
            Message message = new Message(MessageType.NAME_REQUEST, "Введите свое имя");
            boolean nameok = false;
            String returnname = null;
            while (!nameok) {
                connection.send(message);
                Message resiveMessage = connection.receive();
                if (resiveMessage.getType() == MessageType.USER_NAME) {
                    String resData = resiveMessage.getData();
                    if (!resData.isEmpty()) {
                        if (!connectionMap.containsKey(resData)) {
                            returnname = resData;
                            nameok = true;
                        } else {
                            message = new Message(MessageType.NAME_REQUEST, "Данное имя уже используется, введите другое");
                            //connection.send(message);
                        }
                    } else {
                        message = new Message(MessageType.NAME_REQUEST, "Имя не может быть пустым. Введите имя");
                        //connection.send(message);
                    }
                } else {
                    message = new Message(MessageType.NAME_REQUEST, "Не верный тип сообщения. Введите имя пользователя");
                }


            }
            connectionMap.put(returnname, connection);
            message = new Message(MessageType.NAME_ACCEPTED, "Добро пожаловать в наш чат!");
            connection.send(message);
            return returnname;
        }

        private void sendListOfUsers(Connection connection, String userName) throws IOException{

            for(Map.Entry<String, Connection> entry:connectionMap.entrySet()){
                if(!entry.getKey().equals(userName)){
                    Message message = new Message(MessageType.USER_ADDED,entry.getKey());
                    connection.send(message);
                }
            }

        }

        private void serverMainLoop(Connection connection, String userName) throws IOException, ClassNotFoundException{

            while (true) {
                Message reseiveMessage = connection.receive();

                if (reseiveMessage.getType() == MessageType.TEXT) {
                    Message BroadCastMsg = new Message(MessageType.TEXT, userName+": "+reseiveMessage.getData());
                    Server.sendBroadcastMessage(BroadCastMsg);
                } else {
                    ConsoleHelper.writeMessage("Не верный тип сообщения!");
                }
            }
        }

    }
        public static void main(String[] args) {

            ConsoleHelper consoleHelper = new ConsoleHelper();
            int port = consoleHelper.readInt();
            ServerSocket serverSocket = null;
            try {
                serverSocket = new ServerSocket(port);
                System.out.println("Сервер запущен!");
                while (true) {
                    Socket socket = serverSocket.accept();
                    Handler handler = new Handler(socket);
                    handler.start();
                }


            } catch (IOException e) {
                System.out.println("Сервер упал");

            } finally {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }

}
