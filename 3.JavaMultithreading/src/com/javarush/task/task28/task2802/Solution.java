package com.javarush.task.task28.task2802;


import java.util.ArrayList;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* 
Пишем свою ThreadFactory
*/
public class Solution {

    public static void main(String[] args) {
        class EmulateThreadFactoryTask implements Runnable {
            @Override
            public void run() {
                emulateThreadFactory();
            }
        }

        ThreadGroup group = new ThreadGroup("firstGroup");
        Thread thread = new Thread(group, new EmulateThreadFactoryTask());

        ThreadGroup group2 = new ThreadGroup("secondGroup");
        Thread thread2 = new Thread(group2, new EmulateThreadFactoryTask());

        thread.start();
        thread2.start();
    }

    private static void emulateThreadFactory() {
        AmigoThreadFactory factory = new AmigoThreadFactory();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        };
        factory.newThread(r).start();
        factory.newThread(r).start();
        factory.newThread(r).start();
    }

    public static class AmigoThreadFactory implements ThreadFactory{
        static AtomicInteger poolnum = new AtomicInteger(0);
        static ArrayList<String> groups = new ArrayList<>();
        final AtomicInteger threadnum = new AtomicInteger(0);
        @Override

        public Thread newThread(Runnable r) {
            Thread nameT = new Thread(r);
            String groupName = nameT.getThreadGroup().getName();
            if(!groups.contains(groupName)){
                groups.add(groupName);
                AmigoThreadFactory.poolnum.getAndIncrement();
            }


            //Thread nameT = new Thread(r);
            threadnum.getAndIncrement();
            nameT.setDaemon(false);
            nameT.setPriority(Thread.NORM_PRIORITY);
            /*
            1.3. имя трэда должно иметь шаблон "GN-pool-A-thread-B",
где GN - это имя группы,
A - это номер фабрики инкрементируется в пределах класса начиная с 1, используйте AtomicInteger,
B - номер треда инкрементируется в пределах конкретной фабрики начиная с 1, используйте AtomicInteger.
             */
            ;
            String gn = nameT.getThreadGroup().getName()+"-pool-"+poolnum.toString()+"-thread-"+threadnum.toString();
            nameT.setName(gn);

            return nameT;
        }
    }
}
