package com.javarush.task.task28.task2805;

import java.util.concurrent.atomic.AtomicInteger;

public class MyThread extends Thread {
    static AtomicInteger thredPriority = new AtomicInteger(1);

    void setThredPriority(){

       if(thredPriority.get()<10) setPriority(thredPriority.getAndIncrement());
       else{
           setPriority(thredPriority.get());
           thredPriority.set(1);
       }
    }


    public MyThread() {
        setThredPriority();
    }

    public MyThread(Runnable target) {
        super(target);
        setThredPriority();
    }

    public MyThread(ThreadGroup group, Runnable target) {
        super(group, target);
        setThredPriority();
    }

    public MyThread(String name) {
        super(name);
        setThredPriority();
    }

    public MyThread(ThreadGroup group, String name) {
        super(group, name);
        setThredPriority();
    }

    public MyThread(Runnable target, String name) {
        super(target, name);
        setThredPriority();
    }

    public MyThread(ThreadGroup group, Runnable target, String name) {
        super(group, target, name);
        setThredPriority();
    }

    public MyThread(ThreadGroup group, Runnable target, String name, long stackSize) {
        super(group, target, name, stackSize);
        setThredPriority();
    }
}
