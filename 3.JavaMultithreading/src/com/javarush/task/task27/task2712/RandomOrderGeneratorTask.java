package com.javarush.task.task27.task2712;

import java.util.ArrayList;
import java.util.List;

/**
 * В отдельном классе создай таск (Runnable) RandomOrderGeneratorTask. Этот таск должен:
 * 2.1. Хранить список всех планшетов
 * 2.2. Используя Math.random выбирать случайный планшет.
 * 2.3. У RandomOrderGeneratorTask должен быть только один единственный метод.
 * 2.4. Генерировать случайный заказ каждые ORDER_CREATING_INTERVAL миллисекунд для планшета из п.2.2.
 * (не печатай стек-трейс)
 */
public class RandomOrderGeneratorTask implements Runnable {
    private List<Tablet> tabletList = new ArrayList<>();
    private int interval;

    public RandomOrderGeneratorTask(List<Tablet> tablets, int interval){
        this.tabletList = tablets;
        this.interval = interval;
    }

    @Override
    public void run() {
        int rndTabletNum = (int) (Math.random()*tabletList.size());
        Tablet tablet = tabletList.get(rndTabletNum);
        while (!Thread.currentThread().isInterrupted()) {
            tablet.createTestOrder();
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {

            }
        }
    }
}
