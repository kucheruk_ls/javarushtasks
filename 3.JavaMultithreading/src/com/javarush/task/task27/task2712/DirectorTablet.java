package com.javarush.task.task27.task2712;

import com.javarush.task.task27.task2712.ad.Advertisement;
import com.javarush.task.task27.task2712.ad.StatisticAdvertisementManager;
import com.javarush.task.task27.task2712.statistic.StatisticManager;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DirectorTablet {

    /**
     * какую сумму заработали на рекламе, сгруппировать по дням;
     * Используя метод statisticAdvertisement() класса StatisticManager из предыдущего пункта вывести в консоль в
     * убывающем порядке даты и суммы.
     * Для каждой даты из хранилища событий, для которой есть показанная реклама, должна выводится
     * сумма прибыли за показы рекламы для этой даты.
     * В конце вывести слово Total и общую сумму.
     */
    void printAdvertisementProfit(){
        StatisticManager statisticManager = StatisticManager.getInstance();
        Map<String, Double> statMap = statisticManager.statisticAdvertisement();
        //для сортировки создадим сотрированню карту со своим компаратором и отсортируем все
               TreeMap<String, Double> tmapa = new TreeMap<>(new Comparator<String>() {
                   @Override
                   public int compare(String o1, String o2) {
                       return o2.compareTo(o1);
                   }
               });
        tmapa.putAll(statMap);

        // что бы вывести как требуют обратим наши строки в дату и потом будем форматировать дату как нам нужно
        SimpleDateFormat sDFString = new SimpleDateFormat("yyyy/MM/dd",Locale.ENGLISH);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy",Locale.ENGLISH);

        Double total = 0.0;
        for(Map.Entry<String, Double> entry : tmapa.entrySet()){
            Date tmpD = new Date();
            try{
                tmpD = sDFString.parse(entry.getKey());
            }catch (ParseException e){
                e.printStackTrace();
            }

            String printDate = simpleDateFormat.format(tmpD);

            ConsoleHelper.writeMessage(String.format(Locale.ENGLISH, "%s - %.2f",printDate, entry.getValue()/100));
            total+=entry.getValue();
        }
        ConsoleHelper.writeMessage(String.format(Locale.ENGLISH, "Total - %.2f",total/100));
    }

    /**
     * загрузка (рабочее время) повара, сгруппировать по дням;
     * Реализуем логику метода printCookWorkloading в классе DirectorTablet.
     * Используя метод из предыдущего пункта вывести в консоль в убывающем порядке даты, имена поваров и время работы
     * в минутах (округлить в большую сторону).
     * Для каждой даты из хранилища событий, для которой есть запись о работе повара, должна выводится
     * продолжительность работы в минутах для этой даты.
     * Если повар не работал в какой-то из дней, то с пустыми данными его НЕ выводить (см. 13-May-2013)
     * Поваров сортировать по имени
     */
    void printCookWorkloading(){
        StatisticManager statisticManager = StatisticManager.getInstance();
        Map<String, TreeMap<String, Integer>> statMap = statisticManager.statCookWork();
        TreeMap<String, TreeMap<String, Integer>> tmapa = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        });
        tmapa.putAll(statMap);
        // что бы вывести как требуют обратим наши строки в дату и потом будем форматировать дату как нам нужно
        SimpleDateFormat sDFString = new SimpleDateFormat("yyyy/MM/dd",Locale.ENGLISH);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy",Locale.ENGLISH);

        for(Map.Entry<String, TreeMap<String, Integer>> entry:tmapa.entrySet()){
            Date tmpD = new Date();
            try{
                tmpD = sDFString.parse(entry.getKey());
            }catch (ParseException e){
                e.printStackTrace();
            }

            String printDate = simpleDateFormat.format(tmpD);

            ConsoleHelper.writeMessage(printDate);
            TreeMap<String,Integer> tmap = new TreeMap<>();
            tmap.putAll(entry.getValue());
            for(Map.Entry<String, Integer> entry1:tmap.entrySet()){
                ConsoleHelper.writeMessage(entry1.getKey()+" - "+entry1.getValue()+" min");
            }
            ConsoleHelper.writeMessage("");
        }





    }

    /**
     * список активных роликов и оставшееся количество показов по каждому;
     */
    void printActiveVideoSet(){
       StatisticAdvertisementManager statisticAdvertisementManager = StatisticAdvertisementManager.getInstance();

       //ConsoleHelper.writeMessage("---список активных---");
       for(Map.Entry<String,Integer> entry:statisticAdvertisementManager.InspectorAdvertisementStorage(true).entrySet()){
            ConsoleHelper.writeMessage(entry.getKey()+" - "+entry.getValue());
        }
        //ConsoleHelper.writeMessage("---список активных---");
    }

    /**
     * список неактивных роликов (с оставшемся количеством показов равным нулю).
     */
    void printArchivedVideoSet(){
        StatisticAdvertisementManager statisticAdvertisementManager = StatisticAdvertisementManager.getInstance();
        TreeMap<String,Integer> map = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return String.CASE_INSENSITIVE_ORDER.compare(o1,o2);
            }
        });
        map.putAll(statisticAdvertisementManager.InspectorAdvertisementStorage(false));
        //ConsoleHelper.writeMessage("---список неактивных---");
        for(Map.Entry<String,Integer> entry:map.entrySet()){
            ConsoleHelper.writeMessage(entry.getKey());
        }
        //ConsoleHelper.writeMessage("---список неактивных---");
    }
}
