package com.javarush.task.task27.task2712;

import com.javarush.task.task27.task2712.kitchen.Cook;
import com.javarush.task.task27.task2712.kitchen.Order;
import com.javarush.task.task27.task2712.kitchen.Waiter;
import com.javarush.task.task27.task2712.statistic.StatisticManager;
//import com.javarush.task.task27.task2712.statistic.event.EventDataRow;
//import com.sun.org.apache.xpath.internal.operations.Or;

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

public class Restaurant {
    private static final int ORDER_CREATING_INTERVAL = 100;
    private static final LinkedBlockingQueue<Order> orderQueue = new LinkedBlockingQueue<>();

    public static void main(String[] args) throws Exception{

        Cook cook1 = new Cook("John Connor");
        cook1.setQueue(orderQueue);
        Cook cook2 = new Cook("Oskolok");
        cook2.setQueue(orderQueue);
        Thread threadCook1 = new Thread(cook1);
        Thread threadCook2 = new Thread(cook2);
        threadCook1.start();
        threadCook2.start();

        StatisticManager statisticManager = StatisticManager.getInstance();

        ArrayList<Tablet> listTablet = new ArrayList<>();

        for(int i=0;i<5;i++){

            listTablet.add(new Tablet(i));
            Tablet tablet = listTablet.get(i);
            tablet.setQueue(orderQueue);


        }
        Thread thread = new Thread(new RandomOrderGeneratorTask(listTablet,ORDER_CREATING_INTERVAL));
        thread.start();


        Waiter waiter = new Waiter();
        cook1.addObserver(waiter);
        cook2.addObserver(waiter);


        DirectorTablet dt = new DirectorTablet();
        dt.printAdvertisementProfit();
        ConsoleHelper.writeMessage("");
        dt.printCookWorkloading();
        ConsoleHelper.writeMessage("");
        dt.printActiveVideoSet();
        ConsoleHelper.writeMessage("");
        dt.printArchivedVideoSet();

        //StatisticManager statisticManager = StatisticManager.getInstance();
        //statisticManager.statisticAdvertisement();


    }
}
