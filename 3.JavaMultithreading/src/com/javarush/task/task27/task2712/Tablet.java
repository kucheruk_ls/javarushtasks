package com.javarush.task.task27.task2712;

import com.javarush.task.task27.task2712.ad.AdvertisementManager;
import com.javarush.task.task27.task2712.ad.NoVideoAvailableException;
import com.javarush.task.task27.task2712.kitchen.Order;
import com.javarush.task.task27.task2712.kitchen.TestOrder;

import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * планшет
 */
public class Tablet{
     /**
     * номер планшета
     */
    final int number;
    static java.util.logging.Logger logger = Logger.getLogger(Tablet.class.getName());
    private LinkedBlockingQueue<Order> queue;

    public void setQueue(LinkedBlockingQueue<Order> orderQueue) {
        this.queue = orderQueue;
    }


    public Tablet(int number){
        this.number = number;
    }

    /**
     * который будет создавать заказ из тех блюд, которые выберет пользователь.
     */
    public void createOrder() {
        try {
            Order order = new Order(this);
            processOrder(order);

        }catch (IOException e) {
            logger.log(Level.SEVERE,"Console is unavailable.");

        }catch (Exception e){

        }

    }

    public void createTestOrder() {
        try {
            TestOrder order = new TestOrder(this);
            processOrder(order);
        }catch (IOException e) {
            logger.log(Level.SEVERE,"Console is unavailable.");
        }catch (Exception e){
            
        }

    }

    private void processOrder(Order order) {
        System.out.println(order.toString());
            queue.add(order);

        try {
            AdvertisementManager advertisementManager = new AdvertisementManager(order.getTotalCookingTime()*60);
            advertisementManager.processVideos();
        } catch(NoVideoAvailableException e) {
            logger.log(Level.INFO,"No video is available for the order " + order);
        }
    }
    @Override
    public String toString() {
        return "Tablet{" +
                "number=" + number +
                '}';
    }


}
