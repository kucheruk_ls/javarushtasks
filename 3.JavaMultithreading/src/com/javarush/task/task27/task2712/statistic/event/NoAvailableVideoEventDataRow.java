package com.javarush.task.task27.task2712.statistic.event;

import java.util.Date;

/**
 * NoAvailableVideoEventDataRow(int totalDuration)
 * totalDuration - время приготовления заказа в секундах
 */
public class NoAvailableVideoEventDataRow implements EventDataRow {
    private int totalDuration;
    private Date currentDate;

    public NoAvailableVideoEventDataRow(int totalDuration) {
        this.totalDuration = totalDuration;
        this.currentDate = new Date();
    }

    public EventType getType(){
        return EventType.NO_AVAILABLE_VIDEO;
    }

    @Override
    /**
     * , реализация которого вернет дату создания записи
     * @return
     */
    public Date getDate() {
        return currentDate;
    }

    @Override
    /**
     * , реализация которого вернет время - продолжительность
     * @return
     */
    public int getTime() {
        return totalDuration;
    }
}
