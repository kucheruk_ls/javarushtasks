package com.javarush.task.task27.task2712.statistic.event;

import com.javarush.task.task27.task2712.kitchen.Dish;

import java.util.Date;
import java.util.List;

/**
 *  CookedOrderEventDataRow(String tabletName, String cookName, int cookingTimeSeconds, List<Dish> cookingDishs)
 * где - tabletName - имя планшета
 * cookName - имя повара
 * cookingTimeSeconds - время приготовления заказа в секундах
 * cookingDishs - список блюд для приготовления
 */
public class CookedOrderEventDataRow implements EventDataRow {
    private String tabletName;
    private String cookName;
    private int cookingTimeSeconds;
    private List<Dish> cookingDishs;
    private Date currentDate;

    public CookedOrderEventDataRow(String tabletName, String cookName, int cookingTimeSeconds, List<Dish> cookingDishs) {
        this.tabletName = tabletName;
        this.cookName = cookName;
        this.cookingTimeSeconds = cookingTimeSeconds;
        this.cookingDishs = cookingDishs;
        this.currentDate = new Date();
    }

    public EventType getType(){
        return EventType.COOKED_ORDER;
    }


    public String getCookName() {
        return cookName;
    }

    @Override
    /**
     * , реализация которого вернет дату создания записи
     * @return
     */
    public Date getDate() {
        return currentDate;
    }

    @Override
    /**
     * , реализация которого вернет время - продолжительность
     * @return
     */
    public int getTime() {
        return cookingTimeSeconds;
    }
}
