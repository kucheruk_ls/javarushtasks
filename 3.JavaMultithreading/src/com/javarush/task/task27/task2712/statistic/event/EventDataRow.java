package com.javarush.task.task27.task2712.statistic.event;

import java.util.Date;

public interface EventDataRow {

    /**
     *
     * @return - тип события
     */
    EventType getType();

    /**
     * , реализация которого вернет дату создания записи
     * @return
     */
    Date getDate();

    /**
     * , реализация которого вернет время - продолжительность
     * @return
     */
    int getTime();
}
