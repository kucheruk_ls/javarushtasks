package com.javarush.task.task27.task2712.statistic;




import com.javarush.task.task27.task2712.kitchen.Cook;
import com.javarush.task.task27.task2712.statistic.event.CookedOrderEventDataRow;
import com.javarush.task.task27.task2712.statistic.event.EventDataRow;
import com.javarush.task.task27.task2712.statistic.event.EventType;
import com.javarush.task.task27.task2712.statistic.event.VideoSelectedEventDataRow;

import java.text.SimpleDateFormat;
import java.util.*;

public class StatisticManager {
    private static StatisticManager instance;
    private StatisticStorage statisticStorage = new StatisticStorage();
    //private Set<Cook> cooks = new HashSet<>();

    //захардкодим тестовые данные в хранилище статистики


    private StatisticManager(){

    }

    public static StatisticManager getInstance(){

        if(instance==null) instance = new StatisticManager();
        return instance;
    }

    //public Set<Cook> getCooks() {
      //  return cooks;
    //}

    /**
     * Он должен регистрировать события в хранилище.
     * @param data
     */
    public void register(EventDataRow data){
        statisticStorage.put(data);
    }

    //public void register(Cook cook){
       // cooks.add(cook);
    //}

    /**
     * В StatisticManager создай метод (придумать самостоятельно), который из хранилища достанет все данные,
     * относящиеся к отображению рекламы, и посчитает общую прибыль за каждый день.
     * Дополнительно добавь вспомогательный метод get в класс хранилища, чтобы получить доступ к данным.
     */
    public Map<String, Double> statisticAdvertisement(){
        ArrayList<EventDataRow> list = new ArrayList<>();

         //выгребаем данные в лист
        list.addAll(statisticStorage.getStorage().get(EventType.SELECTED_VIDEOS));
        //создадим карту для сохранения результатов
        Map<String, Double> statResultMap = new HashMap<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        //теперь будем сортировать данные по дате и складывать полученные суммы в карту
        Double l = 0.00;
        for(EventDataRow eventDataRow:list){
            VideoSelectedEventDataRow videoSelectedEventDataRow = (VideoSelectedEventDataRow) eventDataRow;
            String s = simpleDateFormat.format(videoSelectedEventDataRow.getDate());
            if(statResultMap.containsKey(s)){
                l = statResultMap.get(s);
                statResultMap.put(s,videoSelectedEventDataRow.getAmount()+l);
            }else{
                statResultMap.put(s,videoSelectedEventDataRow.getAmount()+0.00);
            }
        }
        /*нахардкодим кучу данных
        statResultMap.put("2018/10/02",256L);
        statResultMap.put("2018/10/14",256L);
        statResultMap.put("2018/10/13",256L);
        statResultMap.put("2018/10/11",256L);
        statResultMap.put("2018/10/15",256L);
        statResultMap.put("2018/10/08",256L);
        конец блока хардкода*/
        return statResultMap;
    }

    /**
     * В StatisticManager создай метод (придумать самостоятельно), который из хранилища достанет все данные,
     * относящиеся к работе повара, и посчитает общую продолжительность работы для каждого повара отдельно.
     */

    public Map<String, TreeMap<String, Integer>> statCookWork(){
        ArrayList<EventDataRow> dounloadWorkCookData = new ArrayList<>();

        dounloadWorkCookData.addAll(statisticStorage.getStorage().get(EventType.COOKED_ORDER));

        TreeMap<String, TreeMap<String, Integer>> statisticCookWorkMap = new TreeMap<>();

        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy/MM/dd");

        for(EventDataRow EDRow : dounloadWorkCookData) {
            CookedOrderEventDataRow cookedOrderEventDataRow = (CookedOrderEventDataRow) EDRow;

            String data = simpleDateFormat2.format(cookedOrderEventDataRow.getDate());
            if(statisticCookWorkMap.containsKey(data)){
                TreeMap<String, Integer> tmap = new TreeMap<>(statisticCookWorkMap.get(data));
                if(tmap.containsKey(cookedOrderEventDataRow.getCookName())){
                    Integer t1 = tmap.get(cookedOrderEventDataRow.getCookName());
                    t1 += cookedOrderEventDataRow.getTime();
                    tmap.put(cookedOrderEventDataRow.getCookName(),t1);
                    statisticCookWorkMap.put(data,tmap);
                }else{
                    tmap.put(cookedOrderEventDataRow.getCookName(),cookedOrderEventDataRow.getTime());
                    statisticCookWorkMap.put(data,tmap);
                }
            }else{
                TreeMap<String, Integer> tmap = new TreeMap<>();
                tmap.put(cookedOrderEventDataRow.getCookName(),cookedOrderEventDataRow.getTime());
                statisticCookWorkMap.put(data,tmap);
            }


        }

        //тестовый хардкод данных
/*
        TreeMap<String, Integer> testMap = new TreeMap<>();
        testMap.put("Vasya1", 45);
        testMap.put("Vasya2", 45);
        testMap.put("Vasya2", 45);
        testMap.put("Vasya3", 45);
        statisticCookWorkMap.put("2018/10/15",testMap);
        statisticCookWorkMap.put("2018/10/02",testMap);
        statisticCookWorkMap.put("2018/10/06",testMap);
        statisticCookWorkMap.put("2018/10/11",testMap);
*/
        return statisticCookWorkMap;
    }


    private static class StatisticStorage{
        Map<EventType, List<EventDataRow>> storage = new HashMap<>();


        StatisticStorage(){
            for(EventType et:EventType.values()){
                storage.put(et,new ArrayList<EventDataRow>());
            }

        }

        public Map<EventType, List<EventDataRow>> getStorage() {
            return storage;
        }

        /**
         * Чтобы методом put(EventDataRow data) добавить объект data в данные карты, нужен тип события - EventType.
         * @param data
         */
        private void put(EventDataRow data) {
            List<EventDataRow> tmp = new ArrayList<>();
                tmp = storage.get(data.getType());

                    tmp.add(data);
                    storage.put(data.getType(),tmp);

        }


    }
}
