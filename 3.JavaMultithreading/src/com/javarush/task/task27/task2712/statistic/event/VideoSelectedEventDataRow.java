package com.javarush.task.task27.task2712.statistic.event;

import com.javarush.task.task27.task2712.ad.Advertisement;

import java.util.Date;
import java.util.List;

/**
 * VideoSelectedEventDataRow(List<Advertisement> optimalVideoSet, long amount, int totalDuration)
 * optimalVideoSet - список видео-роликов, отобранных для показа
 * amount - сумма денег в копейках
 * totalDuration - общая продолжительность показа отобранных рекламных роликов
 */
public class VideoSelectedEventDataRow implements EventDataRow {
    private List<Advertisement> optimalVideoSet;
    private long amount;
    private int totalDuration;
    private Date currentDate;

    public VideoSelectedEventDataRow(List<Advertisement> optimalVideoSet, long amount, int totalDuration) {
        this.optimalVideoSet = optimalVideoSet;
        this.amount = amount;
        this.totalDuration = totalDuration;
        this.currentDate = new Date();
    }

    public long getAmount() {
        return amount;
    }

    public EventType getType(){
       return EventType.SELECTED_VIDEOS;
    }

    @Override
    /**
     * , реализация которого вернет дату создания записи
     * @return
     */
    public Date getDate() {
        return currentDate;
    }

    @Override
    /**
     * , реализация которого вернет время - продолжительность
     * @return
     */
    public int getTime() {
        return totalDuration;
    }
}
