package com.javarush.task.task27.task2712;

import com.javarush.task.task27.task2712.kitchen.Dish;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ConsoleHelper {
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    /**
     * - для вывода message в консоль
     * @param message
     */
    public static void writeMessage(String message){
        System.out.println(message);
    }

    /**
     *  - для чтения строки с консоли
     * @return
     */
    public static String readString(){

        String s = "";
        try{
            s = reader.readLine();
            //reader.close();
        }catch (IOException e){

        }

        return s;
    }

    /**
     * - просит пользователя выбрать блюдо и добавляет его в список.
     * @return
     */
    public static List<Dish> getAllDishesForOrder() throws Exception{
        ArrayList<Dish> listDish = new ArrayList<>();
        ConsoleHelper.writeMessage(Dish.allDishesToString());
        writeMessage("Введите название блюда и нажмите Ввод. Что бы завершить выбор введите exit:");
        while (true){
            String s = readString();
            if(s.equals("exit"))break;

            try{
                listDish.add(Dish.valueOf(s));

            }catch (NullPointerException e){
                writeMessage("Необходимо ввести наименование блюда! Повторите ввод:");
            }catch (IllegalArgumentException e){
                writeMessage("Данное блюдо отсутствует в меню. Повторите ввод:");
            }

        }
        return listDish;
    }
}
