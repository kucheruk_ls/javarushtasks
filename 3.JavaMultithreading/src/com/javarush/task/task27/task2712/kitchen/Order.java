package com.javarush.task.task27.task2712.kitchen;


import com.javarush.task.task27.task2712.ConsoleHelper;
import com.javarush.task.task27.task2712.Tablet;

import java.util.List;

/**
 * В классе Order (заказ) должна быть информация, относящаяся к списку выбранных пользователем блюд.
 */
public class Order {
    private final Tablet tablet;
    protected List<Dish> dishes;

    public Order(Tablet tablet) throws Exception{
        this.tablet = tablet;
        initDishes();
    }
//добавил поле
    public Tablet getTablet() {
        return tablet;
    }

    protected void initDishes() throws Exception{
        this.dishes = ConsoleHelper.getAllDishesForOrder();
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    @Override
    public String toString() {
        if(dishes.size()==0)return "";
        StringBuilder sb = new StringBuilder("Your order: ");
        sb.append(dishes.toString());
        sb.append(" of ");
        sb.append(tablet.toString());
        sb.append(", cooking time ");
        sb.append(getTotalCookingTime());
        sb.append("min");

        return sb.toString();
    }

    /**
     * Метод считает суммарное время приготовления всех блюд в заказе в минутах
     * @return
     */
    public int getTotalCookingTime(){
        int timeCookOrder = 0;
        for(Dish d:dishes){
            timeCookOrder+=d.getDuration();
        }
        return timeCookOrder;
    }

    /**
     *
     * @return false - если в заказе нет блюд. true - если в заказе есть блюда
     */
    public boolean isEmpty(){
        if(dishes.size()>0){
            return false;
        }else{
            return true;
        }
    }
}
