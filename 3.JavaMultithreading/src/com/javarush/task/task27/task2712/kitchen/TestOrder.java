package com.javarush.task.task27.task2712.kitchen;


import com.javarush.task.task27.task2712.Tablet;

import java.util.ArrayList;

public class TestOrder extends Order {

    public TestOrder(Tablet tablet) throws Exception {
        super(tablet);
    }

    protected void initDishes(){
        ArrayList<Dish> list = new ArrayList<>();
        int rndDishQuantity = ((int) (Math.random()*10)+1);
        for(int i=0;i<rndDishQuantity;i++){

            int rndDishNum = (int) (Math.random()*Dish.values().length);

            list.add(Dish.values()[rndDishNum]);

        }
        this.dishes = list;

    }
}
