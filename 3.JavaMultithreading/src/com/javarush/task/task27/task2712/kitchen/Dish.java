package com.javarush.task.task27.task2712.kitchen;

/**
 * список блюд
 */
public enum  Dish {
    Fish(25),
    Steak(30),
    Soup(15),
    Juice(5),
    Water(3);

    private int duration;

    Dish(int i){
        this.duration = i;
    }

    public int getDuration() {
        return duration;
    }

    /**
     *
     * @return строку перечисляющюю все блюда из списка
     */
    public static String allDishesToString(){

        StringBuilder sb = new StringBuilder();
        for(Dish s:Dish.values()){
            if(sb.length()>0)sb.append(",");
            sb.append(s.toString());

        }
        return sb.toString();
    }
}
