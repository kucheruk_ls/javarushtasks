package com.javarush.task.task27.task2712.kitchen;

import com.javarush.task.task27.task2712.ConsoleHelper;
//import com.javarush.task.task27.task2712.Tablet;
import com.javarush.task.task27.task2712.statistic.StatisticManager;
import com.javarush.task.task27.task2712.statistic.event.CookedOrderEventDataRow;
//import com.javarush.task.task27.task2712.statistic.event.EventDataRow;

import java.util.Observable;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * класс Cook(Повар) в пакете kitchen, он будет готовить.
 * Пусть в конструкторе приходит его имя, которое выводится методом toString.
 */
public class Cook extends Observable implements Runnable { //Observer waiter - официант
    String name;
    boolean busy; //занят повар или нет
    private LinkedBlockingQueue<Order> queue;

    public boolean isBusy() {
        return busy;
    }

    public Cook(String name){
        this.name = name;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()){

           if(queue.size()>0){
               this.startCookingOrder(queue.poll());
           }
            try{
                Thread.sleep(10);
            }catch (InterruptedException e){

            }


        }
        try{
            Thread.sleep(10);
        }catch (InterruptedException e){

        }
    }

    public void setQueue(LinkedBlockingQueue<Order> orderQueue) {
        this.queue = orderQueue;
    }


    @Override
    public String toString() {
        return name;
    }

    public void startCookingOrder(Order order){
        this.busy=true;
        ConsoleHelper.writeMessage("Start cooking - " + order.toString());

          StatisticManager sm = StatisticManager.getInstance();

              CookedOrderEventDataRow data = new CookedOrderEventDataRow(order.getTablet().toString(), this.name, order.getTotalCookingTime(), order.getDishes());
              sm.register(data);
        int sl = order.getTotalCookingTime();
        try{
            Thread.sleep(sl*10);
        }catch (InterruptedException e){

        }
              setChanged();
              notifyObservers(order);


              this.busy=false;
    }
}
