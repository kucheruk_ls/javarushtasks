package com.javarush.task.task27.task2712.ad;

import java.util.ArrayList;
import java.util.List;

public class AdvertisementStorage {
    private static AdvertisementStorage instance;
    private final List<Advertisement> videos;

    private AdvertisementStorage(){
        videos = new ArrayList<>();
        Object someContent = new Object();
        add(new Advertisement(someContent, "about videos 1", 1523, 3, 3 * 60));
        add(new Advertisement(someContent, "dab 2", 5, 2, 1 * 60));
        add(new Advertisement(someContent, "babusya 3", 99, 2, 3 * 60));
        add(new Advertisement(someContent, "cisco 4", 99, 10, 2 * 60));
        add(new Advertisement(someContent, "якудза 5", 2506, 3, 3 * 60));
        add(new Advertisement(someContent, "арбуз 6", 2506, 3, 3 * 60));
        add(new Advertisement(someContent, "zedix 7", 400, 1, 3 * 60));
        add(new Advertisement(someContent, "Avio 8", 500, 1, 2 * 60));
        add(new Advertisement(someContent, "Bubsi 10", 400, 2, 3 * 60));
        add(new Advertisement(someContent, "Vasya 11", 350, 100, 3 * 60)); // 3 min
        add(new Advertisement(someContent, "Ари 12", 1500, 0, 2 * 60)); //15 min
        add(new Advertisement(someContent, "фасу 13", 4600, 0, 10 * 60));   //10 min
    }

    public static AdvertisementStorage getInstance(){
        if(instance==null) instance = new AdvertisementStorage();

        return instance;
    }

    /**
     * возвращает список видео
     * @return
     */
    public List<Advertisement> list(){
        return videos;
    }

    /**
     * добавляет видео в список.
     * @param advertisement
     */
    public void add(Advertisement advertisement){
        videos.add(advertisement);
    }
}
