package com.javarush.task.task27.task2712.ad;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 *будет предоставлять информацию из AdvertisementStorage в нужном нам виде.
 */
public class StatisticAdvertisementManager {
        private static StatisticAdvertisementManager instance;
        private AdvertisementStorage advertisementStorage = AdvertisementStorage.getInstance();

        private StatisticAdvertisementManager(){

        }

        public static StatisticAdvertisementManager getInstance(){
            if(instance==null){
                instance=new StatisticAdvertisementManager();
            }
            return instance;
        }

    /**
     * создай два (или один) метода (придумать самостоятельно), которые из хранилища
     * AdvertisementStorage достанут все необходимые данные - соответственно список
     * активных и неактивных рекламных роликов.
     * Активным роликом считается тот, у которого есть минимум один доступный показ.
     * Неактивным роликом считается тот, у которого количество показов равно 0.
     */
    public TreeMap<String,Integer> InspectorAdvertisementStorage(Boolean isActive){
        TreeMap<String, Integer> list = new TreeMap<String, Integer>();
        if(isActive==true) {
            for(Advertisement video:advertisementStorage.list()){
                if(video.getHits()>0){
                    list.put(video.getName(),video.getHits());
                }
            }
        }else{
            for(Advertisement video:advertisementStorage.list()){
                if(video.getHits()<=0){
                    list.put(video.getName(),video.getHits());
                }
            }
        }
        return list;
    }



}
