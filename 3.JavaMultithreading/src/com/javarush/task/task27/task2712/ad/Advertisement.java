package com.javarush.task.task27.task2712.ad;

/**
 * рекламное объявление
 */
/**
 * Object content - видео
 * String name - имя/название
 * long initialAmount - начальная сумма, стоимость рекламы в копейках. Используем long, чтобы избежать проблем с округлением
 * int hits - количество оплаченных показов
 * int duration - продолжительность в секундах
 * amountPerOneDisplaying - стоимость одного показа одного объявления
 */
public class Advertisement {
    private Object content;
    private String name;
    private long initialAmount;
    private int hits;
    private int duration;
    long amountPerOneDisplaying;


    public Advertisement(Object content, String name, long initialAmount, int hits, int duration) {
        this.content = content;
        this.name = name;
        this.initialAmount = initialAmount;
        this.hits = hits;
        this.duration = duration;
        try{
            this.amountPerOneDisplaying = initialAmount/hits;
        }catch (ArithmeticException e){
            this.amountPerOneDisplaying = 0;
        }
        //this.priceMin = amountPerOneDisplaying/duration;
    }

    public String getName() {
        return name;
    }

    public int getDuration() {
        return duration;
    }

    public long getAmountPerOneDisplaying() {
        return amountPerOneDisplaying;
    }

    public void revalidate(){
        if(hits<=0){
            throw new UnsupportedOperationException();
        }
        hits--;
    }
    public int getHits() {
        return hits;
    }
}
