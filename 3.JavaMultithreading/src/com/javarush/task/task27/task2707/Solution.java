package com.javarush.task.task27.task2707;

import static java.lang.Thread.sleep;

/*
Определяем порядок захвата монитора
*/
public class Solution {
    public void someMethodWithSynchronizedBlocks(Object obj1, Object obj2) {
        synchronized (obj1) {
            synchronized (obj2) {
                System.out.println(obj1 + " " + obj2);
            }
        }
    }

    public static boolean isNormalLockOrder(final Solution solution, final Object o1, final Object o2) throws Exception {
        //do something here
        boolean normB = false;
       Thread tt1 = new Thread(){
           public void run(){
               solution.someMethodWithSynchronizedBlocks(o1, o2);
           }
       };

       synchronized (o2){
        synchronized (o1){
            tt1.start();
            Thread.sleep(500);

        }
        Thread.sleep(1000);

        Thread tt2 = new Thread(){

            public void run(){
                synchronized (o1){

                }
            }
        };
        tt2.start();
        Thread.sleep(1000);

        return tt2.getState().equals(Thread.State.BLOCKED);


       }




        //return dlm;
    }



    public static void main(String[] args) throws Exception {
        final Solution solution = new Solution();
        final Object o1 = new Object();
        final Object o2 = new Object();



        System.out.println(isNormalLockOrder(solution, o1, o2));
    }
}
