package com.javarush.task.task17.task1710;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD

CrUD - Create, Update, Delete

Программа запускается с одним из следующих наборов параметров:
-c name sex bd
-u id name sex bd
-d id
-i id

Значения параметров:
name - имя, String
sex - пол, "м" или "ж", одна буква
bd - дата рождения в следующем формате 15/04/1990
-c - добавляет человека с заданными параметрами в конец allPeople, выводит id (index) на экран
-u - обновляет данные человека с данным id
-d - производит логическое удаление человека с id, заменяет все его данные на null
-i - выводит на экран информацию о человеке с id: name sex (м/ж) bd (формат 15-Apr-1990)
id соответствует индексу в списке

Все люди должны храниться в allPeople
Используйте Locale.ENGLISH в качестве второго параметра для SimpleDateFormat
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) {
        //start here - начни тут


        //
        if(args[0].equals("-c")){ //-c - добавляет человека с заданными параметрами в конец allPeople, выводит id (index) на экран
            if(args[2].equals("м")){ //создаем мужчину
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH);
                String dp = dateFormat.format(new Date(args[3]));

                allPeople.add(Person.createMale(args[1], new Date(dp)));
                System.out.println(allPeople.size()-1);
            }else if(args[2].equals("ж")){//создаем женщину
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH);
                String dp = dateFormat.format(new Date(args[3]));
                allPeople.add(Person.createFemale(args[1], new Date(dp)));
                System.out.println(allPeople.size()-1);
            }
        }else if(args[0].equals("-u")){ //-u - обновляет данные человека с данным id

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH);
//                String dp = dateFormat.format(new Date(args[4]));
            Date dp = new Date();
            try {
                dp = dateFormat.parse(args[4]);
            }catch(ParseException e){
                e.printStackTrace();
            }
                Person pert = allPeople.get(Integer.parseInt(args[1]));
                pert.setName(args[2]);
                Sex se = Sex.FEMALE;
                if(args[3].equals("м"))se=Sex.MALE;
                if(args[3].equals("ж"))se=Sex.FEMALE;
                pert.setSex(se);
                pert.setBirthDay(dp);


                allPeople.set(Integer.parseInt(args[1]), pert);


        }else if(args[0].equals("-d")){//-d - производит логическое удаление человека с id, заменяет все его данные на null
            int id = Integer.parseInt(args[1]);
            Person perec = allPeople.get(id);
            perec.setName(null);
            perec.setSex(null);
            perec.setBirthDay(null);
            allPeople.set(id, perec);
        }else if(args[0].equals("-i")){ //-i - выводит на экран информацию о человеке с id: name sex (м/ж) bd (формат 15-Apr-1990) id соответствует индексу в списке
            int id = Integer.parseInt(args[1]);
            String name = allPeople.get(id).getName();
            String sex = "";
        if(allPeople.get(id).getSex()==Sex.MALE)sex="м";
        if(allPeople.get(id).getSex()==Sex.FEMALE)sex="ж";
        Date bd = new Date();
        bd = allPeople.get(id).getBirthDay();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
            String bdp = dateFormat.format(bd);
            System.out.println(name+" "+sex+" "+bdp);

        }
//тестовый вывод массива args
/*
        System.out.println("--------------------args-----------------------");
        for(String s:args){
            System.out.println(s);
        }


        //тестовая печать списка allPeople
        System.out.println("-------------------весь список тестовый вывод-------------------------------");
        for(int i=0;i<allPeople.size();i++){
            System.out.println(allPeople.get(i).getName()+" "+allPeople.get(i).getSex()+" "+allPeople.get(i).getBirthDay());
        }
*/
    }
}
