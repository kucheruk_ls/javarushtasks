package com.javarush.task.task17.task1721;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Транзакционность
*/

public class Solution {
    public static List<String> allLines = new ArrayList<String>();
    public static List<String> forRemoveLines = new ArrayList<String>();

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            BufferedReader freader1 = new BufferedReader(new FileReader(reader.readLine()));
            BufferedReader freader2 = new BufferedReader(new FileReader(reader.readLine()));
            String s = "";
            while ((s = freader1.readLine())!=null){
                allLines.add(s);
            }
            freader1.close();
            while ((s = freader2.readLine())!=null){
                forRemoveLines.add(s);
            }
            freader2.close();

        }catch(IOException e){
            System.out.println("Файл не найден!");
            e.printStackTrace();
        }
        try{
            new Solution().joinData();
        }catch(CorruptedDataException e){
            e.printStackTrace();
            System.out.println("хуякс");
        }



    }

    public void joinData () throws CorruptedDataException {
            boolean ok = true;

                for(String s2:forRemoveLines) {
                    if (!allLines.contains(s2)) {
                        ok = false;
                        break;
                    }
                }
                    if (ok) {
                        allLines.removeAll(forRemoveLines);
                        /*
                        System.out.println("--------печать allLines после удаления из него forRemoveLines---------");
                        for(String s:allLines){
                            System.out.println(s);
                        }
                        System.out.println("---------конец-----------------");
                        */
                    } else {
                        allLines.clear();
                        throw new CorruptedDataException();
                    }

    }
}
