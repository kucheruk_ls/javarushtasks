package com.javarush.task.task17.task1711;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD 2
CrUD Batch - multiple Creation, Updates, Deletion

Программа запускается с одним из следующих наборов параметров:
-c name1 sex1 bd1 name2 sex2 bd2 ...
-u id1 name1 sex1 bd1 id2 name2 sex2 bd2 ...
-d id1 id2 id3 id4 ...
-i id1 id2 id3 id4 ...

Значения параметров:
name - имя, String
sex - пол, "м" или "ж", одна буква
bd - дата рождения в следующем формате 15/04/1990
-с - добавляет всех людей с заданными параметрами в конец allPeople, выводит id (index) на экран в соответствующем порядке
-u - обновляет соответствующие данные людей с заданными id
-d - производит логическое удаление человека с id, заменяет все его данные на null
-i - выводит на экран информацию о всех людях с заданными id: name sex bd
id соответствует индексу в списке

Формат вывода даты рождения 15-Apr-1990
Все люди должны храниться в allPeople
Порядок вывода данных соответствует вводу данных
Обеспечить корректную работу с данными для множества нитей (чтоб не было затирания данных)
Используйте Locale.ENGLISH в качестве второго параметра для SimpleDateFormat

Пример вывода для параметра -і с двумя id:
Миронов м 15-Apr-1990
Миронова ж 25-Apr-1997
*/

public class Solution {
    public static volatile List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createFemale("Петров Петр", new Date()));  //сегодня родился    id=1

    }

    public static void main(String[] args) {
        //start here - начни тут
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        switch(args[0]){
            case "-c": { synchronized (allPeople){
                int i=1;
                int rm = args.length;
                while (i<rm){

                    try{
                        Date d = dateFormat.parse(args[i+2]);

                        allPeople.add(args[i+1].equals("м")?Person.createMale(args[i],d):Person.createFemale(args[i],d));
                    }catch(ParseException e){
                        e.printStackTrace();
                    }
                    i=i+3;
                    System.out.println(allPeople.size()-1);

                }
            }
            break;
            }
            case "-u":{
                synchronized (allPeople){
                    int i=1;
                    int rm = args.length;
                    while (i<rm){

                        allPeople.get(Integer.parseInt(args[i])).setName(args[i+1]);
                        allPeople.get(Integer.parseInt(args[i])).setSex(args[i+2].equals("м")?Sex.MALE:Sex.FEMALE);
                        try{
                            allPeople.get(Integer.parseInt(args[i])).setBirthDay(dateFormat.parse(args[i+3]));
                        }catch(ParseException e){
                            System.out.println("Пиздык! дата не запарсилась!");
                        }
                        i=i+4;

                    }
                }
                break;
            }
            case "-d":{
                synchronized (allPeople){
                    int i=1;
                    int rm = args.length;

                    while (i<rm){
                        int id = Integer.parseInt(args[i]);
                        allPeople.get(id).setName(null);
                        allPeople.get(id).setSex(null);
                        allPeople.get(id).setBirthDay(null);
                        i+=i;
                    }
                }
                break;
            }
            case "-i":{
                synchronized (allPeople){
                    int i=1;
                    int rm = args.length;
                    while (i<rm){
                        int id = Integer.parseInt(args[i]);
                        String name = allPeople.get(id).getName();
                        //String sex = allPeople.get(id).getSex()==Sex.MALE?"м":"ж";
                        String sex = "";
                        if(allPeople.get(id).getSex()==Sex.MALE)sex="м";
                        if(allPeople.get(id).getSex()==Sex.FEMALE)sex="ж";
                        Date date = allPeople.get(id).getBirthDay();
                        SimpleDateFormat newDF = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
                        String ndat = newDF.format(date);
                        System.out.println(name+" "+sex+" "+ndat);
                        i=i+1;
                    }
                }
                break;
            }
        }




        //тестовый вывод массива args
/*
        System.out.println("--------------------args-----------------------");
        for(String s:args){
            System.out.println(s);
        }


        //тестовая печать списка allPeople
        System.out.println("-------------------весь список тестовый вывод-------------------------------");
        for(int i=0;i<allPeople.size();i++){
            System.out.println(allPeople.get(i).getName()+" "+allPeople.get(i).getSex()+" "+allPeople.get(i).getBirthDay());
        }

*/


    }
}
