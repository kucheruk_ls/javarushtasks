package com.javarush.task.task20.task2003;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/* 
Знакомство с properties
*/
public class Solution {

    public static Map<String, String> properties = new HashMap<>();

    public void fillInPropertiesMap() throws Exception {
        //implement this method - реализуйте этот метод
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fn = reader.readLine();
        reader.close();

        InputStream inputStream = new FileInputStream(fn);
        load(inputStream);

        OutputStream outputStream = new FileOutputStream(fn);
    }


    public void save(OutputStream outputStream) throws Exception {
        //implement this method - реализуйте этот метод


        Properties prop = new Properties();
        for(Map.Entry<String, String> entry : properties.entrySet()){
            prop.setProperty(entry.getKey(),entry.getValue());
        }
        prop.save(outputStream, "hren vam");

    }

    public void load(InputStream inputStream) throws Exception {
        //implement this method - реализуйте этот метод
       Properties prop = new Properties();
       prop.load(inputStream);

       for(String name : prop.stringPropertyNames() ){
           properties.put(name, prop.getProperty(name));
       }


    }

    public static void main(String[] args) throws Exception {

        Solution solution = new Solution();
        solution.fillInPropertiesMap();

        for(Map.Entry<String, String> entry : properties.entrySet()){
            System.out.println(entry.getKey()+" "+entry.getValue());
        }


    }
}
