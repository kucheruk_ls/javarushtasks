package com.javarush.task.task20.task2022;

import java.io.*;

/* 
Переопределение сериализации в потоке
*/
public class Solution implements Serializable, AutoCloseable {
    private transient FileOutputStream stream;
    private String fileName;
    public Solution(String fileName) throws FileNotFoundException {
        this.stream = new FileOutputStream(fileName);
        this.fileName = fileName;
    }

    public void writeObject(String string) throws IOException {
        stream.write(string.getBytes());
        stream.write("\n".getBytes());
        stream.flush();
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        out.writeObject(stream);
        //out.close();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        //String fileName = (String) in.readObject();
       // FileOutputStream fos = ()in.readObject();
        this.stream = new FileOutputStream(fileName, true);
        //in.close();
    }

    @Override
    public void close() throws Exception {
        System.out.println("Closing everything!");
        stream.close();
    }

    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {

        Solution solution = new Solution("c:\\0\\6.txt");
        solution.writeObject("dannie");
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("c:\\0\\1.txt"));
        oos.writeObject(solution);
        oos.close();
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("c:\\0\\1.txt"));

        Solution solution1 = (Solution) ois.readObject();
        solution1.writeObject("dannie1");

        System.out.println(solution1.stream);

    }
}
