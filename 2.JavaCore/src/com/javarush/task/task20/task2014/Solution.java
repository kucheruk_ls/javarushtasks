package com.javarush.task.task20.task2014;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/* 
Serializable Solution
*/
public class Solution implements Serializable {
    public static void main(String[] args) throws IOException {
        System.out.println(new Solution(4));
        File my_file = File.createTempFile("c:\\0\\6.txt", null);
        FileOutputStream fos = new FileOutputStream(my_file);
        FileInputStream fis = new FileInputStream(my_file);

        Solution saveObject = new Solution(4);
        saveObject.saveObject(fos);
        fos.close();
        Solution loadObject = new Solution(0);
        loadObject.loadObject(fis);
        fis.close();
        if(saveObject==loadObject){
            System.out.println("OK!!!");
            System.out.println(saveObject.toString());
            System.out.println(loadObject.toString());
        }else{
            System.out.println("porozhnyak");
            System.out.println(saveObject.toString());
            System.out.println(loadObject.toString());
        }


    }

    private transient final String pattern = "dd MMMM yyyy, EEEE";
    private transient Date currentDate;
    private transient int temperature;
    String string;

    public Solution(int temperature) {
        this.currentDate = new Date();
        this.temperature = temperature;

        string = "Today is %s, and current temperature is %s C";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        this.string = String.format(string, format.format(currentDate), temperature);
    }

    public void saveObject(OutputStream os) {
        PrintWriter pw = new PrintWriter(os);
        pw.println(string);
        pw.flush();
        pw.close();
    }

    public void loadObject(InputStream is) throws IOException{
        BufferedReader bf = new BufferedReader(new InputStreamReader(is));
        string = bf.readLine();
        bf.close();
    }

    @Override
    public String toString() {
        return this.string;
    }
}
