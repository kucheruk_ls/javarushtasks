package com.javarush.task.task20.task2024;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

/* 
Знакомство с графами
*/
public class Solution implements Serializable {
    int node;
    List<Solution> edges = new LinkedList<>();

    public static void main(String[] args) throws Exception {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("c:\\0\\1.txt"));
        Solution sol = new Solution();
        Solution sol2 = new Solution();
        Solution sol3 = new Solution();
        sol.node = 5;
        sol.edges.add(sol2);
        sol.edges.add(sol3);

        System.out.println(sol.node);
        for(int i=0;i<sol.edges.size();i++){
            System.out.println(sol.edges.get(i));
        }

        oos.writeObject(sol);
        oos.close();

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("c:\\0\\1.txt"));
        Solution solution = (Solution) ois.readObject();
        ois.close();

        System.out.println(solution.node);
        for(int i=0;i<solution.edges.size();i++){
            System.out.println(solution.edges.get(i));
        }

    }
}
