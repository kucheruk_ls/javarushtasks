package com.javarush.task.task20.task2002;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
Читаем и пишем в файл: JavaRush
*/
public class Solution {
    public static void main(String[] args) {
        //you can find your_file_name.tmp in your TMP directory or fix outputStream/inputStream according to your real file location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try {
            File your_file_name = File.createTempFile("your_file_name", null);
            OutputStream outputStream = new FileOutputStream("c:\\0\\1.txt");
            InputStream inputStream = new FileInputStream("c:\\0\\1.txt");

            JavaRush javaRush = new JavaRush();
            //initialize users field for the javaRush object here - инициализируйте поле users для объекта javaRush тут
            SimpleDateFormat sdf = new SimpleDateFormat("MM dd yyyy HH:mm:ss S", Locale.ENGLISH);
            Date bd = sdf.parse("03 14 1987 15:30:24 12");
            User Ivan = new User();
            Ivan.setFirstName("Ivan");
            Ivan.setLastName("Bigs");
            Ivan.setBirthDate(bd);
            Ivan.setMale(true);
            Ivan.setCountry(User.Country.RUSSIA);
            Date bd2 = sdf.parse("03 14 1980 15:25:23 1");
            User Ivan2 = new User();
            Ivan2.setFirstName("Ivan2");
            Ivan2.setLastName("Bigs2");
            Ivan2.setBirthDate(bd2);
            Ivan2.setMale(true);
            Ivan2.setCountry(User.Country.RUSSIA);


            javaRush.users.add(Ivan);
            javaRush.users.add(Ivan2);
            javaRush.save(outputStream);
            outputStream.flush();

            JavaRush loadedObject = new JavaRush();
            loadedObject.load(inputStream);

            //check here that javaRush object equals to loadedObject object - проверьте тут, что javaRush и loadedObject равны
            if(loadedObject instanceof JavaRush){
                System.out.println("Объекты равны!");
            }




            outputStream.close();
            inputStream.close();
            for(int i = 0;i<loadedObject.users.size();i++){

                System.out.println(loadedObject.users.get(i).toString());
                System.out.println(loadedObject.users.get(i).getBirthDate());
            }

        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }
    }

    public static class JavaRush {
        public List<User> users = new ArrayList<>();

        public void save(OutputStream outputStream) throws Exception {
            //implement this method - реализуйте этот метод
            PrintWriter pw = new PrintWriter(outputStream);
            SimpleDateFormat sdf = new SimpleDateFormat("MM dd yyyy HH:mm:ss S",Locale.ENGLISH);
            pw.println(users.size());
            for(int i=0;i<users.size();i++){
                //pw.print(users.get(i));
                pw.println(users.get(i).getFirstName());
                pw.println(users.get(i).getLastName());
                pw.println(sdf.format(users.get(i).getBirthDate()));
                pw.println(users.get(i).isMale());
                pw.println(users.get(i).getCountry());
            }
            pw.flush();

        }

        public void load(InputStream inputStream) throws Exception {
            //implement this method - реализуйте этот метод
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            SimpleDateFormat sdf = new SimpleDateFormat("MM dd yyyy HH:mm:ss S", Locale.ENGLISH);
            int is = Integer.parseInt(reader.readLine());

            String chitalka = "";
            for(int i=0;i<is;i++){

                User user = new User();
                chitalka = reader.readLine();
                user.setFirstName(chitalka);
                chitalka = reader.readLine();
                user.setLastName(chitalka);
                chitalka = reader.readLine();
                user.setBirthDate(sdf.parse(chitalka));

                String s = reader.readLine();
                if(s.equals("true")){
                    user.setMale(true);
                }else{
                    user.setMale(false);
                }
                String country = reader.readLine();

                user.setCountry(User.Country.valueOf(country));

                users.add(user);

            }




        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            JavaRush javaRush = (JavaRush) o;

            return users != null ? users.equals(javaRush.users) : javaRush.users == null;

        }

        @Override
        public int hashCode() {
            return users != null ? users.hashCode() : 0;
        }
    }
}
