package com.javarush.task.task20.task2020;

import sun.awt.datatransfer.DataTransferer;

import java.io.*;
import java.util.logging.Logger;

/* 
Сериализация человека
*/
public class Solution {

    public static class Person implements Serializable {
        String firstName;
        String lastName;
        transient String fullName;
        transient final String greetingString;
        String country;
        Sex sex;
        transient PrintStream outputStream;
        transient Logger logger;

        Person(String firstName, String lastName, String country, Sex sex) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.fullName = String.format("%s, %s", lastName, firstName);
            this.greetingString = "Hello, ";
            this.country = country;
            this.sex = sex;
            this.outputStream = System.out;
            this.logger = Logger.getLogger(String.valueOf(Person.class));
        }
    }

    enum Sex {
        MALE,
        FEMALE
    }

    public static void main(String[] args) throws Exception {

        File my_file = File.createTempFile("c:\\0\\6.txt",null);
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(my_file));
        Person person = new Person("Ivanov", "Ivan", "Russia", Sex.MALE);

        oos.writeObject(person);
        oos.close();

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(my_file));
        Person pers = (Person) ois.readObject();

        if(person==(pers)){
            System.out.println("OK!");
        }else{
            System.out.println("NO!");
        }
        ois.close();



    }
}
