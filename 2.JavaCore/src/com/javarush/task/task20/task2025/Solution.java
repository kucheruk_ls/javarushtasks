package com.javarush.task.task20.task2025;

import java.util.ArrayList;
import java.util.Collections;

import static java.lang.Thread.sleep;
//import java.util.Collections;

/*
Алгоритмы-числа
*/
public class Solution {


    public static long[] getNumbers(long N) {
        /*
        Произведем реализацию метода следующим образом:
        1. Сформируем таблицу степеней.
        2. Будем просчитывать только суммовые разницы
         */
        //формируем таблицу степеней
        long[][] stp = new long[10][22];


        for (int i = 0; i < (long) stp.length; i++) {
            for (int j = 0; j < stp[i].length; j++) {
                long rvs = (long) Math.pow((double) i, j);
                stp[i][j] = rvs;
            }
        }


        //----формируем таблицу степеней
/*
        System.out.println("------тестовая печать таблицы приведения степеней--------");
            for(int i=0;i<stp.length;i++){
                System.out.println("");
                for(int i2=0;i2<stp[i].length;i2++){
                    System.out.print(stp[i][i2]+" ");

                }
            }
        System.out.println("");

        System.out.println("------тестовая печать таблицы приведения степеней--------");
*/
        ArrayList<Long> resultt = new ArrayList<>();
        //ArrayList<Long> kPodschet = new ArrayList<>();
        Solution sol = new Solution();
        for (long n = 1; n <= N; n++) {
            //System.out.println("       n: "+n);
            long pn = n;
            int rpn = sol.numSize(pn);

            int tmp = 0;
            long resultpn = 0;
            int oldtmp = 9;
            boolean netnull = false; //переменная равна false если от конца чиcла только нули
            while (pn>0){
                tmp = (int) (pn%10);

                if(tmp>oldtmp&&oldtmp!=0||(tmp>oldtmp&&oldtmp==0&&netnull==true))break;
                if(tmp>0)netnull=true;
                oldtmp=tmp;
                resultpn = resultpn + (stp[tmp][rpn]);
                pn = pn/10;
                if(resultpn>N)break;
            }
            long kontrol = 0;

            long prn = resultpn;
            //System.out.println("resultpn: "+resultpn);
            int rptn = sol.numSize(prn);

            int ptmp = 0;

            while (prn > 0) {
                ptmp = (int) (prn % 10);
                kontrol = kontrol + (stp[ptmp][rptn]);
                prn = prn / 10;
                if (kontrol > resultpn) break;
            }

            if(kontrol==resultpn){
                if(!resultt.contains(resultpn)){
                    resultt.add(resultpn);
                }

            }

        }

        Collections.sort(resultt);



        long[] vozvr = new long[resultt.size()];
        for(int i=0;i<resultt.size();i++){
            vozvr[i]=resultt.get(i);
        }


        return vozvr;
    }


    //посчитаем количество цифр в числе
    private int numSize(long x) {
        long p = 10;
        for (int i2=1; i2<19; i2++) {
            if (x < p)
                return i2;
            p = 10*p;
        }
        return 19;
    }

    public static void main(String[] args) {

        Long t0 = System.currentTimeMillis();
        long n = Integer.MAX_VALUE; //31 число
        //long n = 94204591914L; // это 40е число
        //long n = 35875699062250035L; // это 46 число
        //int n = 999999999; //9.62
        //int n = 2147483647;//22.338 - 24.887
        long[] lst = getNumbers(n);
        Long t1 = System.currentTimeMillis();
        System.out.println("time: " + (t1 - t0) / 1000d + " sec");
        System.out.println("memory: " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024 * 1024) + " mb");
        for (int i = 0; i < lst.length; i++) {
            System.out.println(lst[i]);
                   }
    }
}