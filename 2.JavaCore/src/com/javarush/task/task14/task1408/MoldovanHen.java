package com.javarush.task.task14.task1408;

public class MoldovanHen extends Hen {
    public int getCountOfEggsPerMonth(){
        return 28;
    }
    public String getDescription(){
        //Sssss. Я несу N яиц в месяц.">
        return super.getDescription()+" Моя страна - "+Country.MOLDOVA+". Я несу "+getCountOfEggsPerMonth()+" яиц в месяц.";


    }
}
