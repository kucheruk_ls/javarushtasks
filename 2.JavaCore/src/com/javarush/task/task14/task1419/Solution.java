package com.javarush.task.task14.task1419;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/* 
Нашествие исключений
*/

public class Solution {
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args) {
        initExceptions();

        for (Exception exception : exceptions) {
            System.out.println(exception);
        }
    }

    private static void initExceptions() {   //it's first exception NullPointerException, ArithmeticException, FileNotFoundException, URISyntaxException.
        try {
            float i = 1 / 0;

        } catch (ArithmeticException e) {
            exceptions.add(e);  //1
        }

        try {
            ArrayList<String> list = new ArrayList<>();
            list.get(4);

        }catch (IndexOutOfBoundsException e){
            exceptions.add(e);  //2
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("c:\\bab.txt")));

        }catch(FileNotFoundException e){
            exceptions.add(e);    //3
        }

        try {

            Integer i = null;
            i.toString();

        }catch (NullPointerException e){
                  exceptions.add(e);//4
        }

        try {

            Integer i = null;
            i.toString();


        }catch (Exception e){
            e = new ArrayStoreException();
            exceptions.add(e); //5
        }

        try {

            Integer i = null;
            i.toString();


        }catch (Exception e){
            e = new ClassCastException();
            exceptions.add(e); //6
        }

        try {

            Integer i = null;
            i.toString();


        }catch (Exception e){
            e = new IllegalArgumentException();
            exceptions.add(e); //7
        }

        try {

            Integer i = null;
            i.toString();


        }catch (Exception e){
            e = new NegativeArraySizeException();
            exceptions.add(e); //8
        }

        try {

            Integer i = null;
            i.toString();


        }catch (Exception e){
            e = new NumberFormatException();
            exceptions.add(e); //9
        }

        try {

            Integer i = null;
            i.toString();


        }catch (Exception e){
            e = new UnsupportedOperationException();
            exceptions.add(e); //10
        }

    }
}
