package com.javarush.task.task14.task1417;

public abstract class Money {
    //1. В абстрактном классе Money создай приватное поле amount типа double.
    private double amount;
/*
    public Money(){

    }
*/
    public Money(double amount){
        this.amount=amount;
    }

    public double getAmount(){
        return this.amount;
    }

    public abstract String getCurrencyName();

    //2. Создай публичный геттер для поля amount(public double getAmount()), чтобы к этому полю можно было получить доступ извне класса Money.



}

