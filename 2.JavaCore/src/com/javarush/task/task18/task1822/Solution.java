package com.javarush.task.task18.task1822;

/* 
Поиск данных внутри файла
*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader freader = new BufferedReader(new FileReader(reader.readLine()));

        String s = "";

        while ((s = freader.readLine())!=null) {
            if (s.startsWith(args[0] + " ")) System.out.println(s);
        }

        reader.close();
        freader.close();
    }
}
