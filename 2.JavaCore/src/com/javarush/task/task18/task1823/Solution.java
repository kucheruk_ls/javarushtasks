package com.javarush.task.task18.task1823;


import java.io.*;
import java.util.HashMap;

import java.util.Map;
import java.util.TreeMap;

/* 
Нити и байты
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = "";
        while (!(s = reader.readLine()).equals("exit")){
            new ReadThread(s).start();

        }

        for(Map.Entry<String, Integer> entry : resultMap.entrySet()){
            System.out.println(entry.getKey()+" "+entry.getValue());
        }


    }

    public static class ReadThread extends Thread {
        private String fileName;
        public ReadThread(String fileName) {
            //implement constructor body
            this.fileName = fileName;
        }
        // implement file reading here - реализуйте чтение из файла тут
        public void run(){
            TreeMap<Integer, Integer> tmap = new TreeMap();
            try{
                FileInputStream fis = new FileInputStream(fileName);
                while (fis.available()>0){
                    int sb = fis.read();
                    if(tmap.containsKey(sb)){
                        int value = tmap.get(sb)+1;
                        tmap.remove(sb);
                        tmap.put(sb, value);
                    }else{
                        tmap.put(sb,1);
                    }
                }

                fis.close();
            }catch(FileNotFoundException e){
                System.out.println(this.getName() +" сообщает: файл не найден");
            }catch (IOException e){
                e.printStackTrace();
            }
            //Iterator<Map.Entry<Integer, Integer>> iterator = tmap.entrySet().iterator();
            int maxKey = 0;
            int maxValue = 0;

            for(Map.Entry<Integer, Integer> entry : tmap.entrySet()){
                int key = entry.getKey();
                int value = entry.getValue();
                if(value>maxValue){
                    maxKey = key;
                    maxValue = value;
                }
            }
            //System.out.println(fileName+" "+maxKey);
            Solution.resultMap.put(fileName, maxKey);



        }



    }
}
