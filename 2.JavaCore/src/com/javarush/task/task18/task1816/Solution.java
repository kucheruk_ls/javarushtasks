package com.javarush.task.task18.task1816;

/* 
Английские буквы
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws Exception {
       BufferedReader reader = new BufferedReader(new FileReader(args[0]));
       String s = "";
       int count = 0;

        while ((s = reader.readLine())!=null){
            char[] mass = s.toCharArray();
            for(int i=0;i<mass.length;i++){
                char ch = mass[i];
                if((65<=ch)&&(ch<=90)||(97<=ch)&&(ch<=122)){
                    count++;
                }
            }
        }
        reader.close();
        System.out.println(count);

    }
}
