package com.javarush.task.task18.task1809;

/* 
Реверс файла
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.FileOutputStream;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fis1 = new FileInputStream(reader.readLine());
        FileOutputStream fis2 = new FileOutputStream(reader.readLine());
        byte[] bufffis1 = new byte[fis1.available()];

        fis1.read(bufffis1);

        for(int i=bufffis1.length-1;i>-1;i--){
            fis2.write(bufffis1[i]);
        }
        reader.close();
        fis1.close();
        fis2.close();
    }
}
