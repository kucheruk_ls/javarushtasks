package com.javarush.task.task18.task1814;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/* 
UnsupportedFileName
*/

public class TxtInputStream extends FileInputStream {


    public TxtInputStream(String fileName) throws Exception {

        super(fileName);
        if(!fileName.matches(".+\\.txt")){
            super.close();
            throw new UnsupportedFileNameException();
        }

    }

    public static void main(String[] args) throws Exception {
        TxtInputStream tis = new TxtInputStream("c:/0/1.mp3");
    }
}

