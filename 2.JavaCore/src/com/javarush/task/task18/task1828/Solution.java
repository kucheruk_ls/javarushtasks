package com.javarush.task.task18.task1828;

/* 
Прайсы 2
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fname = reader.readLine();
        reader.close();
        ArrayList<String> list = new ArrayList<>();
        int ind = -1;
        try{
            if(args[0].equals("-u")) {
                BufferedReader freader = new BufferedReader(new FileReader(fname));
                String s = "";
                while ((s=freader.readLine())!=null){
                    list.add(s);
                }
                freader.close();
                StringBuilder BiGN  =new StringBuilder();
                for(int i=0;i<list.size();i++) {
                    String si = list.get(i);
                    //очистим si от пробелов
                    String id = si.substring(0, 8);
                    for (int c = id.length(); c > 0; c--) {
                        if (id.lastIndexOf(" ") != -1) {
                            int p = id.lastIndexOf(" ");
                            id = id.substring(0, p);
                        } else {
                            break;
                        }
                    }

                    //проверим наш ли это id
                    StringBuilder sb = new StringBuilder();
                    if (id.equals(args[1])) { //если наш заменим строку
                        ind = i;
                        sb.append(args[1]);
                        for (int i1 = sb.length(); i1 < 8; i1++) {
                            sb.append(" ");
                        }

                    BiGN.append(sb);
                    sb.delete(0, sb.length());
                    sb.append(args[2]);
                    for (int i3 = sb.length(); i3 < 30; i3++) {
                        sb.append(" ");
                    }
                    BiGN.append(sb);
                    sb.delete(0, sb.length());
                    sb.append(args[3]);
                    for (int i4 = sb.length(); i4 < 8; i4++) {
                        sb.append(" ");
                    }
                    BiGN.append(sb);
                    sb.delete(0, sb.length());
                    sb.append(args[4]);
                    for (int i5 = sb.length(); i5 < 4; i5++) {
                        sb.append(" ");
                    }
                    BiGN.append(sb);
                }
                }
                list.set(ind,BiGN.toString());
                freader.close();

            }else if(args[0].equals("-d")){
                BufferedReader freader = new BufferedReader(new FileReader(fname));
                String s = "";
                while ((s=freader.readLine())!=null){
                    list.add(s);
                }
                freader.close();
                StringBuilder BiGN  =new StringBuilder();
                for(int i=0;i<list.size();i++) {
                    String si = list.get(i);
                    //очистим si от пробелов
                    String id = si.substring(0, 8);
                    for (int c = id.length(); c > 0; c--) {
                        if (id.lastIndexOf(" ") != -1) {
                            int p = id.lastIndexOf(" ");
                            id = id.substring(0, p);
                        } else {
                            break;
                        }
                    }

                    //проверим наш ли это id
                    StringBuilder sb = new StringBuilder();
                    if (id.equals(args[1])) {
                        list.remove(i);


                    }
                    freader.close();
                }
            }
            BufferedWriter bw = new BufferedWriter(new FileWriter(fname));
            for(int i6=0;i6<list.size();i6++){
                bw.write(list.get(i6)+"\n");
            }
            bw.close();



        }catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Произведен запуск без параметров");
        }
    }
}
