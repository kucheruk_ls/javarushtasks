package com.javarush.task.task18.task1808;

/* 
Разделение файла

Разделение файла
Считать с консоли три имени файла: файл1, файл2, файл3.
Разделить файл1 по следующему критерию:
Первую половину байт записать в файл2, вторую половину байт записать в файл3.
Если в файл1 количество байт нечетное, то файл2 должен содержать большую часть.
Закрыть потоки.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        String file2 = reader.readLine();
        String file3 = reader.readLine();
        FileInputStream streamin = new FileInputStream(file1);
        FileOutputStream streamout = new FileOutputStream(file2);
        FileOutputStream streamout2 = new FileOutputStream(file3);

        byte[] buffer=null;
        if(streamin.available()%2!=0){
            buffer=new byte[streamin.available()/2+1];
        }else{
            buffer=new byte[streamin.available()/2];
        }

        while (streamin.available()>0){
            int count = streamin.read(buffer);
            streamout.write(buffer,0,count);
            count = streamin.read(buffer);
            streamout2.write(buffer,0,count);

        }

        streamout.close();
        streamout2.close();
        streamin.close();






    }
}
