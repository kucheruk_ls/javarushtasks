package com.javarush.task.task18.task1804;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* 
Самые редкие байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fis = new FileInputStream(reader.readLine());
        HashMap<Integer, Integer> bmap = new HashMap<>();
        while (fis.available()>0) {

            int data = fis.read();
            if (bmap.containsKey(data)) {
                int count = bmap.get(data);
                count = count+1;
                bmap.put(data, count);
            } else {
                bmap.put(data, 1);
            }
        }
        reader.close();
        fis.close();
        int bv = Integer.MAX_VALUE;
        Iterator<Map.Entry<Integer, Integer>> entry = bmap.entrySet().iterator();
        while (entry.hasNext()){
            Map.Entry<Integer, Integer> pair = entry.next();
            //System.out.print(pair.getValue()+" ");

            if(bv>pair.getValue()){
                bv=pair.getValue();

            }

        }


        Iterator<Map.Entry<Integer, Integer>> entry2 = bmap.entrySet().iterator();
        while (entry2.hasNext()){
            Map.Entry<Integer, Integer> pair = entry2.next();
            if(pair.getValue()==bv){
                System.out.print(pair.getKey()+" ");
            }
        }



    }
}
