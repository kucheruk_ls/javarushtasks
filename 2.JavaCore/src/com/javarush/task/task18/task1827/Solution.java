package com.javarush.task.task18.task1827;

/* 
Прайсы
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException  {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fn = reader.readLine();
        BufferedReader freader = new BufferedReader(new FileReader(fn));
        reader.close();
        ArrayList<String> list = new ArrayList<>();
        String s = "";
        try {
            if (args[0].equals("-c")) {
                while ((s = freader.readLine()) != null) {
                    list.add(s);
                }
                freader.close();
                int cc = 0;
                for (int i = 0; i < list.size(); i++) {
                    String ss = list.get(i).substring(0, 8);
                    if (ss.indexOf(" ") != -1) {
                        int si = ss.indexOf(" ");
                        ss = ss.substring(0, si);
                    }

                    int tmp = Integer.parseInt(ss);
                    if (tmp > cc) cc = tmp;
                }
                StringBuilder sb = new StringBuilder();
                cc = cc + 1;
                //сформируем id;
                StringBuilder sid = new StringBuilder();
                sid.append(cc);

                for (int i = sid.length(); i < 8; i++) {
                    sid.append(" ");

                }
                sb.append(sid);
                //сформируем название товара;
                StringBuilder tname = new StringBuilder();
                tname.append(args[1]);
                for (int t = tname.length(); t < 30; t++) {
                    tname.append(" ");
                }
                sb.append(tname);
                //сформируем цену
                StringBuilder price = new StringBuilder();
                price.append(args[2]);
                for (int p = price.length(); p < 8; p++) {
                    price.append(" ");
                }
                sb.append(price);
                //сформируем количество
                StringBuilder kol = new StringBuilder();
                kol.append(args[3]);
                for (int k = kol.length(); k < 4; k++) {
                    kol.append(" ");
                }
                sb.append(kol);

                String res = sb.toString();
                list.add(res);

                BufferedWriter fos = new BufferedWriter(new FileWriter(fn));
                for (int i2 = 0; i2 < list.size(); i2++) {
                    fos.write((list.get(i2)) + "\n");
                }
                fos.close();


            }
        }catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Не заданы параметры");
        }



    }
}
