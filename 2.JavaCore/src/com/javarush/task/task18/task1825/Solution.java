package com.javarush.task.task18.task1825;

import java.io.*;
import java.util.*;

/* 
Собираем файл
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = "";
        StringBuilder sb = new StringBuilder();
        String num = "";
        String newf = ""; //имя нового файла

        TreeMap<Integer, String> mapa = new TreeMap<>();
        while (!(s = reader.readLine()).equals("end")) {
            for (int i = s.length() - 1; i > 0; i--) {
                if (isNumber(s.charAt(i))) {
                    sb.insert(0, s.charAt(i));

                } else {
                    newf = s.substring(0,s.lastIndexOf("."));
                    break;
                }
            }
            num = sb.toString();
            sb.delete(0, sb.length());


            mapa.put(Integer.parseInt(num), s);
        }



        File nf = new File(newf);
        FileOutputStream fos = new FileOutputStream(nf);
        String fn="";

        for (Map.Entry<Integer, String> entry : mapa.entrySet()) {
            fn = entry.getValue();
            FileInputStream fiss = new FileInputStream(fn);
            byte[] buffer = new byte[fiss.available()];
                fiss.read(buffer);

                fos.write(buffer);
                fiss.close();



        }

        fos.close();


    }






    public static boolean isNumber(char ch){
        boolean r = false;
        char[] num = new char[]{48,49,50,51,52,53,54,55,56,57};
        for(char f:num){
            if(f==ch){
                r=true;
                break;
            }

        }
        return r;
    }

}
