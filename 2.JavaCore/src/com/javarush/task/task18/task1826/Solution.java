package com.javarush.task.task18.task1826;

/* 
Шифровка
*/

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class Solution {
    public static void main(String[] args) throws Exception {

        FileInputStream fis = new FileInputStream(args[1]);
        FileOutputStream fos = new FileOutputStream(args[2]);
        String par = args[0];
        byte secretKey = 60;

        byte[] buffers = new byte[fis.available()];
        fis.read(buffers);

        fis.close();

        //проведем операцию преобразования.
        if(par.equals("-e")) {
            for (int i = 0; i < buffers.length - 1; i++) {
                buffers[i] = (byte) (buffers[i] + secretKey);
            }
            fos.write(buffers);
        }
        if(par.equals("-d")){
            for(int i = 0;i<buffers.length-1;i++){
                buffers[i] = (byte) (buffers[i] - secretKey);
            }
            fos.write(buffers);
        }
        fos.close();



    }

}
