package com.javarush.task.task18.task1813;

import java.io.*;
import java.nio.channels.FileChannel;

/* 
AmigoOutputStream
*/

public class AmigoOutputStream extends FileOutputStream {
    public static String fileName = "C:/tmp/result.txt";
    //public static String fileName = "C:/0/1.txt";
    private FileOutputStream fos;

    public AmigoOutputStream(FileOutputStream fos) throws FileNotFoundException{
        super(fileName);
        this.fos = fos;
    }

    public AmigoOutputStream(String name, boolean append) throws FileNotFoundException {
        super(name, append);

    }
    public void flush() throws IOException{
        fos.flush();
    }

    @Override
    public void write(int b) throws IOException {
        fos.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        fos.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        fos.write(b, off, len);
    }

    @Override
    public void close() throws IOException {
        fos.flush();
        String s = "JavaRush © All rights reserved.";
        byte[] b = s.getBytes();
        fos.write(b);
        fos.close();
    }

    @Override
    public FileChannel getChannel() {
        return fos.getChannel();
    }

    @Override
    protected void finalize() throws IOException {
        super.finalize();
    }

    public static void main(String[] args) throws FileNotFoundException {
        new AmigoOutputStream(new FileOutputStream(fileName));
    }
}
