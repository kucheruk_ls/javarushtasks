package com.javarush.task.task18.task1818;

/* 
Два в одном
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String f1 = reader.readLine();
        String f2 = reader.readLine();
        String f3 = reader.readLine();
        reader.close();
        FileOutputStream fos1 = new FileOutputStream(f1);
        FileInputStream fis1 = new FileInputStream(f2);
        FileInputStream fis2 = new FileInputStream(f3);

        byte[] bufb = new byte[fis1.available()];
        fis1.read(bufb);
        fos1.write(bufb);
        byte[] buf2 = new byte[fis2.available()];
        fis2.read(buf2);
        fos1.write(buf2);

        fis1.close();
        fis2.close();
        fos1.close();


    }
}
