package com.javarush.task.task18.task1817;

/* 
Пробелы
*/

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DecimalFormat;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader freader = new BufferedReader(new FileReader(args[0]));
        String s = "";
        double count = 0;
        double prblcount = 0;
        while((s = freader.readLine())!=null){
            char[] ch = s.toCharArray();
            for(int i=0;i<ch.length;i++){
                count++;
                if(ch[i]==32)prblcount++;

            }

        }
        freader.close();
        DecimalFormat format = new DecimalFormat("##0.00");
        double i = prblcount/count*100.0;
        String form = format.format(i);
        System.out.println(form);

    }
}
