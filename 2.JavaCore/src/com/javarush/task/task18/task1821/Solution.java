package com.javarush.task.task18.task1821;

/* 
Встречаемость символов
*/

import java.io.FileInputStream;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws Exception {
        FileInputStream fis = new FileInputStream(args[0]);
        TreeMap<Integer, Integer> map = new TreeMap<>();
        Iterator<Map.Entry<Integer, Integer>> iterator = map.entrySet().iterator();
        while (fis.available()>0){
            int re = fis.read();
            if(map.containsKey(re)){

                int count = map.get(re)+1;
                map.remove(re);
                map.put(re,count);

            }else{
                map.put(re,1);
            }
        }
        fis.close();
        for(Map.Entry<Integer, Integer> entry : map.entrySet()) {

            int key = entry.getKey();

            System.out.println((char) key+" "+entry.getValue());
        }




    }
}
