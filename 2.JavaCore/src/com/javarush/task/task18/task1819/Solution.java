package com.javarush.task.task18.task1819;

/* 
Объединение файлов
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String f1 = reader.readLine(); //в начало первого файла записать содержимое второго
        String f2 = reader.readLine();
        FileInputStream fis1 = new FileInputStream(f1);
        FileInputStream fis2 = new FileInputStream(f2);

        byte[] tmpf1 = new byte[fis1.available()];
        fis1.read(tmpf1);
        fis1.close();
        FileOutputStream fos = new FileOutputStream(f1);
        byte[] tmpf2 = new byte[fis2.available()];
        fis2.read(tmpf2);
        fis2.close();
        fos.write(tmpf2);
        fos.write(tmpf1);
        fos.close();

    }
}
