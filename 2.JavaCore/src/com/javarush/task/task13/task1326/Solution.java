package com.javarush.task.task13.task1326;

/* 
Сортировка четных чисел из файла
1. Ввести имя файла с консоли.
2. Прочитать из него набор чисел.
3. Вывести на консоль только четные, отсортированные по возрастанию.

Пример ввода:
5
8
11
3
2
10

Пример вывода:
2
8
10


Требования:
1. Программа должна считывать данные с консоли.
2. Программа должна создавать FileInputStream для введенной с консоли строки.
3. Программа должна выводить данные на экран.
4. Программа должна вывести на экран все четные числа считанные из файла отсортированные по возрастанию.
5. Программа должна закрывать поток чтения из файла(FileInputStream).
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException {
        // напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        //чтение из файла через FileReader и через FileInputStream;
        //BufferedReader fileReader = new BufferedReader(new FileReader(reader.readLine()));
        BufferedReader fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(reader.readLine())));

        ArrayList<Integer> list = new ArrayList<>();
        while (true){
            String s = fileReader.readLine();
            if(s==null)break;
            int ic = Integer.parseInt(s);
            if(ic%2==0) list.add(ic);


        }
        for(int i=0;i<list.size();i++){
            for(int i2=0;i2<list.size();i2++){
                if(list.get(i)<list.get(i2)){
                    int tmp = list.get(i);
                    list.set(i,list.get(i2));
                    list.set(i2,tmp);
                }
            }
        }

        for(int i3=0;i3<list.size();i3++){
            System.out.println(list.get(i3));
        }

        reader.close();
        fileReader.close();



    }
}
