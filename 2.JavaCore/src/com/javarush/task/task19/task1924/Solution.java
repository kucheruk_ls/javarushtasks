package com.javarush.task.task19.task1924;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* 
Замена чисел
c:\0\3.txt
*/

public class Solution {
    public static Map<Integer, String> map = new HashMap<Integer, String>();
    static{
        map.put(0, "ноль");
        map.put(1, "один");
        map.put(2, "два");
        map.put(3, "три");
        map.put(4, "четыре");
        map.put(5, "пять");
        map.put(6, "шесть");
        map.put(7, "семь");
        map.put(8, "восемь");
        map.put(9, "девять");
        map.put(10, "десять");
        map.put(11, "одиннадцать");
        map.put(12, "двенадцать");
    }

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader freader = new BufferedReader(new FileReader(reader.readLine()));
        reader.close();
        ArrayList<String> list = new ArrayList<>();
        String str = "";
        StringBuilder sb = new StringBuilder();
        while ((str=freader.readLine())!=null){
            String[] ms = str.split(" ");
            for(int i=0;i<ms.length;i++) {
                if (ms[i].matches("[0-9]+")) {
                    if (map.containsKey(Integer.parseInt(ms[i]))) {
                        ms[i] = map.get(Integer.parseInt(ms[i]));
                    }
                    if (i > 0) {
                        sb.append(" ");
                    }
                    sb.append(ms[i]);
                    if(i==ms.length-1)sb.append("\n");
                }else{
                    if (i > 0) {
                        sb.append(" ");
                    }
                    sb.append(ms[i]);
                    if(i==ms.length-1)sb.append("\n");
                }
            }

        }
        System.out.println(sb.toString());
        freader.close();

    }
}
