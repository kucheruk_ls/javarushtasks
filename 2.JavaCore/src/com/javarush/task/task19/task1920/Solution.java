package com.javarush.task.task19.task1920;

/* 
Самый богатый
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

import static java.lang.Math.abs;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader freader = new BufferedReader(new FileReader(args[0]));
        TreeMap<String, Double> map = new TreeMap<>();
        ArrayList<String> list = new ArrayList<>();

        Double d = 0.0;
        while (freader.ready()){
            String[] mas = freader.readLine().split(" ");
            if(map.containsKey(mas[0])){
                d = map.get(mas[0]);
                d = d+Double.parseDouble(mas[1]);
                map.remove(mas[0]);
                map.put(mas[0],d);
            }else{
                map.put(mas[0], Double.parseDouble(mas[1]));
            }

        }
        freader.close();
        Double sr = 0.0;

        for(Map.Entry<String, Double> entry:map.entrySet()){
           // System.out.println(entry.getKey()+" "+entry.getValue());
            if(entry.getValue()>sr){
                sr=entry.getValue();

            }
        }

        for(Map.Entry<String, Double> entry:map.entrySet()){
            String key = entry.getKey();
            Double val = entry.getValue();
            //if(sr==val){
              //  list.add(key);
            //} if (abs(a/b - 1) < epsilon)
            if(abs(val/sr-1)<5.96e-08){
                list.add(key);
            }



        }
        Collections.sort(list);

        for(String s:list){
            System.out.println(s);
        }



    }
}
