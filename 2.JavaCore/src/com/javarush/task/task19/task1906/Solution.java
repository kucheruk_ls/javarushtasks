package com.javarush.task.task19.task1906;

/* 
Четные символы
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String f1 = reader.readLine();
        String f2 = reader.readLine();
        reader.close();
        BufferedReader freader = new BufferedReader(new FileReader(f1));
        FileWriter fileWriter = new FileWriter(f2);
        int counts = 0; //считает считанные символы
        while (freader.ready()){
            counts++;
            int i = freader.read();
            if(counts%2==0){
                fileWriter.write(i);
            }
        }

        freader.close();
        fileWriter.close();

    }
}
