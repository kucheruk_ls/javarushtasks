package com.javarush.task.task19.task1926;

/* 
Перевертыши
*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader freader = new BufferedReader(new FileReader(reader.readLine()));
        reader.close();
        StringBuilder sb = new StringBuilder();
        String str = "";
        while ((str=freader.readLine())!=null){
            sb.append(str);
            String sstr = sb.reverse().toString();
            sb.delete(0,sb.length());
            System.out.println(sstr);
        }
        freader.close();
    }
}
