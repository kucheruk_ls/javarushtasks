package com.javarush.task.task19.task1908;

/* 
Выделяем числа
c:\0\1.txt
c:\0\2.txt
*/

import java.io.*;

public class Solution {



    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String f1 = reader.readLine();
        String f2 = reader.readLine();
        reader.close();

        BufferedReader freader = new BufferedReader(new FileReader(f1));
        String si = "";
        StringBuilder sb = new StringBuilder();
        while ((si = freader.readLine())!=null){
            sb.append(si);

        }
        freader.close();

        String[] ma = sb.toString().split("\\s");
        sb.delete(0,sb.length());
        //String sis = sb.toString();
        for(String s:ma){
            if(s.matches("[0-9]+")){
                if(sb.length()!=0){
                    sb.append(" ");
                }
                sb.append(s);

            }

        }
        String str = sb.toString();

        BufferedWriter fwriter = new BufferedWriter(new FileWriter(f2));
        fwriter.write(str);
        fwriter.close();



    }
}
