package com.javarush.task.task19.task1922;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Ищем нужные строки
*/

public class Solution {
    public static List<String> words = new ArrayList<String>();

    static {
        words.add("А");
        words.add("Б");
        words.add("В");
    }

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader freader = new BufferedReader(new FileReader(reader.readLine()));
        reader.close();
        String str = "";
        int count = 0;
        while((str = freader.readLine())!=null){
            String[] strm = str.split(" ");
            for(int i=0;i<words.size();i++){
                for(int i2 = 0;i2<strm.length;i2++){
                    if(words.get(i).equals(strm[i2])){
                        count++;

                    }
                }
            }
            if(count==2){
                System.out.println(str);
                count=0;
            }else{
                count=0;
            }

        }
        freader.close();
    }
}
