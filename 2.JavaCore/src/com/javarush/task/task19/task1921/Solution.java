package com.javarush.task.task19.task1921;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* 
Хуан Хуанович
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws Exception {
        BufferedReader freader = new BufferedReader(new FileReader(args[0]));
        SimpleDateFormat sdf = new SimpleDateFormat("MM dd yyyy");
        Date bd = new Date();
        while (freader.ready()){
            String str = freader.readLine();
            String[] mass = str.split(" ");
            bd = sdf.parse(mass[mass.length-2]+" "+mass[mass.length-3]+" "+mass[mass.length-1]);
            StringBuilder sb = new StringBuilder();
            for(int i=0;i<mass.length-3;i++){

                if(i>0)sb.append(" ");
                sb.append(mass[i]);
            }

            PEOPLE.add(new Person(sb.toString(),bd));

        }

        freader.close();
        //for(Person p:PEOPLE){
          //  System.out.println(p.getName()+" "+ p.getBirthday());
        //}

    }
}
