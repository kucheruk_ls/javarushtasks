package com.javarush.task.task19.task1909;

/* 
Замена знаков
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String f1 = reader.readLine();
        String f2 = reader.readLine();
        reader.close();

        BufferedReader freader = new BufferedReader(new FileReader(f1));
        StringBuilder sb = new StringBuilder();
        while (freader.ready()) {
            sb.append(freader.readLine().replaceAll("[.]", "!"));
        }
        freader.close();

        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(f2));
        bufferedWriter.write(sb.toString());
        bufferedWriter.close();
    }
}
