package com.javarush.task.task19.task1910;

/* 
Пунктуация
*/


import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String f1 = reader.readLine();
        String f2 = reader.readLine();
        reader.close();

        StringBuilder sb = new StringBuilder();
        BufferedReader freader = new BufferedReader(new FileReader(f1));
        while (freader.ready()){
            sb.append(freader.readLine().replaceAll("\\p{Punct}",""));
        }
        freader.close();
        BufferedWriter fwrite = new BufferedWriter(new FileWriter(f2));
        fwrite.write(sb.toString());
        fwrite.close();



    }
}
