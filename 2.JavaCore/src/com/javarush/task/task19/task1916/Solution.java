package com.javarush.task.task19.task1916;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Отслеживаем изменения
c:\0\1.txt
c:\0\2.txt
*/

public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String f1 = reader.readLine();
        String f2 = reader.readLine();
        reader.close();
        ArrayList<String> list1 = new ArrayList<>();
        ArrayList<String> list2 = new ArrayList<>();
        String s1 = "";
        String s2 = "";
        BufferedReader f1r = new BufferedReader(new FileReader(f1));
        BufferedReader f2r = new BufferedReader(new FileReader(f2));
        while ((s1 = f1r.readLine())!=null){
            list1.add(s1);
        }
        while ((s2 = f2r.readLine())!=null){
            list2.add(s2);
        }
        f1r.close();
        f2r.close();

        //----------------------блок сортировки данных-------------------------------

        while (list1.size()!=0&&list2.size()!=0){
            if(list1.get(0).equals(list2.get(0))){
                lines.add(new LineItem(Type.SAME,list1.remove(0)));
                list2.remove(0);
            }else if(!list1.get(0).equals(list2.get(0))&&list1.get(0).equals(list2.get(1))){
                lines.add(new LineItem(Type.ADDED,list2.remove(0)));

            }else if(!list1.get(0).equals(list2.get(0))&&!list1.get(0).equals(list2.get(1))){
                lines.add(new LineItem(Type.REMOVED,list1.remove(0)));

            }
        }
        if(list1.size()>0&&list2.size()==0){
            lines.add(new LineItem(Type.REMOVED,list1.get(0)));
        }else if(list1.size()==0&&list2.size()>0){
            lines.add(new LineItem(Type.ADDED,list2.get(0)));
        }









        //--------------------тестовая печать списка lines----------------------

        for(LineItem li:lines){
            System.out.println(li.toString());
        }





        //-------------------конец блока сортировки данных-----------------------------
    }


    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }

        public String toString(){
            return this.type+" "+this.line;
        }
    }
}
