package com.javarush.task.task19.task1918;

/* 
Знакомство с тегами
c:\0\4.html
*/

import java.io.BufferedReader;
//import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.*;

public class Solution {
    public static void main(String[] args)throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader freader = new BufferedReader(new FileReader(reader.readLine()));
        reader.close();
        String str = "";
        StringBuilder sb = new StringBuilder();
        while ((str = freader.readLine())!=null){

            sb.append(str);
        }
        freader.close();
        str = sb.toString();
        String openteg = "<"+args[0];
        String closeteg = "</"+args[0]+">";
        boolean open = false;

        ArrayList<String> list = new ArrayList<>();

        HashMap<Integer, String> opt = new HashMap<>();
        HashMap<Integer, String> clt = new HashMap<>();

        int startidx = 0;
        int stopidx = 0;
        int idxstart = 0;
        int idxstop = 0;
        while (true) {
            idxstart = str.indexOf(openteg, startidx);
            if(idxstart==-1)break;
            opt.put(idxstart, "opentag");
            startidx = idxstart+1;


        }
        while (true){
            idxstop = str.indexOf(closeteg,stopidx);
            if(idxstop==-1)break;
            clt.put(idxstop,"closetag");
            stopidx = idxstop+1;
        }

        //HashMap<Integer, Integer> restag = new HashMap<>();
        TreeMap<Integer, String> tagmap = new TreeMap<>();

        TreeMap<Integer, Integer> restag = new TreeMap<>();


           tagmap.putAll(opt);
           tagmap.putAll(clt);


        int opentag=0;
        int closetag=0;
        //stcikl:
        //tagmap.remove(opentag);
        //tagmap.remove(closetag);
        boolean poradomoy = false;
        while (true) {
            for (Map.Entry<Integer, String> entry : tagmap.entrySet()) {


                if (entry.getValue().equals("opentag")) {

                    open = true;
                    opentag = entry.getKey();
                } else if (entry.getValue().equals("closetag") && open == true) {

                    closetag = entry.getKey();
                    restag.put(opentag, closetag);

                    open = false;
                    tagmap.put(opentag, "null");
                    tagmap.put(closetag, "null");

                }


            }
            if(!tagmap.containsValue("opentag")&&!tagmap.containsValue("closetag"))break;



        }



        for (Map.Entry<Integer, Integer> entry : restag.entrySet()) {
            //System.out.println(entry.getKey() + " " + entry.getValue());
            System.out.println(str.substring(entry.getKey(),entry.getValue()+closeteg.length()));
        }




    }

}
