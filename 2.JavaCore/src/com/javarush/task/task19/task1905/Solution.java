package com.javarush.task.task19.task1905;

import java.util.HashMap;
import java.util.Map;

/* 
Закрепляем адаптер
*/

public class Solution {
    public static Map<String,String> countries = new HashMap<String,String>();

    static{
       countries.put("UA","Ukraine");
       countries.put("RU","Russia");
       countries.put("CA","Canada");
    }

    public static void main(String[] args) {
        Contact cont = new ReaCust();
        Customer cust = new Custik();
        DataAdapter da = new DataAdapter(cust, cont);
        System.out.println(da.getContactFirstName());
        System.out.println(da.getContactLastName());
        System.out.println(da.getDialString());
        System.out.println(da.getCountryCode());
    }

    public static class DataAdapter implements RowItem {
        private Customer customer;
        private Contact contact;
        public DataAdapter(Customer customer, Contact contact) {
            this.contact = contact;
            this.customer = customer;
        }

        public String getCountryCode(){
            String s = "";
            for(Map.Entry<String, String> entry : countries.entrySet()){
                if(entry.getValue().equals(customer.getCountryName())) {
                    s = entry.getKey();
                    break;
                }
            }
            return s;
        }

        public String getCompany(){
            return customer.getCompanyName();
        }

        public String getContactFirstName() {
            String ss = contact.getName();
            int ids = ss.indexOf(" ");
            ss = ss.substring(ids+1, ss.length());

            return ss;

        }

        public String getContactLastName(){
            String ss = contact.getName();
            int ids = ss.lastIndexOf(",");
            ss = ss.substring(0, ids);
            return ss;
        }

        public String getDialString(){
            String nume = contact.getPhoneNumber();
            StringBuilder sb = new StringBuilder();
            sb.append("callto://+");
            char[] st = nume.toCharArray();
            char[] cif = new char[]{48,49,50,51,52,53,54,55,56,57};

            for(int i = 0;i<st.length;i++){
                for(int i2 = 0;i2<cif.length;i2++){
                    if(st[i]==cif[i2]){
                        sb.append(st[i]);
                    }
                }
            }
            return sb.toString();
        }
    }

    public static class ReaCust implements Contact{
        public String getName(){
            return "Ivanov, Ivan";
        }
        public String getPhoneNumber(){
            return "+3(805)0123-4567";
        }
    }

    public static class Custik implements Customer{
        @Override
        public String getCompanyName() {
            return null;
        }

        @Override
        public String getCountryName() {
            return "Ukraine";
        }
    }

    public static interface RowItem {
        String getCountryCode();        //example UA
        String getCompany();            //example JavaRush Ltd.
        String getContactFirstName();   //example Ivan
        String getContactLastName();    //example Ivanov
        String getDialString();         //example callto://+380501234567
    }

    public static interface Customer {
        String getCompanyName();        //example JavaRush Ltd.
        String getCountryName();        //example Ukraine
    }

    public static interface Contact {
        String getName();               //example Ivanov, Ivan
        String getPhoneNumber();        //example +38(050)123-45-67 or +3(805)0123-4567 or +380(50)123-4567 or ...
    }
}