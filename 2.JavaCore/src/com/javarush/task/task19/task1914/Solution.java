package com.javarush.task.task19.task1914;

/* 
Решаем пример
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {

        PrintStream origOut = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream tmpOut = new PrintStream(baos);
        System.setOut(tmpOut);

        testString.printSomething();

        System.setOut(origOut);

        String dtr = baos.toString();
        String[] str = dtr.split(" ");
        int one = Integer.parseInt(str[0]);
        int two = Integer.parseInt(str[2]);
        int res = 0;
        if(str[1].equals("-")){
            res = one-two;
        } else if(str[1].equals("+")){
            res = one+two;
        }else if(str[1].equals("*")){
            res = one*two;
        }

        System.out.println(str[0]+" "+str[1]+" "+str[2]+" "+str[3]+" "+res);


    }

    public static class TestString {
        public void printSomething() {
            System.out.println("3 + 6 = ");
        }
    }
}

