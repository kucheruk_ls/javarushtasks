package com.javarush.task.task19.task1915;

/* 
Дублируем текст
*/

import java.io.*;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String f1 = reader.readLine();
        reader.close();

        PrintStream originOut = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream tmpOut = new PrintStream(baos);
        System.setOut(tmpOut);
            testString.printSomething();
        System.setOut(originOut);
        String result = baos.toString();
        System.out.println(result);
        FileOutputStream fos = new FileOutputStream(f1);
        byte[] bm = baos.toByteArray();
        for(int i=0;i<bm.length;i++){
            fos.write(bm[i]);
        }
        fos.close();



    }

    public static class TestString {
        public void printSomething() {
            System.out.println("it's a text for testing");
        }
    }
}

