package com.javarush.task.task19.task1919;

/* 
Считаем зарплаты
*/
import java.util.Map;
import java.util.TreeMap;
import java.io.BufferedReader;
import java.io.FileReader;

public class Solution {
    public static void main(String[] args) throws Exception{
        BufferedReader freader = new BufferedReader(new FileReader(args[0]));
        TreeMap<String, Double> map = new TreeMap<>();
        Double d = 0.0;
        while (freader.ready()){
            String[] ma = freader.readLine().split(" ");
            if(map.containsKey(ma[0])){
                d = map.get(ma[0]);
                d = d+Double.parseDouble(ma[1]);
                map.remove(ma[0]);
                map.put(ma[0],d);
            }else{
                d = Double.parseDouble(ma[1]);
                map.put(ma[0],d);
            }

        }
        freader.close();

        for(Map.Entry<String, Double> entry : map.entrySet()){
            System.out.println(entry.getKey()+" "+entry.getValue());
        }

    }
}
