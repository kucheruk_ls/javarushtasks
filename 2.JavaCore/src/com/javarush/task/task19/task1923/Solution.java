package com.javarush.task.task19.task1923;

/* 
Слова с цифрами
*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class Solution {

    public static boolean NumStr(String str){
        boolean res = false;
        char[] m = str.toCharArray();
        char[] cif = new char[]{48,49,50,51,52,53,54,55,56,57};
        for(int i=0;i<m.length;i++){
            for(int i2=0;i2<cif.length;i2++){
                if(m[i]==cif[i2]){
                    res=true;
                }
            }
        }
        return res;
    }


    public static void main(String[] args) throws Exception {
        BufferedReader freader = new BufferedReader(new FileReader(args[0]));
        ArrayList<String> list = new ArrayList<>();
        String str = "";
        StringBuilder sb = new StringBuilder();
        while ((str=freader.readLine())!=null){
            String[] mass = str.split(" ");
            for(String s:mass){
                if(NumStr(s)){
                    if(sb.length()>0){
                        sb.append(" ");
                    }
                    sb.append(s);
                }
            }
        }
        freader.close();
        BufferedWriter fwriter = new BufferedWriter(new FileWriter(args[1]));
        fwriter.write(sb.toString());
        fwriter.close();

    }
}
