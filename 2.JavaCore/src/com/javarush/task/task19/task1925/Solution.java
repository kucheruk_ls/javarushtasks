package com.javarush.task.task19.task1925;

/* 
Длинные слова
*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader freader = new BufferedReader(new FileReader(args[0]));
        String str = "";
        StringBuilder sb = new StringBuilder();
        while ((str = freader.readLine())!=null){
            String[] slovo = str.split(" ");
            for(int i=0;i<slovo.length;i++){
                if(slovo[i].length()>6){
                    if(sb.length()>0){
                        sb.append(",");
                    }
                    sb.append(slovo[i]);
                }
            }
        }
        freader.close();
        BufferedWriter fw = new BufferedWriter(new FileWriter(args[1]));
        fw.write(sb.toString());
        fw.close();
    }
}
