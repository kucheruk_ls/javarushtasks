package com.javarush.task.task19.task1913;

/* 
Выводим только цифры
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream origOut = System.out;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream tmpOut = new PrintStream(baos);

        System.setOut(tmpOut);

        testString.printSomething();

        System.setOut(origOut);

        String result = baos.toString().replaceAll("\\D","");

        System.out.println(result);




    }

    public static class TestString {
        public void printSomething() {
            System.out.println("it's 1 a 23 text 4 f5-6or7 tes8ting");
        }
    }
}
