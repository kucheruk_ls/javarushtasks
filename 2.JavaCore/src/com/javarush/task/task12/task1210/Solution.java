package com.javarush.task.task12.task1210;

/* 
Три метода и максимум
Написать public static методы: int max(int, int), long max(long, long), double max(double, double).
Каждый метод должен возвращать максимальное из двух переданных в него чисел.
*/

public class Solution {
    public static void main(String[] args) {

    }
    //Напишите тут ваши методы
    public static int max(int i, int a){
        return i > a ? i : a;
    }
    public static long max(long i, long a){
        return i > a ? i : a;
    }
    public static double max(double i, double a){
        return i > a ? i : a;
    }

}
