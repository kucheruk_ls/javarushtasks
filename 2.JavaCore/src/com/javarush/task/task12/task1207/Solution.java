package com.javarush.task.task12.task1207;

/* 
Int и Integer
Написать два метода: print(int) и print(Integer).
Написать такой код в методе main, чтобы вызвались они оба.


*/

public class Solution {
    public static void main(String[] args) {
        int i = 10;
        Integer bigi = 15;

        print(i);
        print(bigi);
    }

    //Напишите тут ваши методы
    public static void print(int i){
        System.out.println(i);
    }
    public static void print(Integer bigi){
        System.out.println(bigi+" s");
    }
}
