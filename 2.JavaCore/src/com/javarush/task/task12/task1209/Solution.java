package com.javarush.task.task12.task1209;

/* 
Три метода и минимум
Написать public static методы: int min(int, int), long min(long, long), double min(double, double).
Каждый метод должен возвращать минимальное из двух переданных в него чисел.
*/

public class Solution {
    public static void main(String[] args) {

    }
    //Напишите тут ваши методы
    public static int min(int i, int i2){
        return i < i2 ? i : i2;
    }
    public static long min(long i, long i2){
        return i < i2 ? i : i2;
    }
    public static double min(double i, double i2){
        return i < i2 ? i : i2;
    }



}
