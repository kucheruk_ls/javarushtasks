package com.javarush.task.task16.task1631;

import com.javarush.task.task16.task1631.common.*;

import static com.javarush.task.task16.task1631.common.ImageTypes.BMP;
import static com.javarush.task.task16.task1631.common.ImageTypes.JPG;
import static com.javarush.task.task16.task1631.common.ImageTypes.PNG;

public class ImageReaderFactory implements ImageReader {

    public static ImageReader getImageReader(ImageTypes types){


        if(types==BMP)return new BmpReader();
        else if(types==PNG)return new PngReader();
        else if(types==JPG)return new JpgReader();
        else{
            throw new IllegalArgumentException("Неизвестный тип картинки");
        }


    }


}
