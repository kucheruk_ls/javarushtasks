package com.javarush.task.task16.task1632;

/*

 Создай 5 различных своих нитей c отличным от Thread типом:
1.1. Нить 1 должна бесконечно выполняться;
1.2. Нить 2 должна выводить "InterruptedException" при возникновении исключения InterruptedException;
1.3. Нить 3 должна каждые полсекунды выводить "Ура";
1.4. Нить 4 должна реализовать интерфейс Message, при вызове метода showWarning нить должна останавливаться;
1.5. Нить 5 должна читать с консоли числа пока не введено слово "N", а потом вывести в консоль сумму введенных чисел.
2. В статическом блоке добавь свои нити в List<Thread> threads в перечисленном порядке.
3. Нити не должны стартовать автоматически.
 */


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class Solution {
    public static List<Thread> threads = new ArrayList<>(5);

    static{

        threads.add(new Thread(new Thread1()));
        threads.add(new Thread(new Thread2()));
        threads.add(new Thread(new Thread3()));
        threads.add(new Thread4());
        threads.add(new Thread(new Thread5()));
    }

    public static void main(String[] args) {
        threads.get(3).start();
        threads.get(3).interrupt();
    }

    public static class Thread1 implements Runnable{
        public void run(){
            while (true){
                //System.out.println("я нить 1");
            }
        }
    }

    public static class Thread2 implements Runnable{
        public void run(){
            try{
                while (true){
                    Thread.sleep(20);
                }
            }catch(InterruptedException e){
                System.out.println("InterruptedException");
            }
        }
    }

    public static class Thread3 implements Runnable{
        public void run(){
            try{
                while (true){
                    System.out.println("Ура");
                    Thread.sleep(500);

                }
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }

    public static class Thread4 extends Thread implements Message{
        public boolean rune = false;
        public void run(){
            while (!rune){

            }
        }



        @Override
        public void interrupt() {
            showWarning();
        }

        @Override
        public void showWarning() {
            rune = true;
        }
    }

    public static class Thread5 implements Runnable{
        public void run(){
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            int ri = 0;
            String s="";
            try{
                while (!(s=reader.readLine()).equals("N")){
                    ri = ri+Integer.parseInt(s);
                }

            }catch(Exception e){
                e.printStackTrace();
            }
            System.out.println(ri);
        }
    }

}