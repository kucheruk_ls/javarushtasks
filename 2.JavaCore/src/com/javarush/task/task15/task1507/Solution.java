package com.javarush.task.task15.task1507;

/* 
ООП - Перегрузка
Перегрузите метод printMatrix 8 различными способами. В итоге должно получиться 10 различных методов printMatrix.
*/

import com.sun.org.apache.xpath.internal.SourceTree;

public class Solution {
    public static void main(String[] args) {
        printMatrix(2, 3, "8");
    }

    public static void printMatrix(int m, int n, String value) {
        System.out.println("Заполняем объектами String");
        printMatrix(m, n, (Object) value);
    }

    public static void printMatrix(int m, int n, Object value) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(value);
            }
            System.out.println();
        }
    }

    public static void printMatrix(){//3
        System.out.println("3");

    }

    public static void printMatrix(int m){//4

        System.out.println(m);
    }

    public static void printMatrix(int m, int n){//5
        System.out.println(m+n);

    }

    public static void printMatrix(String s){//6
        System.out.println(s);

    }

    public static void printMatrix(String s, int i){//7
        System.out.println(s+i);

    }

    public static void printMatrix(Object o){//8
        System.out.println(o.toString());

    }

    public static void printMatrix(Object o, int i){//9
        System.out.println(o.toString()+i);

    }

    public static void printMatrix(int i, int m, boolean b){
        if(b) System.out.println(i);
        else System.out.println(m);

    }
}
