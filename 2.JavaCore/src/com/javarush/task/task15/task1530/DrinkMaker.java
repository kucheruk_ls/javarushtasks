package com.javarush.task.task15.task1530;

public abstract class DrinkMaker {
    abstract void getRightCup(); // выбрать подходящюю чашку
    abstract void putIngredient();  // - положить ингредиенты
    abstract void pour(); // - залить жидкостью

    public void makeDrink(){
        getRightCup();
        putIngredient();
        pour();
    }

}
