package com.javarush.task.task15.task1527;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Парсер реквестов
Считать с консоли URL-ссылку.
Вывести на экран через пробел список всех параметров (Параметры идут после ? и разделяются &, например, lvl=15).
URL содержит минимум 1 параметр.
Выводить параметры нужно в той же последовательности, в которой они представлены в URL.
Если присутствует параметр obj, то передать его значение в нужный метод alert.
alert(double value) - для чисел (дробные числа разделяются точкой)
alert(String value) - для строк
Обрати внимание на то, что метод alert необходимо вызывать ПОСЛЕ вывода списка всех параметров на экран.

Пример 1

Ввод:
http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo

Вывод:
lvl view name

Пример 2

Ввод:
http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo

Вывод:
obj name
double 3.14
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        //add your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> parlist = new ArrayList<>();
        String url = reader.readLine();
        String obj=null;
        url = url.substring(url.indexOf("?"));
        int indobj = url.indexOf("obj");
        int ravpobj = url.indexOf("=", indobj);
        int indend = url.indexOf("&",ravpobj);
        if(url.contains("obj")) obj = url.substring(ravpobj+1,indend);
        parlist.add(url.substring(url.indexOf("?")+1,url.indexOf("=")));

        String[] mas = url.split("&");
        for(int i=0;i<mas.length;i++){
            if(mas[i].contains("?")){
             String tmp = mas[i];
             tmp = tmp.substring(tmp.indexOf("?")+1);
             mas[i]=tmp;
            }
            if(mas[i].contains("=")){
                mas[i]=mas[i].substring(0,mas[i].indexOf("="));
            }
        }

        String vivmas="";
        for(String s:mas){
            if(vivmas==""){
                vivmas=vivmas+s;
            }else{
                vivmas=vivmas+" "+s;
            }

        }
        System.out.println(vivmas);
if(obj!=null){
    if(obj.matches("[0-9]+.[0-9]+")){
        alert((double) Double.parseDouble(obj));
    }else{
        alert((String) obj);
    }
}







    }

    public static void alert(double value) {
        System.out.println("double " + value);
    }

    public static void alert(String value) {
        System.out.println("String " + value);
    }
}
