package com.javarush.task.task15.task1514;

import java.util.HashMap;
import java.util.Map;

/* 
Статики-1
*/

public class Solution {
    public static Map<Double, String> labels = new HashMap<Double, String>();

    static
    {
        labels.put(1.0, "d");
        labels.put(4.0, "dfdf");
        labels.put(5.0, "ddddd");
        labels.put(42.0, "deefdf");
        labels.put(52.0, "dsdsddddd");
    }


    public static void main(String[] args) {
        System.out.println(labels);
    }
}
