package com.javarush.games.minesweeper;

import com.javarush.engine.cell.*;

public class MinesweeperGame extends Game {
    private static final int SIDE = 9;
    private GameObject[][] gameField = new GameObject[SIDE][SIDE];
    private int countMinesOnField;
    @Override
    public void initialize() {
        setScreenSize(SIDE, SIDE);
        createGame();
    }

    private void createGame(){
        boolean[][] isM = new boolean[SIDE][SIDE];
        for(int i=0;i<isM.length;i++){
            for(int j=0;j<isM[i].length;j++){
                isM[i][j]=false;
            }
        }


        for(int j2=0;j2<10;j2++) {
            int x = getRandomNumber(10);
            int y = getRandomNumber(10);
            if(isM[x][y]==true){
                j2--;
            }else{
                isM[x][y]=true;
            }
        }

        for(int y=0;y<gameField.length;y++){
            for(int x=0;x<gameField[y].length;x++){
                if(isM[y][x]==true) {
                    gameField[y][x] = new GameObject(x, y, true);

                }else{
                    gameField[y][x] = new GameObject(x,y,false);

                }
                setCellColor(x,y,Color.ORANGE);

            }
        }

        countMinesOnField=10;
    }
}
